import React from 'react';
import {
	View, 
	ScrollView, 
	Text, 
	ActivityIndicator,
	TouchableOpacity,
    TouchableNativeFeedback,
	Platform,
	Image,
	Modal,
	Switch,
	Picker
} from 'react-native';
import {_} from 'lodash';
import styles from '../Screens/style';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

export class Header extends React.Component {
	render() {
		let style = this.props.style;
		return (
			<View style={[inlineStyles.header, style ? style : { backgroundColor:'#fff' }]}>
				{this.props.children}
			</View>
		);
	}
}

export class Loader extends React.Component {
	render() {
		return (
			<View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, height: '100%', width: '100%', position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(255, 255, 255, 0.6)' }}>
				<ActivityIndicator size="large" color="#0000ff" />
			</View>
		);
	}
}

export class Wrapper extends React.Component {
	render() {
		return (
			<View  style={{flex: 1, zIndex: 99,}}>
				<ScrollView style={{ backgroundColor: '#ffffff', flex: 1}} refreshControl={this.props.refreshControl}>
					{this.props.children}
				</ScrollView>
				{this.props.footer}
			</View>
		);
	}
}

export class GuestWrapper extends React.Component {

	render() {
		return (
			<ScrollView style={{backgroundColor: '#ffffff'}}>
				{this.props.children}
			</ScrollView>
		);
	}
}

export class ReviewCart extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			parent : this.props.parent
		}
	}

	__renderGallery(images,length){
		if(length === 1){
			return(
				<CTouchable onPress={ () => this.state.parent.__openGallery(images, 0) }>
					<Image source={ { uri : images[0]  } } style={ [ styles.reviewCart.gallery.images, { width : '100%', height : 150 }  ] } />
				</CTouchable>
			)
		} else if(length === 2){
			return(
				<View style={ { flexDirection : 'row' } }>
					<CTouchable onPress={ () => this.state.parent.__openGallery(images, 0) } style={ { flex : 0.5 } }>
						<Image source={ { uri : images[0]  } } style={ [ styles.reviewCart.gallery.images, { height : 140, marginLeft : 5 }  ] } />
					</CTouchable>
					<CTouchable onPress={ () => this.state.parent.__openGallery(images, 1) } style={ { flex : 0.5 } }>
						<Image source={ { uri : images[1]  } } style={ [ styles.reviewCart.gallery.images, { height : 140 }  ] } />
					</CTouchable>
				</View>
			)
		} else {
			return(
				<View style={ { flexDirection : 'row' } }>
					<CTouchable onPress={ () => this.state.parent.__openGallery(images, 0) } style={ { flex : 0.5 } }>
						<Image source={ { uri : images[0]  } } style={ [ styles.reviewCart.gallery.images, { height : 140, marginLeft : 5 }  ] } />
					</CTouchable>
					<View style={ { flex : 0.5, flexDirection : 'column' } }>
						<CTouchable onPress={ () => this.state.parent.__openGallery(images, 1) } style={ { flex : 0.5 } } >
							<Image source={ { uri : images[1]  } } style={ [ styles.reviewCart.gallery.images, { height : 70, marginBottom : 2 }  ] } />
						</CTouchable>
						<View style={ { flex : 0.5 } } >
							<Image source={ { uri : images[3]  } } style={ [ styles.reviewCart.gallery.images, { height : 70, marginTop : 2 }  ] } />
							<View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, alignItems : 'center', justifyContent : 'center', backgroundColor : '#000', opacity : 0.5 } }>
								<CTouchable onPress={ () => this.state.parent.__openGallery(images, 1) }>
									<Text style={ { color : '#fff', fontSize : 22 } }>{'+ 2'}</Text>
								</CTouchable>
							</View>
						</View>
					</View>
				</View>
			)
		}
	}
	
	render() {
		let item = this.props.item;
		let name = this.props.name;
		let image = this.props.image;
		if(item){
			return (
				<View style={ [ styles.reviewCart ] }>
					<View style={ [ styles.reviewCart.cart ] }>
						<View style={ { flex : 1.8 } }>
							<CTouchable onPress={ () => this.props.redirectToUser ? this.props.redirectToUser(item.user.id) : '' }>
								<Image style={ [ styles.profileImage ] } source={ { uri : image  } } />
							</CTouchable> 
						</View>
						<View style={ [ styles.reviewCart.info, { flex : 8.2 }] }>
							<View style={ { flexDirection : 'row' } }>
								<View style={ { flexDirection : 'row', flex : 0.9 } }>
									<CTouchable onPress={ () => this.props.redirectToUser ? this.props.redirectToUser(item.user.id) : '' }><Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ name ? name : null }</Text></CTouchable> 
									<Text style={ [ styles.reviewCart.info.text, { color : '#5f5f5f', fontSize : 10, marginTop : 3  } ] }>{ 'reviewed' }</Text>
									<CTouchable onPress={ () => this.props.go('DetailScreen', { data : this.props.restaurant }) }><Text style={ [ styles.reviewCart.info.text, { color : '#e05355', fontSize : 13, borderBottomColor : '#e05355', borderBottomWidth : 1  } ] }>{ this.props.restaurantName }</Text></CTouchable>
								</View>
							</View>
							<View style={ { marginTop : 2, marginBottom : 5 } }>
								<Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ item.created_at_formatted }</Text>
							</View>
							<View style={ { flex : 0.6, flexDirection : 'row', } }>
								{
									this.props.ratings(item.rating) 
								}
							</View>
							<View style={ { marginTop : 3, } }>
								{
									this.props.content(item.review, item.image_urls.length > 0 ? true : false) 
								}
							</View>
							{
								item.image_urls.length > 0 ?
									<View style={ [ styles.reviewCart.gallery ] }>
										{
											this.__renderGallery(item.image_urls,item.image_urls.length)
										}
										
									</View>
								:
								null
							}

							{
								!this.props.currentUser ?
									<View style={ [ styles.reviewCart.icon ] }>
										<View style={ { flexDirection : 'row', flex : 0.8 } }>
											<View style={ [ styles.reviewCart.icon._icons, { paddingRight : 15  } ] }>
												<CTouchable onPress={ () => this.props.vote( { id : item.id, vote : 1 } ) } style={ { flexDirection : 'row'} } >
													<Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-up'} color={ item.myvote === 'up' ? '#e05355' : '#4a4a4a' } />
													<Text>{item.upvote}</Text>
												</CTouchable>
											</View>
											<View style={ [ styles.reviewCart.icon._icons ] }>
												<CTouchable onPress={ () => this.props.vote( { id : item.id, vote : 0 } ) } style={ { flexDirection : 'row'} } >
													<Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-down'} color={ item.myvote === 'down' ? '#e05355' : '#4a4a4a' } />
													<Text>{item.downvote}</Text>
												</CTouchable>
											</View>
										</View>
										<View style={ { flex : 0.2 } }>
											<CTouchable onPress={ () => this.state.parent.__report(item.id) }>
												<Text style={ [ styles.defaultText,{ color : 'red' } ] } >{'Report'}</Text>
											</CTouchable>
										</View>
									</View> :
								null

							}
							
						</View>
					</View>
					<Modal transparent={true} visible={this.state.parent.state.reviewGalleryModal} animationType={'slide'} onRequestClose={ () => this.state.parent.setState( { reviewGalleryModal : false } ) }>
						<View style={styles.modalInner}>
							<View style={ { width: 400, padding: 20, backgroundColor: '#ffffff', borderRadius: 15 } }>
								<View style={ styles.validationIcon } >
									<CTouchable onPress={() =>  this.state.parent.setState( { reviewGalleryModal : false } )}>
										<Icon name="x" color="#cccccc" size={22} />
									</CTouchable>
								</View>
								<View style={ { marginTop : 20 } }>
									<Image style={ [ { width : '100%', height : 300 } ] } source={ { uri : this.state.parent.state.reviewGalleryModalImage } } />	
								</View>
							</View>
						</View>
					</Modal>
				</View>
			)
		} else{
			return null
		}
	}
}

export class Cuisines extends React.Component {

	render() {
		
		return(
            this.props.cuisines.map((cuisines, index) => {
				return(
                    <View style={ [ styles.searchFiltersView.filters, this.props.active.indexOf(cuisines.id) !== -1 ? styles.homeScreenFilters : '' ] }>
                        <CTouchable onPress={ () => this.props.activeFilters(cuisines.id, index, this.props.active.indexOf(cuisines.id) !== -1 ? false : true)  }>
                            <Text style={ [ styles.defaultBoldText, styles.searchFiltersView.box, this.props.active.indexOf(cuisines.id) !== -1 ? styles.homeScreenFilters.text : '' ] }>{cuisines.name}</Text>
                        </CTouchable>
                    </View>
                )
            })
        )
	}
}

export class Cart extends React.Component {
	
	__charLimit(text , limit ){
		if(text.length > limit){
			return text.substring(0, 20) + '.....';
		} else{
			return text;
		}
	}

	render() {
		let item = this.props.item;
		if(item){
			return (
				<CTouchable onPress={ () => this.props.navigator('DetailScreen', { data : item }) } style={ [ styles.CartShadow, { flexDirection : 'row', marginBottom : 15, borderBottomWidth : 0, borderBottomColor : '#fff' } ] }>
					<View style={ [ { flex : 2.5 , } ] }>
						<Image 
						source={ { uri : item.logo_url } }
						style={ [ styles.lisitngImages, { resizeMode : 'cover', height : 90 } ]}
						/>
					</View>
					<View style={ { flex : 7.5, backgroundColor : '#fff', paddingLeft : 5 } }>
						<View style={ [ styles.cartTitleView, { flex : 0.4 } ] } >
							<Text style={ { color : '#000000', fontFamily : 'serif', fontSize : 18, fontWeight : 'Bold' } }>{ item.name }</Text> 
						</View>
						<View style={ { flexDirection : 'row', flex : 0.4  } }>
							{
								this.props.ratings(item.rating) 
							}
							<View>
								<Text style={ { color : '#9b9b9b', fontSize : 12 } }>{ ' - ' + item.review_count + ' REVIEWS' }</Text>
							</View>
						</View>
						<View style={ { flexDirection : 'row',flex : 0.2, marginTop : 3 } }>
							{
								item.cuisine_text ?
									<View style={ [ styles.homeCartOtherInfo ] }>
										<Text style={ { color : '#9b9b9b', fontSize : 10, marginHorizontal : 5 } }>{ 'Cuisine' }</Text>
										<Text style={ { color : '#000000', fontSize : 10, fontFamily : 'serif' } }>{ this.__charLimit(item.cuisine_text,20) }</Text>
									</View> :
								null
							}
							{
								item.timings ?
									<View style={ [ styles.homeCartOtherInfo, { paddingHorizontal : 5, } ] }>
										<Text style={ { color : '#9b9b9b', fontSize : 10, marginHorizontal : 5 } }>{ 'Hours' }</Text>
										<Text style={ { color : '#000000', fontSize : 10, fontFamily : 'serif' } }>{ '2 PM - 12 AM' }</Text>
									</View> :
								null
							}
							{
								item.distance_formatted ?
									<View style={ [ styles.homeCartOtherInfo, { paddingHorizontal : 5, } ] }>
										<Icon name="map-pin" color="#e05355" size={ 12 } style={ { marginHorizontal : 5 } }></Icon>
										<Text style={ { color : '#000000', fontSize : 10, fontFamily : 'serif' } }>{ item.distance_formatted }</Text>
									</View> :
								null
							}
							
						</View>
					</View>
				</CTouchable>
			);
		}
		return null;
	}
}

export class MenuCart extends React.Component {
	constructor(props) {
		super(props); 
		this.state = {
			isModalVisible : false,
			image : '',
			name : '',
			description : '',
			price : ''
		}
	}
	
	render() {
		let item = this.props.item;
		if(item){
			return (
				<CTouchable onPress={ () => this.setState( { image : item.image_url, isModalVisible : true, name : item.name, description : item.description, price : item.price_formatted } ) } style={ [ styles.menuCartView.cart ] } >
					<View style={ { flex : 8, paddingLeft : 15} }>
						<View style={ [ styles.menuCartView.detailsView, { flexDirection : 'row' } ] }>
							<Text style={ [ styles.menuCartView.text, styles.defaultBoldText ] }>{ item.name }</Text>
							{
								item.isNew ?
									<View style={ {  bottom : 8, marginLeft : 3, backgroundColor : 'red', paddingVertical : 3, borderRadius : 5, paddingHorizontal : 6  } }>
										<Text style={ { color : '#fff', fontSize : 10 } }>{ 'New' }</Text>
									</View>
								: null
							}
						</View>
						<View style={ styles.menuCartView.detailsView }>
							<Text style={ [ styles.defaultBoldText, { color : '#8e8e8e', fontSize : 15 }] }>{ item.description.substring(0, 20) + '.....' }</Text>
						</View>
						<View style={ styles.menuCartView.detailsView }>
							<Text style={ [  styles.defaultBoldText, { color : '#fe4004', fontSize : 15 }] }>{ 'from ' + item.price_formatted }</Text>
						</View>
						{
							this.props.edit ?
								<CTouchable onPress={() => this.props.navigator('AddMenu', {  data : item  } )} style={ { paddingVertical : 1 } }>
									<Icon name={'edit'} size={15} />
								</CTouchable>
							:
							null	
						}
					</View>
					<View style={ { flex : 2, paddingRight : 15 } }>
						<Image style={ [ { borderRadius : 5, height : 80 } ]} source={ { uri : item.image_url } } />
					</View>
					
					<Modal transparent={true} visible={this.state.isModalVisible} animationType={'slide'} onRequestClose={ () => this.setState( { isModalVisible : false } ) }>
						<View style={styles.modalInner}>
							<View style={ { width: 400, padding: 20, backgroundColor: '#ffffff', borderRadius: 15 } }>
								<View style={ styles.validationIcon } >
									<CTouchable onPress={() =>  this.setState( { isModalVisible : false } )}>
										<Icon name="x" color="#cccccc" size={22} />
									</CTouchable>
								</View>
								<View style={ { marginTop : 20 } }>
									<Image style={ [ { width : '100%', height : 300 } ] } source={ { uri : this.state.image } } />	
								</View>
								<View style={ { marginVertical : 3 } }>
									<Text style={ [ styles.menuCartView.text, styles.defaultBoldText,{ fontSize : 18 } ] }>{ this.state.name }</Text>
								</View>
								<View style={ { marginVertical : 3 } }>
									<Text style={ [  styles.defaultBoldText, { color : '#fe4004', fontSize : 16 }] }>{ this.state.price }</Text>
								</View>
								<View style={ { marginVertical : 3 } }>
									<Text style={ [  styles.defaultBoldText, { color : '#8e8e8e', fontSize : 14 }] }>{ this.state.description }</Text>
								</View>
							</View>
						</View>
					</Modal>
				</CTouchable>
			)
		}
		return null;
	}
}

export class CTouchable extends React.Component {

	get iosTouchable() {
		return (
			<TouchableOpacity style={this.props.style} onPress={this.props.onPress}> 
				{this.props.children}
			</TouchableOpacity>
		);
	}

	get androidTouchable() {
		return (
			<TouchableNativeFeedback onPress={this.props.onPress}> 
				<View style={this.props.style}>{this.props.children}</View>
			</TouchableNativeFeedback>
		);
	}

	render() {
		return (
			<View>
				{ Platform.OS === 'ios' || Platform.Version <= 21 ? this.iosTouchable : this.androidTouchable }
			</View>
		);
	}

}

export class HourChart extends React.Component {
	
	render(){
		let item = this.props.hours;
		return(
			<View style={ { paddingHorizontal : 5 } }>
				<View style={ { flexDirection : 'row', marginVertical : 10 } }>
					<View style={ { flex : 1, marginTop : 8 } }>
						<Icon name="clock" size={28} color={'#2dcdcd'} />
					</View>
					<View style={ { flex : 4 } }>
						{
							item.map( (items, index) => {
								return(
									<View style={ [ styles.HoursDayTable,{ marginVertical : 8 } ] }>
										<Text style={ [ items.today ? styles.boldHourText : styles.defaultText ] }>{ items.day_name }</Text>
									</View>
								)
							} )
						}
					</View>
					<View style={ { flex : 4 } }>
						{
							item.map( (items, index) => {
								if(items.is_close === "1"){
									return(
										<View style={ [ styles.HoursDayTable,{ marginVertical : 8 } ] }>
											<Text style={ [ styles.boldHourText,{ color : 'red' } ] }>{ 'Closed!' }</Text>
										</View>
									)
								} else{
									return(
										<View style={ [ styles.HoursDayTable,{ marginVertical : 8 } ] }>
											<Text style={ [ items.today ? styles.boldHourText : styles.defaultText ] }>{ items.hour_detail }</Text>
										</View>
									)
								}
								
							} )
						}
					</View>
				</View>
			</View>
		)
	}
}

export class RegisterHourCart extends React.Component {
	render(){
		this.state = this.props;
		return(
			<View style={ { marginVertical : 15 } }>
				<View style={ { flexDirection : 'row' } }>
					<View style={ { flex : 3 } }>
						<Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Day' }</Text>
					</View>
					<View style={ { flex : 2 } }>
						<Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Closed' }</Text>
					</View>
					<View style={ { flex : 2.5 } }>
						<Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Open At' }</Text>
					</View>
					<View style={ { flex : 2.5 } }>
						<Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Closed At' }</Text>
					</View>
				</View>
				<View style={ { flexDirection : 'row', marginVertical : 8 } }>
					<View style={ { flex : 3 } }>
						{
							this.state.hours.map( (item, index) => {
								return(
									<View style={ [ styles.HoursDayTable ] }>
										<Text style={ [ item.today ? styles.boldHourText : styles.defaultText ] }>{ item.day_name }</Text>
									</View>
								)
							} )
						}
					</View>
					<View style={ { flex : 2 } }>
						{
							this.state.hours.map( (item, index) => {
								return(
									<View style={ [ styles.HoursToggleBtnTable ] }>
										<Switch onValueChange={ (value) => this.state.setValue(value, index, 'toggle', this.state.hours)  } style={ [ styles.HoursToggleBtnTable.btn ] } value={item.is_close === "1" ? true : false} />
									</View>
								)
							} )
						}
					</View>
					<View style={ { flex : 2.5 } }>
						{
							this.state.hours.map( (item, index) => {
								return(
									<View style={ [ styles.timeDropDown ] }>
										<Picker enabled={ this.state.isClosed(index, this.state.hours) } style={ [ styles.hourPicker ] } selectedValue={item.open_hour} onValueChange = { (value) => this.state.setValue(value, index, 'opentime', this.state.hours) }>
											{   
												this.state.times.map( ( item, index ) => {
													return (
														<Picker.Item label={item + ''} value={item} />   
													)
												} )
											}
										</Picker>
									</View>
								)
							} )
						}
					</View>
					<View style={ { flex : 2.5 } }>
						{
							this.state.hours.map( (item, index) => {
								return(
									<View style={ [ styles.timeDropDown ] }>
										<Picker enabled={ this.state.isClosed(index, this.state.hours) } style={ [ styles.hourPicker ] } selectedValue={item.close_hour} onValueChange = { (value) => this.state.setValue(value, index, 'closetime', this.state.hours) }>
											{   
												this.state.times.map( ( item, index ) => {
													return (
														<Picker.Item label={item + ''} value={item} />   
													)
												} )
											}
										</Picker>
									</View>
								)
							} )
						}
					</View>
				</View>
			</View>
		)
	}
}

let inlineStyles = {
	header: { 
		backgroundColor: '#ffffff', 
		height: 50, 
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.18,
		shadowRadius: 1.00,
		elevation: 1,
		flexDirection: 'row',
		flex: 1,
		paddingLeft: 5,
		paddingRight: 5,
		borderBottomWidth : 0,
        borderBottomColor : '#d9d9d9'
	}
};