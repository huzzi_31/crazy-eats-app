import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cart,Header,MenuCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export class RestaurantProfile extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            reviewMessage : '',
            activeMenu : 1
        }
    }

    __renderGallery(){

    }

    render() {
       return (
            <Wrapper >
                {/* header */}
                <Header>
                    <View style={ styles.header.left }></View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{'Profile'}</Text>
                    </View>
                    <View style={ styles.header.right }>
                        <MaterialIcon name={'settings'} size={30} color={'#000'} />
                    </View>
                </Header>
                {/* header */}
                <View style={{width:'100%'}}>
                    <Image source={ require('../../assets/img/author_demo.jpg') } style={ { height:220,width:'100%' } } />
                    <View style={ { marginTop : 80, justifyContent : 'center' } }>
                        <View style={ { position : 'absolute', left : 0, right : 0, bottom : 0, justifyContent : 'flex-end', alignItems : 'center', paddingVertical :10, paddingHorizontal : 10, } }>
                            <Image 
                                source={ require('../../assets/img/restaurant_logo.png') } 
                                style={ { borderRadius : 50, width : 100, height : 100}}
                            />
                            <Text style={ [ styles.defaultBoldText, { fontSize : 22, color:'#000', marginTop : 5 } ] }>{'China Town'}</Text>
                        </View>
                    </View>
                </View>
                <View style={ { justifyContent : 'center', alignItems : 'center', marginVertical : 15 } }>
                    <CTouchable style={ { borderRadius : 50, paddingVertical : 8, justifyContent : 'center', alignItems : 'center', backgroundColor : '#fff', borderColor : '#fd3c00', width : 150, borderWidth : 1  } }>
                        <Text style={ [ styles.defaultBoldText, { color : '#fd3c00' } ] }>{ 'Unfriend' }</Text>
                    </CTouchable>
                </View>
                <View style={ [ styles.mainContentReviews, ] }> 
                    <View style={ [ styles.reviewCart ] }>
                        <View style={ [ styles.reviewCart.cart ] }>
                            <View style={ { flex : 0.1 } }>
                                <Image style={ [ styles.profileImage ] } source={ require('../../assets/img/author_dummy.jpg') } />
                            </View>
                            <View style={ [ styles.reviewCart.info, { flex : 0.9 }] }>
                               <View style={ { flexDirection : 'row' } }>
                                    <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ 'John Doe' }</Text>
                                    <Text style={ [ styles.reviewCart.info.text, { color : '#5f5f5f', fontSize : 10, marginTop : 3  } ] }>{ 'reviewed' }</Text>
                                    <Text style={ [ styles.reviewCart.info.text, { color : '#e05355', fontSize : 13, borderBottomColor : '#e05355', borderBottomWidth : 1  } ] }>{ 'KFC (Boat Basin)' }</Text>
                               </View>
                               <View style={ { marginTop : 2, marginBottom : 5 } }>
                                   <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ '14 hours ago' }</Text>
                               </View>
                               <View style={ { flex : 0.6, flexDirection : 'row', } }>
                                    <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                    <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                    <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                    <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                    <MaterialIcon name="star" color="#9b9b9b" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                </View>
                                <View style={ { marginTop : 3 } }>
                                    <Text style={ [ styles.reviewCart.description ] }>
                                        {'Great food quality, probably best I have ever had in the city. Service was unbelievable, they just want customer satisfaction at any cost. Must visit place if you are looking for great food in peaceful environment.'}
                                    </Text>
                                </View>
                                <View style={ [ styles.reviewCart.gallery ] }>
                                    {
                                        this.__renderGallery()
                                    }
                                </View>
                                <View style={ [ styles.reviewCart.icon ] }>
                                    <View style={ [ styles.reviewCart.icon._icons, { paddingRight : 15  } ] }>
                                        <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-up'} color={'#4a4a4a'} />
                                        <Text>{'43'}</Text>
                                    </View>
                                    <View style={ [ styles.reviewCart.icon._icons ] }>
                                        <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-down'} color={'#4a4a4a'} />
                                        <Text>{'5'}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
