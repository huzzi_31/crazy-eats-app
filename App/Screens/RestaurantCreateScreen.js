import React from 'react';
import {View, Alert,Text,TextInput,Picker,Image} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cuisines,RegisterHourCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {Validator} from '../Utils/Validator';
import {Guest} from '../Models/Guest';
import CheckBox from 'react-native-check-box';
import RadioForm from 'react-native-simple-radio-button';
import {listing} from './../Models/listing';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
var states = require('../../state.json');

export class RestaurantCreateScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.__activeFilters = this.__activeFilters.bind(this);
        this.state = {
            restaurant_phoneError : false,
            restaurant_nameError : false,
            restaurant_addressError : false,
            restaurant_descriptionError : false,
            restaurant_name : '',
            restaurant_description : '',
            restaurant_address : '',
            cuisine_ids : [],
            cuisines : [],
            filterToggle : false,
            logo_path : '',
            banner_path : '',
            loaded : true,
            restaurant_banner : false,
            restaurant_logo : false,
            registerValidated : false,
            phone : '',
            restaurant_phone: '',
            resizeImage : '',
            errors : {},
            hours : this.__getHoursObject(),
            times : this.__getquarterTimes()
        };
    }

    componentWillMount(){
        this.__getCuisines();  
    }

    __init(){
        this.setState = {
            restaurant_phoneError : false,
            restaurant_nameError : false,
            restaurant_addressError : false,
            restaurant_descriptionError : false,
            restaurant_name : '',
            restaurant_description : '',
            restaurant_address : '',
            cuisine_ids : [],
            cuisines : [],
            filterToggle : false,
            logo_path : '',
            banner_path : '',
            loaded : true,
            restaurant_banner : false,
            restaurant_logo : false,
            registerValidated : false,
            phone : '',
            restaurant_phone: '',
            resizeImage : '',
            errors : {},
            hours : this.__getHoursObject(),
            times : this.__getquarterTimes()
        }
    }

    __getCuisines(){
        this.__activeLoader();
        listing.getCuisines().then( response => {
            console.log(response);
            this.__deactiveLoader();
            if(response.cuisines){
                this.setState( { cuisines : response.cuisines } )
            }
        } ).catch( error => {
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        } )  
    }

    __validateRegister() {
    	let errorCount = 0;
    	let errors = {};

        if(Validator.isEmpty(this.state.restaurant_name)) {
            errorCount++;
            errors.restaurant_nameError = true;
            
        } else {
            errors.restaurant_nameError = false;
        }
        if(Validator.isEmpty(this.state.restaurant_description)) {
            errorCount++;
            errors.restaurant_descriptionError = true;
            
        } else {
            errors.restaurant_descriptionError = false;
        }
        if(Validator.isEmpty(this.state.restaurant_address)) {
            errorCount++;
            errors.restaurant_addressError = true;
        } else {
            errors.restaurant_addressError = false;
        }

        if(Validator.isEmpty(this.state.restaurant_phone)) {
            errorCount++;
            errors.restaurant_phoneError = true;
            
        } else {
            errors.restaurant_phoneError = false;
        }

    	errors.registerValidated = true;
        this.setState(errors);
        return errorCount == 0 ? true : false; 
        

    }

    __setParams(){
        return {
            restaurant_name : this.state.restaurant_name,
            restaurant_description :  this.state.restaurant_description,
            restaurant_address :  this.state.restaurant_address,
            restaurant_phone : this.state.restaurant_phone,
            cuisine_ids : this.state.cuisine_ids,
            logo_path : this.state.logo_path,
            banner_path : this.state.banner_path,
            hours :  this.state.hours,
            latitude : global.currentLat,
            longitude : global.currentLng,
        };
    }

    __register(){
        if(this.__validateRegister()){
            this.__activeLoader();
            let params = this.__setParams();

            Guest.createRestaurant(params).then(response => {
                console.log(response);
                this.__deactiveLoader();
                if(response.status){
                    Alert.alert('Success', 'Your Restaurant Has Been Created!');
                    global.restaurtant.emit('updateRestaurant');
                    global.drawerComponent.setState({
                        restaurant : this.__getCurrentUserRestaurants()
                    });
                    this.__back();
                } else{
                    Alert.alert('Error', response.message);
                    if(response.errors != undefined && response.errors){
                        this.setState( { errors : response.errors }, () => {
                            this.__errorHandling();
                        } );
                    }
                }
            }).catch(error => {
                this.__deactiveLoader();
                console.log(error);
                Alert.alert('Error', 'Please Try Again!');
            })
        }
    }

    __errorHandling(){
        let validation = {};
        const errorsHandle = Object.keys(this.state.errors).map(key => 
            {   
                if(key === 'password'){
                    validation['confirmPasswordError'] = true
                }
                validation[key + 'Error'] = true
            }
        )
        validation['registerValidated'] =  true;
        this.setState(validation)
    }

    __activeFilters(id, _step, toggle){
        const __step = [...this.state.cuisine_ids];

        if(toggle){
            __step.push(id);
        } else{
            var index = __step.indexOf(id)
            __step.splice(index, 1);
        }

        this.setState( { cuisine_ids : __step } )
    }

    __openImagePicker(options,save_type) {
        ImagePicker.showImagePicker(options, (image) => {
            if (image.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (image.error) {
              console.log('ImagePicker Error: ', image.error);
            }
            else if (image.customButton) {
              console.log('User tapped custom button: ', image.customButton);
            }
            else {
                this.__activeLoader();
                let originalRotation = image.originalRotation;
                let uri = image.uri;
                let width = image.width;
                let height = image.height;
                let formatType = image.type;
                
                ImageResizer.createResizedImage( uri, width, height , 'JPEG', 80, originalRotation === 90 ? 90 : (originalRotation === 270 ? -90 : 0) ).then( uri => {
                    
                    listing.getAttachmentUrls(uri.uri, formatType, image.fileName, save_type).then(response  => {
                        this.__deactiveLoader();
                        if(response.status === 1){
                            console.log(save_type, "type");
                            if(save_type === 'RB'){
                                this.setState( { banner_path : response.image_path, restaurant_banner : response.image_url } )
                            }
                            if(save_type === 'RL'){
                                this.setState( { logo_path : response.image_path, restaurant_logo : response.image_url  } )
                            }
                        
                        } else{
                            Alert.alert('Error', response.data.error ? response.data.error : 'Internal Server Error' )
                        }
                    }).catch(error =>{
                        this.__deactiveLoader();
                        Alert.alert("Error", "Please Check Your Connection!");
                    })
                } ).catch( err => {
                    this.__deactiveLoader();
                    console.log( err );
                    Alert.alert( 'Unable to resize the photo', 'Please try again!' )
                } )
            }
        });
    }
    
    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                <View style={ styles.loginMainContent }>
                    <View style={ [ styles.left ] }>
                        <CTouchable onPress={ () => this.__back() }>
                            <Text style={ [ styles.defaultBoldText,{ color : '#4a4a4a', borderBottomColor : '#000', borderBottomWidth : 1, fontSize : 18 } ] }>{ 'Back' }</Text>
                        </CTouchable>
                    </View>
                    <View style={ [ styles.loginHeadingView, { marginBottom : 50, marginTop : 20 } ] }>
                        <Text style={ styles.loginHeadingView.text }>
                            { 'Create Restaurant' }
                        </Text>
                    </View>
                    <View style={ styles.loginFormView }>
                        <View>
                            <View style={ { marginTop : 15, justifyContent : 'center', alignItems : 'center' } }>
                                <Text style={ [ styles.loginFormView.label, { fontSize : 20 } ] }>{'Restaurant Details'}</Text>
                            </View>
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{ 'Restaurant Name' }</Text>
                                <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                    <TextInput value={this.state.restaurant_name} ref={ input => { this.inputs['restaurant_name'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('restaurant_description') } onChangeText={ (text) => this.setState( { restaurant_name : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Full restaurant name' }></TextInput>
                                    {this.__showValidationIcon(this.state.restaurant_nameError, this.state.registerValidated)}
                                </View>
                                {   
                                    this.state.errors.restaurant_name ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_name[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{ 'Restaurant Description' }</Text>
                                <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                    <TextInput value={this.state.restaurant_description} multiline = {true} numberOfLines={4} ref={ input => { this.inputs['restaurant_description'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('restaurant_address') } onChangeText={ (text) => this.setState( { restaurant_description : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Restaurant Description' }></TextInput>
                                    {this.__showValidationIcon(this.state.restaurant_descriptionError, this.state.registerValidated)}
                                </View>
                                {   
                                    this.state.errors.restaurant_description ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_description[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{ 'Address' }</Text>
                                <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                    <TextInput value={this.state.restaurant_address} multiline = {true} numberOfLines={4} ref={ input => { this.inputs['restaurant_address'] = input; }} onSubmitEditing={ () =>  this.focusNextField('restaurant_phone') } onChangeText={ (text) => this.setState( { restaurant_address : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Address' }></TextInput>
                                    {this.__showValidationIcon(this.state.restaurant_addressError, this.state.registerValidated)}
                                </View>
                                {   
                                    this.state.errors.restaurant_address ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_address[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{ 'Restaurant Phone' }</Text>
                                <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                    <TextInput value={this.state.restaurant_phone} keyboardType={'numeric'} ref={ input => { this.inputs['restaurant_phone'] = input; }} onSubmitEditing={ () => this.__register()  } returnKeyType={"next"} onChangeText={ (text) => this.setState( { restaurant_phone : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'xxxxxxxxxx' }></TextInput>
                                    {this.__showValidationIcon(this.state.restaurant_phoneError, this.state.registerValidated)}
                                </View>
                                {   
                                    this.state.errors.restaurant_phone ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_phone[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{  'Restaurant Banner'  }</Text>
                                <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', rotation : 0, storageOptions: { skipBackup: true,  path: 'images' } },'RB') }>
                                    <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                                </CTouchable>
                                {   
                                    this.state.errors.banner_path ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.banner_path[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            {
                                this.state.restaurant_banner ?
                                    <View style={ [ styles.imageView ] }>
                                        <Image source={ { uri : this.state.restaurant_banner } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                        <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                            <CTouchable onPress={ () => this.setState( { restaurant_banner : false, banner_path : ''  } ) } style={ { width : '5%' } }>
                                                <Icon name={'x-circle'} size={20} />
                                            </CTouchable>
                                        </View>
                                    </View>
                                :
                                null
                            }
                            <View style={ {marginTop : 15} }>
                                <Text style={ styles.loginFormView.label }>{  'Restaurant Logo' }</Text>
                                <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, 'RL') }>
                                    <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                                </CTouchable>
                                {   
                                    this.state.errors.logo_path ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.logo_path[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                            {
                                this.state.restaurant_logo ?
                                    <View style={ [ styles.imageView ] }>
                                        <Image source={ { uri :  this.state.restaurant_logo } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                        <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                            <CTouchable onPress={ () => this.setState( { restaurant_logo : false, logo_path : ''  } ) } style={ { width : '5%' } }>
                                                <Icon name={'x-circle'} size={20} />
                                            </CTouchable>
                                        </View>
                                    </View>
                                :
                                null
                            }
                        </View>
                        <View style={ styles.searchFiltersView }>
                            <View style={ { flexDirection : 'row', marginTop : 15, flex : 1, flexWrap : 'wrap' } }>
                                {/* cuisines filters */}
                                <Cuisines toggle={this.state.filterToggle} active={this.state.cuisine_ids} activeFilters={this.__activeFilters} cuisines={this.state.cuisines} />
                                {/* cuisines filters */}
                                {   
                                    this.state.errors.cuisine_ids ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.cuisine_ids[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                        </View>
                        
                        {/* restaurant hours */}
                            <RegisterHourCart hours={this.state.hours} times={this.state.times} isClosed={this.__thisIsClosed.bind(this)} setValue={this.__setValue.bind(this)} />
                        {/* restaurant hours */}    
                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__register()  }>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'Create'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
