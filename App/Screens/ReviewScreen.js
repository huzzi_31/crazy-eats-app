import React from 'react';
import {FlatList, View, Alert,Text,Modal} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header,ReviewCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {listing} from '../Models/listing';

export class ReviewScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.ratings = this.__ratings.bind(this);
        this.contentDescription = this.__contentDescription.bind(this);
        this.goToUser = this.__goToUserProfile.bind(this);
        this.addvote = this.__addVote.bind(this);
        this.state = {
            reviewMessage : '',
            activeMenu : 1,
            restaurantId : this.props.navigation.state.params.id,
            restaurantName : this.props.navigation.state.params.name,
            restaurant : this.props.navigation.state.params.restaurant,
            reviews : [],
            user : [],
            loaded : false,
            isReviews : false,
            currentImageGallery : [],
            isgalleryModal : false,
			reviewGalleryModal : false,
			reviewGalleryModalImage : '',
        }
        this.__getReviews();
    }

    __getReviews(){
        this.__activeLoader();
        listing.getReviews(this.state.restaurantId).then( response => {
            if(response.status === false){
                Alert.alert('Error', response.message)
            } else{
                if(response.reviews.length > 0){
                    this.setState( { reviews : response.reviews, isReviews : true  } );
                }
            }
            this.__deactiveLoader();
        } ).catch( error => {
            console.log(error);
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        })
    }

    __addVote(params){
        this.__activeLoader();
        listing.vote(this.state.restaurantId, params, 'review').then( response => {
            if(response.status === false){
                Alert.alert('Error', response.message)
            } else{
                if(response.reviews.length > 0){
                    this.setState( { reviews : response.reviews, } );
                }
            }
            this.__deactiveLoader();
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            Alert.alert('Error','Please Try Again');
        } )
    }

    __report(reivew_id){
        this.__activeLoader();
        let param = {
            restaurant_id : this.state.restaurantId,
            review_id : reivew_id
        };

        listing.reportReview(param).then( response => {
            if(response.status){
                if(response.reviews){
                    this.setState( { reviews : response.reviews, isReviews : true  } );
                }
            } else{
                Alert.alert('Error', response.message);
            }
            this.__deactiveLoader();
        } ).catch( error => {
            console.log(error);
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        })
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
          <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }></View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{ 'Reviews Feed' }</Text>
                    </View>
                    <View style={ styles.header.right }>
                        <CTouchable onPress={ () => this.__go('WriteReview', { id : this.state.restaurantId, }) }>
                            <Icon name={'plus'} size={30} color={'#000'} />
                        </CTouchable>
                    </View>
                </Header>
                {/* header */}
                <View style={ [ styles.mainContentReviews ] }> 
                    {
                        this.state.isReviews ?
                            <FlatList 
                                extraData={this.state}
                                data={this.state.reviews}
                                renderItem={({item}) =>
                                    <ReviewCart parent={this} name={item.user.name} item={item} restaurant={this.state.restaurant} restaurantId={this.state.restaurant} go={this.__go.bind(this)} restaurantName={this.state.restaurantName} ratings={this.ratings} content={this.contentDescription} gallery={this.images} vote={this.addvote} redirectToUser={this.goToUser} image={item.user.profile_image_url}  />
                                }   
                                keyExtractor={(item, index) => index} 
                            /> 
                        :
                            <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                                <Text style={ [ styles.defaultBoldText, { color : '#292929' }] }>{ 'No Reviews Yet! Be The First To Review' }</Text>
                            </View>
                    }
                </View>
            </Wrapper>
        );
    }
}
