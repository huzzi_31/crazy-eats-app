import React from 'react';
import {Dimensions, View,Text} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {BaseScreen} from '../Screens/BaseScreen';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
import styles from './style';
import {CTouchable,Wrapper,Header,ReviewCart,ReviewGallery} from '../Components';
import Icon from 'react-native-vector-icons/Feather';

export class Gallery extends BaseScreen {

    constructor(props) {
		super(props);
		this.state = {
			active : this.props.navigation.state.params.index,
            images : this.props.navigation.state.params.images
		}
    }
    
    get pagination () {
		const { active } = this.state;
        return (
            <Pagination
				dotsLength={this.state.images.length}
              	activeDotIndex={active}
				dotStyle={{
					width: 10,
					height: 10,
					borderRadius: 5,
					marginHorizontal: 2,
					backgroundColor : '#000',
				}}
				inactiveDotStyle={{
					width: 10,
					height: 10,
					borderRadius: 5,
					marginHorizontal: 2,
					backgroundColor : '#fff',
				}}  inactiveDotScale={0.6}
            />
        );
    }

    render() {
        return (
            <View style={ { flex : 1, justifyContent : 'center', backgroundColor : '#000' } }>
                <View style={ { left : 5, position : 'absolute', top : 5 } }>
                    <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                        <Icon name={'chevron-left'} size={35} color={ '#fff' } />
                    </CTouchable>
                </View>
                <View>
                    <Carousel
                        data={this.state.images}
                        autoplay={true}
                        autoplayInterval={1000}
                        renderItem={this.__imageCarousel}
                        sliderWidth={viewportWidth} 
                        itemWidth={viewportWidth}
                        onSnapToItem={ (index) => this.setState( { active : index } ) }
                        inactiveSlideScale={1}
                    />
                </View>
            </View>
        );
    }
}
