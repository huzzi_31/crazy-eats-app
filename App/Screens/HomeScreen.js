import React from 'react';
import {Image, View, Alert,Text,TextInput,ActivityIndicator,FlatList} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cart,MenuCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {listing} from './../Models/listing';
import OneSignal from 'react-native-onesignal'; 
import moment from 'moment';

export class HomeScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.go = this.__go.bind(this);
        this.__activeFilters = this.__activeFilters.bind(this);
        this.ratings = this.__ratings.bind(this);
        this.state = {
            activeFilter : 1,
            params : {
                longitude : global.currentLng,
                latitude : global.currentLat,
                q : '',
                cuisines : [],
            },
            cuisines : [],
            loader : true,
            cuisinesLoader : true,
            filterToggle : false,
            restaurants : [],
            isUserProfile : global.userData.role === 'restaurant' ? false : true,
            activeMenu : 1,
            restaurantId : global.userData.role === 'restaurant' ? global.userData.restaurant_id : false,
            data : [],
            menus : [],
            time : '',
            listingTab : 1,
            isMenuEdit : true
        };
        this.__init();
        this.configureNotification();
        global.initHistory.on('getHistory', () => { this.__init(); });
        global.menus.on('getMenus', () => { this.__getMenus(); }); 
    }

    __init(){
        if(this.state.isUserProfile){
            this.__getRestaurants();
            this.__getCuisines();
        } else{
            this.__getMenus();
        }
    }

    configureNotification(){
        OneSignal.init("d19ded94-8c9f-4793-b26f-dace0ef351bf");
        OneSignal.configure();
        this.onOpened = this.onOpened.bind(this);
        OneSignal.addEventListener('opened', this.onOpened);
    }

    onOpened(openResult) {
        console.log('HUzaifa push')
        if(global.userData.role == 'user'){
            let data = openResult.notification.payload.additionalData;
            console.log('Data: ', data);
            this.__go('DetailScreen', { data : data.restaurant, type : 'push' });
 
        }
    }
    __getMenus(){
        this.setState( { loader : true } );
        listing.listMenus(this.state.restaurantId).then( response => {
            this.setState( { loader : false } );
            if(response.menus){
                this.setState( { data : response.menus, activeMenu : response.menus[0].id, menus : response.menus[0].menus,   } ) 
            }
        } ).catch( error => {
            this.setState( { loader : false } );
        } )
    }
   
    __activeFilters(id, _step, toggle){
        let __step = this.state.params;
        __step.cuisines = [];
        __step.cuisines.push(id);
        
        this.__go('SearchResults',{ params : __step })
    }

    __search(){
        let __step = this.state.params;
        __step.cuisines = [];
        this.__go('SearchResults',{ params : __step })
    }

    __activeMenu(_step){
        this.setState( { activeMenu : _step, menus : this.state.data.filter((e) => e.id === _step)[0].menus } )
    }
    
    __getRestaurants() {
        this.setState( { loader : true } );
        listing.getRestaurants(this.state.params).then( response => {
            this.setState( { loader : false } );
            console.log(response);
            if(response.restaurants){
                this.setState( { restaurants : response.restaurants } )
            }
        } ).catch( error => {
            this.setState( { loader : false } );
            Alert.alert('Error','Please Try Again');
        } ) 
    }

    __getCuisines(){
        this.setState( { cuisinesLoader : true } );
        listing.getCuisines().then( response => {
            this.setState( { cuisinesLoader : false } );
            if(response.cuisines){
                this.setState( { cuisines : response.cuisines } )
                global.cuisinesList = response.cuisines;
            }
        } ).catch( error => {
            this.setState( { cuisinesLoader : false } );
            Alert.alert('Error','Please Try Again');
        } )  
    }

    __q(q){
        let params = this.state.params;
        params.q = q;
        params.latitude = '';
        params.longitude = '';
        this.setState( { params : params, } )
    }

    __nearby(){
        let params = this.state.params;
        params.latitude =  global.currentLat;
        params.longitude = global.currentLng;
        this.setState( { params : params, }, () => this.__go('SearchResults',{ params : this.state.params}) )
    }

    // __getGreetingTime(){

    //     var currentTime = moment();

    //     const splitAfternoon = 12; 
    //     const splitEvening = 17; 
    //     const currentHour = parseFloat(currentTime.format('HH'));
      
    //     if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
    //         return (
    //             <Image 
    //                 source={require('../../assets/img/homebanner.png')}
    //                 style={ { width : '100%', height : 100 } }
    //             />
    //         )
    //     } else if (currentHour >= splitEvening) {
    //         return (
    //             <Image 
    //                 source={require('../../assets/img/eve_banner.jpg')}
    //                 style={ { width : '100%', height : 100 } }
    //             />
    //         )
    //     } else{
    //         return (
    //             <Image 
    //                 source={require('../../assets/img/morning_banner.jpg')}
    //                 style={ { width : '100%', height : 100 } }
    //             />
    //         )
    //     }
      
    // } 

    __renderCart(item){
        if(this.state.listingTab === 1){
            if(parseInt(item.partnered) === 1){
                return(
                    <Cart ratings={this.ratings} item={item} navigator={this.go} />
                )
            }
        } else{
            if(parseInt(item.partnered) === 0){
                return(
                    <Cart ratings={this.ratings} item={item} navigator={this.go} />
                )
            } 
        }
    }

    __render(){
        if(this.state.isUserProfile){
            return(
                <View style={ styles.HomeMainContent }>
                    <View style={ { flexDirection : 'row' } }>
                        <View style={ [ styles.center, { flex : 1 } ] }>
                            <Text style={ [ styles.defaultBoldText, { color : '#555555', fontSize : 25 } ] }>{'Home'}</Text>
                        </View>
                        <CTouchable onPress={ () => this.__go('SearchForm') } style={ [ { flex : 0, marginTop : 5 } ] }>
                            <Icon name={ 'search' } color='#4a4a4a' size={25} />
                        </CTouchable>
                    </View>
                    <View style={ { marginTop : 15 } }>
                        <Image 
                            source={require('../../assets/img/homebannerofficial.jpg')}
                            style={ { width : '100%', height : 100 } }
                        />
                        <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, justifyContent : 'flex-end', alignItems : 'flex-end', paddingVertical : 5, paddingHorizontal : 10, flexDirection : 'row', marginTop:5 }}>
                            <CTouchable onPress={ () => this.__nearby() }>
                                <Text style={  [ styles.defaultBoldText, {color : '#fff',fontSize : 10 }] }>
                                    {'Restaurant Nearby'} 
                                    <Icon color='#fff' name={'chevron-right'} size={10} />
                                </Text>
                            </CTouchable>
                        </View>
                    </View>
                    <View style={ [ styles.homeSearchView ] }>
                        <CTouchable onPress={ () => this.__go('SearchForm', { cuisines : this.state.params.cuisines }) } style={ { padding : 10 } }>
                            <Icon color='#ff9e80' name={'search'} size={25} />
                        </CTouchable>
                        <TextInput value={this.state.params.q} onChangeText={ (text) => this.__q(text) } onSubmitEditing={ () => this.__search() } style={ [ styles.defaultBoldText, { fontSize : 16, width : '100%' } ] } underlineColorAndroid={ 'transparent' } placeholder={ 'Search restaurants,cuisines..' } placeholderTextColor={'#ff9e80'} />
                    </View>
                    <View style={ styles.searchFiltersView }>
                        <View style={ [ styles.left ] }>
                            <Text style={ [ styles.defaultBoldText, { color : '#555555', fontSize : 20 } ] }>{'Pick a Cuisine'}</Text>
                        </View>
                        {/* cuisines filters */}
                        {
                            !this.state.cuisinesLoader ?
                                <View style={ { flexDirection : 'row', marginTop : 15, flex : 1, flexWrap : 'wrap' } }>
                                    {
                                        this.state.cuisines.map((cuisines, index) => {
                                            return(
                                                <View style={ [ styles.searchFiltersView.filters ] }>
                                                    <CTouchable onPress={ () => this.__activeFilters(cuisines.id)  }>
                                                        <Text style={ [ styles.defaultBoldText, styles.searchFiltersView.box ] }>{cuisines.name}</Text>
                                                    </CTouchable>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            :
                                <View style={ { justifyContent : 'center', alignItems : 'center' } }>
                                    <ActivityIndicator size="large" color="#0000ff" />
                                </View>
                        }
                        {/* cuisines filters */}
                    </View>
                    <View style={ [ styles.cartListView ] }>
                        <View style={ { marginVertical : 10, flexDirection : 'row' } }>
                            <View style={ [ styles.left,{ flex : 1 }  ] }>
                                <Text style={ [ styles.defaultBoldText, { color : '#000000', fontSize : 20 } ] }>{'Restaurants Nearby'}</Text>
                            </View>
                            <View  style={ [ styles.right ] }>
                                <CTouchable onPress={ () => this.__go('SearchResults', { params : this.state.params } )}>
                                    <Text style={ [ styles.defaultBoldText, { color : '#007aff', fontSize : 12 } ] }>{'See All'}</Text>
                                </CTouchable>
                            </View>
                        </View>
                        <View style={ [ styles.menuTitleBar ] }>
                            <View style={ [ styles.menuTitleBar.view, this.state.listingTab === 1 ? styles.activeMenu : '', { flex : 5 } ] }>
                                <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.setState({listingTab : 1}) }>
                                    <Text style={ [ styles.menuTitleBar.text, this.state.listingTab === 1 ? styles.activeMenu.text : '' ] }>{ 'Partners' }</Text>
                                </CTouchable>
                            </View>
                            <View style={ [ styles.menuTitleBar.view, this.state.listingTab === 2 ? styles.activeMenu : '' , { flex : 5 } ] }>
                                <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.setState({listingTab : 2}) }>
                                    <Text style={ [ styles.menuTitleBar.text, this.state.listingTab === 2 ? styles.activeMenu.text : '' ] }>{ 'Non-Partners' }</Text>
                                </CTouchable>
                            </View>
                        </View>
                        <View style={ [ styles.cartView ] }>
                            {
                                !this.state.loader ?
                                    <View>
                                        {
                                            this.state.restaurants.length > 0 ?
                                                <FlatList 
                                                    extraData={this.state}
                                                    data={this.state.restaurants}
                                                    renderItem={({item}) =>
                                                        this.__renderCart(item)
                                                    }   
                                                    keyExtractor={(item, index) => index} 
                                                />
                                            :
                                                <View style={ { justifyContent : 'center', alignItems : 'center' } }>
                                                    <Text>{'We dont have any restaurant registered near by your current location.'}</Text>  
                                                    <CTouchable style={ [ styles.searchFiltersView.filters, { backgroundColor : '#e05355', } ] } onPress={ () => this.__go('SearchResults',{ params :  this.state.params }) }>
                                                        <Text style={ [ styles.defaultBoldText, styles.searchFiltersView.box, { color : '#fff' } ] }>{'See All'}</Text>
                                                    </CTouchable>      
                                                </View>
                                        }
                                    </View> :
                                <View>
                                    <ActivityIndicator size="large" color="#0000ff" />
                                </View>

                            }
                        </View>
                    </View>
                </View>
            )
        } else{
            return(
                <View style={ styles.HomeMainContent }>
                   <View style={ { flexDirection : 'row' } }>
                        <View style={ [ styles.center, { flex : 1 } ] }>
                            <Text style={ [ styles.defaultBoldText, { color : '#555555', fontSize : 25 } ] }>{'Home'}</Text>
                        </View>
                    </View> 
                    <View style={ [ styles.mainContentMenu ] }> 
                        <View style={ [ styles.menuTitleBar ] }>
                            {
                                this.state.data.map((category, index) => {
                                    return(
                                        <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === category.id ? styles.activeMenu : '', { flex : 3 } ] }>
                                            <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(category.id) }>
                                                <Text style={ [ styles.menuTitleBar.text ] }>{ category.name }</Text>
                                            </CTouchable>
                                        </View>
                                    )
                                })
                            }
                        </View>
                        <View style={ [ styles.menuCartView ] }>
                            {
                                this.state.menus.length > 0 ?
                                    <FlatList 
                                        extraData={this.state}
                                        data={this.state.menus}
                                        renderItem={({item}) =>
                                            <MenuCart edit={this.state.isMenuEdit} navigator={this.go} item={item} />
                                        }   
                                        keyExtractor={(item, index) => index} 
                                    /> :
                                <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                                    <Text style={ [ styles.defaultBoldText, { color : '#292929' }] }>{ 'No Menus Found!' }</Text>
                                </View>
                            }
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {
        const PageFooter = (props) => 
            <View>
                <CTouchable activeOpacity={ 0.5 } style={ [ styles.FloatBtnFixed,{ flexDirection : 'row' } ] } onPress={() => this.__go('AddMenu')}>
                    <Icon name={'plus'} size={20} color={'#fff'} />
                    <Text style={ [ { color : '#fff', fontSize : 15 } ] }>{'Add Menu'}</Text>
                </CTouchable>
            </View>;
       return (
            <Wrapper footer={ !this.state.loader ? !this.state.isUserProfile  ?  <PageFooter /> : null : null}>
                {
                    this.__render()
                }
            </Wrapper>
        );
    }
}
