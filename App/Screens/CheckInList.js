import React from 'react';
import { View,Text,FlatList,ActivityIndicator,Image} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cart,Header} from '../Components';
import {listing} from '../Models/listing';


export class CheckInList extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            history : [],
            loaded : true,
            restaurantCheckIns : [],
            restaurantId : this.props.navigation.state.params.id,
        }
        this.__getHistory();
    }

    __getHistory(){
        this.__activeLoader();
        listing.getRestaurantCheckIns(this.state.restaurantId).then( response => {
            this.__deactiveLoader();
            this.setState( {
                restaurantCheckIns : response.check_ins,
            } )
            
        } ).catch( error => {   
            this.__deactiveLoader();
        } )
    }
    
    __renderCart(){
        return(
            <View style={ [ styles.mainContentMenu ] }> 
                <View style={ [ styles.cartListView,{ marginTop : 10, } ] }>
                   {
                        !this.state.loader ?
                            <View>
                                <FlatList 
                                    extraData={this.state}
                                    data={this.state.restaurantCheckIns}
                                    renderItem={({item}) =>
                                        <View style={ [ styles.reviewCart ] }>
                                            <View style={ [ styles.reviewCart.cart ] }>
                                                <View style={ { flex : 0.1 } }>
                                                    <Image style={ [ styles.profileImage ] } source={ { uri :  item.profile_image_url  } } /> 
                                                </View>
                                                <View style={ [ styles.reviewCart.info, { flex : 0.9 }] }>
                                                    <View style={ { flexDirection : 'row' } }>
                                                        <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ item.name }</Text>
                                                    </View>
                                                    <View style={ { marginTop : 2, marginBottom : 5 } }>
                                                        <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ item.pivot_created_at }</Text>
                                                    </View>
                                                </View>
                                            </View>
                                        </View>
                                    }   
                                    keyExtractor={(item, index) => index} 
                                />
                            </View> :
                        <View>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>

                    }
                </View>
            </View>
        )
    }

    render() {
        const PageLoader = (props) => this.__loader();
      	return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{'Checked in history'}</Text>
                    </View>
                </Header>
                {/* header */}
                {
                    this.__renderCart()
                }
            </Wrapper>
        );
    }
}
