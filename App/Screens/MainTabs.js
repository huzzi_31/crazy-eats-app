import React from 'react';
import {
	View,
	AsyncStorage,
	Text
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import { TabNavigator, TabBarBottom } from 'react-navigation';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import {HomeScreen} from './HomeScreen';
import { FavouritesScreen } from './FavouritesScreen';
import { ProfileScreen } from './ProfileScreen';
import { ReviewForm } from './ReviewForm';

export default TabNavigator({
		Home: { screen: HomeScreen },
		Review : { screen: ReviewForm },
		Favourite : { screen: FavouritesScreen },
		Profile : { screen: ProfileScreen }
	}, {
		navigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, tintColor }) => {
		    	const { routeName } = navigation.state;
		    	let iconName;

		    	switch(routeName) {
		    		case 'Home':
		    			iconName = 'home';
						break;
					case 'Review':
						iconName = 'message-square';
						break;
					case 'Favourite':
						iconName = global.userData.role === 'restaurant' ? 'list' : 'heart';
						break;
					case 'Profile':
						iconName = 'user';
						break;
					
				}
				
				if(routeName === 'Home'){
					return (
						<MaterialIcon name={iconName} size={30} color={tintColor} />
					)
				} else{
					return (
						<Icon name={iconName} size={30} color={tintColor} />
					)	
				}

		  	},
		}),
	tabBarOptions: {
		activeTintColor: '#fd3c00',
		inactiveTintColor: '#4a4a4a',
		showLabel : false,
		style: {
			backgroundColor: '#fff',
		}
	},
	tabBarComponent: TabBarBottom,
	tabBarPosition: 'bottom',
	animationEnabled: true,
	swipeEnabled: true
});