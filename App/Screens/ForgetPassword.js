import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper} from '../Components';
import {Guest} from '../Models/Guest';
import CheckBox from 'react-native-check-box';

export class ForgetPassword extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            email : '',
        }
    }

    __resetPassword(){
        Guest.forgetPassword(this.state.email).then( response => {
            if(response.status){
                Alert.alert('Success', response.message);
                this.setState({ email : '' });
            } else{
                Alert.alert('Error', response.message);  
            }
        } ).catch( error => {

        } )
    }

    render() {
      	return (
            <Wrapper>
                <View style={ styles.loginMainContent }>
                    <View style={ [ styles.left ] }>
                        <CTouchable onPress={ () => this.__back() }>
                            <Text style={ [ styles.defaultBoldText,{ color : '#4a4a4a', borderBottomColor : '#000', borderBottomWidth : 1, fontSize : 18 } ] }>{ 'Back' }</Text>
                        </CTouchable>
                    </View>
                    <View style={ [ styles.loginHeadingView, { marginBottom : 50, marginTop : 20 } ] }>
                        <Text style={ styles.loginHeadingView.text }>
                            { 'Forget Password' }
                        </Text>
                    </View>
                    <View style={ styles.loginFormView }>
                        <View style={ {marginTop : 15} }>
                            <Text style={ [ styles.defaultBoldText, { color : '#9b9b9b', fontSize : 13 } ] }>
                                {'To reset your password,enter your email address and we will send you instruction on how to create a new password'}
                            </Text>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Email Address' }</Text>
                            <TextInput value={ this.state.email } onSubmitEditing={ () => this.__resetPassword() } onChangeText={ (text) => this.setState( { email : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.loginFormView.textField,styles.defaultBoldText,{ fontSize : 20 } ] } placeholder={ 'yourname@mail.com' }></TextInput>
                        </View>
                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__resetPassword()}>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'SUBMIT'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
