import React from 'react';
import {
	View,
	AsyncStorage,
	Image,
	Text,
	Dimensions,
	Alert
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './style';
import { StackActions, NavigationActions } from 'react-navigation';
import ReactNativeLocationServicesSettings from 'react-native-location-services-settings';
import {Loader,CTouchable} from '../Components';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {listing} from './../Models/listing';
import {_} from 'lodash';
import {FBLoginManager} from 'react-native-facebook-login';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export class BaseScreen extends React.Component {

	constructor(props) {
		super(props);
		this.focusNextField = this.focusNextField.bind(this);
		this.inputs = {};
		this.state = {
			loader : true,
			moreDescription : false,
			registerHours : [
				{
					close_hour: "00:00:00",
					day_name: "Monday",
					is_close: "1",
					open_hour: "00:00:00",

				},
				{
					close_hour: "00:00:00",
					day_name: "Tuesday",
					is_close: "1",
					open_hour: "00:00:00",
				},
				{
					close_hour: "00:00:00",
					day_name: "Wednesday",
					is_close: "1",
					open_hour: "00:00:00",
				},
				{
					close_hour: "00:00:00",
					day_name: "Thursday",
					is_close: "1",
					open_hour: "00:00:00",
				},
				{
					close_hour: "00:00:00",
					day_name: "Friday",
					is_close: "1",
					open_hour: "00:00:00",
				},
				{
					close_hour: "00:00:00",
					day_name: "Saturday",
					is_close: "1",
					open_hour: "00:00:00",
				},
				{
					close_hour: "00:00:00",
					day_name: "Sunday",
					is_close: "1",
					open_hour: "00:00:00",
				}
			]
		}
	}

	focusNextField(key) {
		this.inputs[key].focus();
	}
	
	__back() {
		this.props.navigation.goBack();
	}

	__go(to, params = {}) {
		const pushAction = StackActions.push({
			routeName: to,
			params: params 
		  });
		  this.props.navigation.dispatch(pushAction);
	}

	__goAndReset(routeName, params = {}) {
		const resetAction = StackActions.reset({
			index: 0,
			actions: [NavigationActions.navigate({ routeName: routeName, params })],
		  });
		this.props.navigation.dispatch(resetAction);
	}
	
	__getCurrentUserRestaurants(){
        listing.getCurrentUserRestaurants().then( response => {
            if(response.status){
               return response.restaurants;
			}
		} ).catch( error => {
			Alert.alert('Error', 'Please Try Again!');
        } )
    }

	__showValidationIcon(hasError, validated = false) {
		
		if(!validated) {
			return null;
		}

		if(hasError == true) {
			return (
				<View style={styles.validationIcon}>
					<FontAwesome name="times-circle" size={22} color="#f20000" />
				</View>
			);
		}

		return (
			<View style={styles.validationIcon}>
				<FontAwesome name="check-circle" size={22} color="#4caf50" />
			</View>
		);
	}

	__loader(){
		return(
			<View style={ styles.loaderOverlay }>
				<Loader />
			</View>
		)
	}

	__activeLoader(){
        this.setState({
            loaded:false
        })
    }

    __deactiveLoader(){
        this.setState({
            loaded:true
        })  
	}

	__logout() {
		AsyncStorage.clear();
		global.isUserLoggedIn = false;
		global.userData = {};
		global.drawerComponent.setState({
			hideNavigation : true,
			isUserLoggedIn: false,
		});
		FBLoginManager.logout((data) => {console.log(data)});
		this.__goAndReset('login',{screen : 'normal-login'});
	}

	__getLatLng(){
		console.log('d');
		navigator.geolocation.getCurrentPosition(
			(position) => {
				var lat = parseFloat(position.coords.latitude);
				var long = parseFloat(position.coords.longitude);
				console.log(lat,long,'d');
				global.currentLat = lat;
				global.currentLng = long;
				global.isLocationEnable = true;
				if( global.isUserLoggedIn ){
					this.__goAndReset('MainTabs');
				}
			},
			(error) => {
				console.log(error.message)
			},
			{enableHighAccuracy: true, timeout: 10000, maximumAge: 10000}
		)
	}

	__askForGps(){
		ReactNativeLocationServicesSettings.checkStatus('high_accuracy').then(res => {
            if (!res.enabled) {
            	ReactNativeLocationServicesSettings.askForEnabling(res => {
                	if (res) {
						this.__getLatLng();
                	} else {
                  		alert('location services were denied by the user')
                	}
              	})
            } else{
				this.__getLatLng();
			}
        })
	}


	__ratings(rating){
		let array = [1, 2, 3, 4, 5];
        return(
            array.map((ratings, index) => {
				return( 
					<MaterialIcon name="star" color={ rating >= ratings ? '#fdc10a' : '#9b9b9b' } size={ 15 } style={ [  index === 0 ? { paddingHorizontal : 0, } : { paddingHorizontal : 1,  } ] }></MaterialIcon>
                )
            })
        )
	}
	
	__renderMonthList(){
		let array = [];
        for (var i = 1; i <= 12; i++) {
            array[i] = i;
        }
        return array;
	}

	__contentDescription(text,isGallery){
		return(
			<Text style={ [ styles.reviewCart.description ] }>
				{text}
			</Text>
		)
	}

	__openGallery(array, index = 0){
		this.__go('gallery', { images : array, index : index });
	}
	
    __imageCarousel ({item, index}) {
        return (
            <View key={index}>
                <Image 
                source={ { uri : item } }
                style={ { width : '100%', height : viewportHeight / 4 }}
                />
            </View>
        );
    }
	
	__goToUserProfile(id){
		this.__go('UserProfile', { id : id } )
	}

	__getquarterTimes() {
        var times = [];
        for (let i = 1; i < 25; i++) {
			var am = ':00 AM', pm = ':00 AM';
			var time = i + am;
			
			if(i > 11 && i < 24){
				var time = i === 12 ? i + pm  : i - 12 + pm;
			}
		 	if(i > 23){
				var time = i - 12 + am;
			}
			times.push(time);
			
		}
        return times;
	}

	// __getquarterTimes() {
    //     var quarterHours = ["00", "30"];
    //     var times = [];
    //     for(var i = 0; i < 24; i++){
    //         for(var j = 0; j < 2; j++){
    //             var time = i + ":" + quarterHours[j] + ":00";
    //             if(i < 10){
    //             time = "0" + time;
    //             }
    //             times.push(time);
    //         }
    //     }
    //     return times;
	// }
	
	__getHoursObject(){
		return this.state.registerHours;
	}

	__thisIsClosed(index, array){
        let params = array;
        return params[index].is_close === "1" ? false : true ;
	}
	
	__setValue(value, index, i, array){
        let params = array;
        if(i == 'toggle'){
            params[index].is_close = value ? "1" : "0";
        } else if(i == 'opentime'){
            params[index].open_hour = value;
        } else if(i == 'closetime'){
            params[index].close_hour = value;
        }
        this.setState( { hours : params } )

	}
	
	__setAttachments(attachments){
		let array = attachments;
		array = array.filter(function( element ) {
			return element !== undefined;
		});
		return array;
	}

	
}