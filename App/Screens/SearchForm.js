import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cuisines} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import CheckBox from 'react-native-check-box';
import {listing} from '../Models/listing';

export class SearchForm extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            isByPlaces : false,
            cuisines : [],
            filterToggle : false,
            loaded : false,
            isUserProfile : global.userData.role === 'restaurant' ? false : true,
            params : {
                cuisines : [],
                longitude : '',
                latitude : '',
                q : ''
            },
            selectedCuisines : [],
        }
        this.__activeFilters = this.__activeFilters.bind(this);
        this.__getCuisines();
        
    }

    __getCuisines(){
        listing.getCuisines().then( response => {
            this.__deactiveLoader();
            if(response.cuisines){
                this.setState( { cuisines : response.cuisines } )
            }
        } ).catch( error => {
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        } )  
    }
    
    __activeFilters(id, _step, toggle){
        const __step = this.state.selectedCuisines;

        if(toggle){
            __step.push(id);
        } else{
            var index = __step.indexOf(id)
            __step.splice(index, 1);
        }

        this.setState( { selectedCuisines : __step } )
    }

    __q(q){
        let params = this.state.params;
        params.q = q;
        params.latitude = '';
        params.longitude = '';
        this.setState( { params : params, } )
    }

    __setParams(){
        let params = this.state.params;
        params.latitude = this.state.isByPlaces ? global.currentLat : '';
        params.longitude = this.state.isByPlaces ? global.currentLng : '';
        params.cuisines = this.state.selectedCuisines;
        this.setState( { params : params, selectedCuisines : [] }, () => {
            this.__go('SearchResults',{ params : this.state.params })
        } )
       
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null }>
                {/* header */}
                <View style={ styles.DetailHeader }>
                    <View style={ { flexDirection : 'row' } }>
                        <View style={ [ styles.HeaderSearchBox ] }>
                            <CTouchable onPress={ () => this.__back() } style={ { padding : 10, } }>
                                <Icon color='#d1d1d6' name={'x'} size={20} />
                            </CTouchable>
                            <TextInput value={this.state.params.q} onChangeText={ (text) => this.__q(text) } onSubmitEditing={ () => this.__setParams() } style={ [ styles.defaultBoldText, { fontSize : 15, width : '100%', color : '#111112' } ] } underlineColorAndroid={ 'transparent' }  placeholder={ 'Search restaurants,cuisines..' }  />
                        </View>
                        <View style={ { paddingHorizontal : 10 } }>
                            <View style={ { marginTop : 8 } }>
                                <CTouchable onPress={ () => this.__setParams() }>
                                    <Icon color='#007aff' name={'check'} size={30} />
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
                {/* header */}
                <View style={ [ styles.HomeMainContent, {  paddingVertical : 10, paddingHorizontal : 10 } ] }>
                    <View style={ [ styles.cartView, { marginTop : 0 } ] }>
                        {
                            this.state.isUserProfile ? 
                                <View style={ styles.registerCheckBoxView }>
                                    <CheckBox
                                    onClick={()=>{
                                        this.setState({
                                            isByPlaces:!this.state.isByPlaces
                                        })
                                    }}
                                    isChecked={this.state.isByPlaces}
                                    style={ styles.registerCheckBoxView.CheckBox }
                                    />
                                    <Text style={ [ styles.defaultBoldText, { color : '#9b9b9b',fontSize : 16 } ] }>
                                        { 'Restaurants NearBy Me' }
                                    </Text>
                                </View>
                            : null
                        }
                        <View style={ styles.searchFiltersView }>
                            <View style={ [ styles.left ] }>
                                <Text style={ [ styles.defaultBoldText, { color : '#555555', fontSize : 20 } ] }>{'Filter By Cuisine'}</Text>
                            </View>
                            {/* cuisines filters */}
                            <View style={ { flexDirection : 'row', marginTop : 15, flex : 1, flexWrap : 'wrap' } }>
                                <Cuisines toggle={this.state.filterToggle} active={this.state.selectedCuisines} activeFilters={this.__activeFilters} cuisines={this.state.cuisines} />
                            </View>
                            {/* cuisines filters */}
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
