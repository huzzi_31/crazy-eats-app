import React from 'react';
import {View,Text,Switch,Picker,Alert,Dimensions} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {Guest} from '../Models/Guest';
const { width: viewportWidth, } = Dimensions.get('window');

export class RestaurantHourScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            hours : this.props.navigation.state.params.data.hours,
            times : this.__getquarterTimes(),
        }
    }

    __updateHour(){
        Guest.updateHours( this.state.hours ).then( response => {
            console.log(response);
            global.restaurtant.emit('updateRestaurant');
        } ).catch( error => {
            console.log(error);
            Alert.alert('Error','Please Try Again');
        } )  
    }

   
    __setValue(value, index, i){
        let params = this.state.hours;
        console.log(params[index]);
        if(i == 'toggle'){
            params[index].is_close = value ? "1" : "0";
        } else if(i == 'opentime'){
            params[index].open_hour = value;
        } else if(i == 'closetime'){
            params[index].close_hour = value;
        }
        this.setState( { hours : params },() => this.__updateHour() )

    }

    
    render() {
       return (
            <Wrapper>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                            <Icon name={'chevron-left'} size={25} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ [ styles.header.center, { flexDirection : 'row' } ] }>
                        <Text style={ [ styles.headerText, { color : '#000' } ] }>{ 'Hour Detail' }</Text>
                    </View>
               </Header>
                {/* header */}
                <View style={ { paddingHorizontal : 5, marginVertical : 15 } }>
                    <View style={ { flexDirection : 'row' } }>
                        <View style={ { flex : 4 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Day' }</Text>
                        </View>
                        <View style={ { flex : 2 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Closed' }</Text>
                        </View>
                        <View style={ { flex : 2 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Open At' }</Text>
                        </View>
                        <View style={ { flex : 2 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ 'Closed At' }</Text>
                        </View>
                    </View>
                    <View style={ { flexDirection : 'row', marginVertical : 8 } }>
                        <View style={ { flex : 3 } }>
                            {
                                this.state.hours.map( (item, index) => {
                                    return(
                                        <View style={ [ styles.HoursDayTable ] }>
                                            <Text style={ [ item.today ? styles.boldHourText : styles.defaultText ] }>{ item.day_name }</Text>
                                        </View>
                                    )
                                } )
                            }
                        </View>
                        <View style={ { flex : 3 } }>
                            {
                                this.state.hours.map( (item, index) => {
                                    return(
                                        <View style={ [ styles.HoursToggleBtnTable ] }>
                                            <Switch onValueChange={ (value) => this.__setValue(value, index, 'toggle')  } style={ [ styles.HoursToggleBtnTable.btn ] } value={item.is_close === "1" ? true : false} />
                                        </View>
                                    )
                                } )
                            }
                        </View>
                        <View style={ { flex : 2 } }>
                            {
                                this.state.hours.map( (item, index) => {
                                    return(
                                        <View style={ [ styles.timeDropDown ] }>
                                            <Picker enabled={ this.__thisIsClosed(index, this.state.hours) } style={ [ styles.hourPicker ] } selectedValue={item.open_hour} onValueChange = { (value) => this.__setValue(value, index, 'opentime') }>
                                                {   
                                                    this.state.times.map( ( item, index ) => {
                                                        return (
                                                            <Picker.Item label={item + ''} value={item} />   
                                                        )
                                                    } )
                                                }
                                            </Picker>
                                        </View>
                                    )
                                } )
                            }
                        </View>
                        <View style={ { flex : 2 } }>
                            {
                                this.state.hours.map( (item, index) => {
                                    return(
                                        <View style={ [ styles.timeDropDown ] }>
                                            <Picker enabled={ this.__thisIsClosed(index, this.state.hours) } style={ [ styles.hourPicker ] } selectedValue={item.close_hour} onValueChange = { (value) => this.__setValue(value, index, 'closetime') }>
                                                {   
                                                    this.state.times.map( ( item, index ) => {
                                                        return (
                                                            <Picker.Item label={item + ''} value={item} />   
                                                        )
                                                    } )
                                                }
                                            </Picker>
                                        </View>
                                    )
                                } )
                            }
                        </View>
                    </View>
                </View>
            
            </Wrapper>
        );
    }
}
