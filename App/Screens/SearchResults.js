import React from 'react';
import {FlatList, View, Alert,Text,TextInput} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cart,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {listing} from '../Models/listing';

export class SearchResults extends BaseScreen {

    constructor(props) {
        super(props);
        this.go = this.__go.bind(this);
        console.log(this.props.navigation);
        this.state = {
            params : this.props.navigation.state.params.params,
            loaded : false,
            restaurants : [],
            isResultFound : true,
            activeMenu : 1
        };
        this.ratings = this.__ratings.bind(this);
        this.__getRestaurants();
    }

    __getRestaurants() {
        listing.getRestaurants(this.state.params).then( response => {
            this.__deactiveLoader();
            if(response.restaurants && response.restaurants.length > 0){
                this.setState( { restaurants : response.restaurants, isResultFound : true } )
            } else{
                this.setState( { isResultFound : false } )
            }
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            this.setState( { isResultFound : false } )
            Alert.alert('Error','Please Try Again');
        } ) 
    }

    __activeMenu(activeMenu){
        this.setState( { 
            activeMenu : activeMenu
        } )
    }

    __renderCart(item){
        if(this.state.activeMenu === 1){
            if(parseInt(item.partnered) === 1){
                return(
                    <Cart ratings={this.ratings} item={item} navigator={this.go} />
                )
            }
        } else{
            if(parseInt(item.partnered) === 0){
                return(
                    <Cart ratings={this.ratings} item={item} navigator={this.go} />
                )
            } 
        }
    }

    
    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null }>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable  onPress={() => this.__back()}>
                            <Icon name={'chevron-left'} size={25} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{'Search Results'}</Text>
                    </View>
               </Header>
               <View style={ [ styles.menuTitleBar ] }>
                    <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 1 ? styles.activeMenu : '', { flex : 5 } ] }>
                        <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(1) }>
                            <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 1 ? styles.activeMenu.text : '' ] }>{ 'Partners' }</Text>
                        </CTouchable>
                    </View>
                    <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 2 ? styles.activeMenu : '' , { flex : 5 } ] }>
                        <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(2) }>
                            <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 2 ? styles.activeMenu.text : '' ] }>{ 'Non-Partners' }</Text>
                        </CTouchable>
                    </View>
                </View>
                {/* header */}
                <View style={ [ styles.HomeMainContent, {  paddingVertical : 10, paddingHorizontal : 10 } ] }>
                    <View style={ [ styles.cartView, { marginTop : 0 } ] }>
                        {
                            this.state.isResultFound ?
                                <FlatList 
                                    extraData={this.state}
                                    data={this.state.restaurants}
                                    renderItem={({item}) =>
                                        this.__renderCart(item)
                                    }   
                                    keyExtractor={(item, index) => index} 
                                />
                            :
                                <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                                    <View>
                                        <Icon name={'help-circle'} color={'#cccccc'} size={45} />
                                    </View>
                                    <View>
                                        <Text style={ [ styles.defaultBoldText,{ fontSize : 15, color : '#cccccc', textAlign : 'center' } ] }>{'Sorry! No result found according to your search criteria.'}</Text>
                                    </View>
                                </View>
                        }
                        
                    </View>
                </View>
            </Wrapper>
        );
    }
}
