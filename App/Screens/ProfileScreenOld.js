import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions,PermissionsAndroid} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cart,Header,MenuCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');


export class ProfileScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            reviewMessage : '',
            activeMenu : 1,
            isUserProfile :  global.userData.role === 'restaurant' ? false : true,
        };
      console.warn(global.userData);
    }

    __renderGallery(){
       
    }
    __renderProfile(){
        if(this.state.isUserProfile){
            return (
                <View style={ [ styles.reviewCart ] }>
                    <View style={ [ styles.reviewCart.cart ] }>
                        <View style={ { flex : 0.1 } }>
                            <Image style={ [ styles.profileImage ] } source={ require('../../assets/img/author_dummy.jpg') } />
                        </View>
                        <View style={ [ styles.reviewCart.info, { flex : 0.9 }] }>
                            <View style={ { flexDirection : 'row' } }>
                                <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ 'John Doe' }</Text>
                                <Text style={ [ styles.reviewCart.info.text, { color : '#5f5f5f', fontSize : 10, marginTop : 3  } ] }>{ 'reviewed' }</Text>
                                <Text style={ [ styles.reviewCart.info.text, { color : '#e05355', fontSize : 13, borderBottomColor : '#e05355', borderBottomWidth : 1  } ] }>{ 'KFC (Boat Basin)' }</Text>
                            </View>
                            <View style={ { marginTop : 2, marginBottom : 5 } }>
                                <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ '14 hours ago' }</Text>
                            </View>
                            <View style={ { flex : 0.6, flexDirection : 'row', } }>
                                <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                <MaterialIcon name="star" color="#fdc10a" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                                <MaterialIcon name="star" color="#9b9b9b" size={ 15 } style={ styles.homeScreenRating }></MaterialIcon>
                            </View>
                            <View style={ { marginTop : 3 } }>
                                <Text style={ [ styles.reviewCart.description ] }>
                                    {'Great food quality, probably best I have ever had in the city. Service was unbelievable, they just want customer satisfaction at any cost. Must visit place if you are looking for great food in peaceful environment.'}
                                </Text>
                            </View>
                            <View style={ [ styles.reviewCart.gallery ] }>
                                {
                                    this.__renderGallery()
                                }
                            </View>
                            <View style={ [ styles.reviewCart.icon ] }>
                                <View style={ [ styles.reviewCart.icon._icons, { paddingRight : 15  } ] }>
                                    <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-up'} color={'#4a4a4a'} />
                                    <Text>{'43'}</Text>
                                </View>
                                <View style={ [ styles.reviewCart.icon._icons ] }>
                                    <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-down'} color={'#4a4a4a'} />
                                    <Text>{'5'}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else{
            return (
                <View style={ { flexDirection : 'column', paddingHorizontal : 15 } }>
                    <View style={ { flexDirection : 'row' } }>
                        <View style={ { flex : 0.9 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000000', fontSize : 20 } ] }>{ 'Short Description' }</Text>
                        </View>
                        <View style={ { flex : 0.1, justifyContent : 'flex-end', alignItems : 'flex-end' } }>
                            <MaterialIcon name={'edit'}  size={20}  />
                        </View>
                    </View>
                    <View>
                        <Text style={ [ styles.defaultBoldText,{ fontSize : 12 } ] }>{ 'China Town provides a variety of great environs under one roof. Whether it’s a quick lunch, home delivery, takeaway, relax and dine-in, exclusive catering or hosting that special event at the restaurant.' }</Text>
                    </View>
                    <View style={ { flexDirection : 'row', marginTop : 20 } }>
                        <View style={ { flex : 1 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#000000', fontSize : 20 } ] }>{ 'Created By' }</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={ [ styles.defaultBoldText, { fontSize : 12 } ] }>{ 'Muhammad Haseeb - Owner' }</Text>
                    </View>
                </View>
            )
        }
    }

    render() {
       return (
            <Wrapper >
                {/* header */}
                <Header>
                    <View style={ styles.header.left }></View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{global.userData.name}</Text>
                    </View>
                    <View style={ styles.header.right }>
                        {/* <MaterialIcon name={'settings'} size={30} color={'#000'} /> */}
                        <CTouchable onPress={ () => this.__logout() }>
                            <Text>{ 'Logout' }</Text>
                        </CTouchable>
                    </View>
                </Header>
                {/* header */}
                <View style={{width:'100%'}}>
                    <Image source={ require('../../assets/img/author_demo.jpg') } style={ { height:220,width:'100%' } } />
                    <View style={ { position : 'absolute', top : 0, left : 0, right : 10, bottom : 90, alignItems : 'flex-end', justifyContent : 'flex-end' } }>
                        <MaterialIcon name={'edit'} color={'#fff'} size={30}  />
                    </View>
                    <View style={ { marginTop : 80, justifyContent : 'center' } }>
                        <View style={ { position : 'absolute', left : 0, right : 0, bottom : 0, justifyContent : 'flex-end', alignItems : 'center', paddingVertical :10, paddingHorizontal : 10, } }>
                            <Image 
                                source={ require('../../assets/img/author_dummy.jpg') } 
                                style={ { borderRadius : 50, width : 100, height : 100}}
                            />
                            <Text style={ [ styles.defaultBoldText, { fontSize : 22, color:'#000', marginTop : 5 } ] }>{'John Doe'}</Text>
                        </View>
                    </View>
                </View>
                {
                    this.state.isUserProfile ? 
                    <View style={ { justifyContent : 'center', alignItems : 'center', marginVertical : 15 } }>
                        <CTouchable style={ { borderRadius : 50, paddingVertical : 8, justifyContent : 'center', alignItems : 'center', backgroundColor : '#fff', borderColor : '#fd3c00', width : 150, borderWidth : 1  } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#fd3c00' } ] }>{ 'Unfriend' }</Text>
                        </CTouchable>
                    </View> :
                    null
                }
                <View style={ [ styles.mainContentReviews, ] }> 
                    {
                        this.__renderProfile()
                    }
                </View>
            </Wrapper>
        );
    }
}
