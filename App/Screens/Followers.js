import React from 'react';
import { View, Text, FlatList, ActivityIndicator, Image} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cart,Header} from '../Components';
import {listing} from '../Models/listing';


export class Followers extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            userID : this.props.navigation.state.params.id,
            label : this.props.navigation.state.params.label ? this.props.navigation.state.params.label : 'Followers',
            data : [],
            loaded : true,
        }

        this.__getFollower();
    }

    
    __getFollower(){
        this.__activeLoader();
        listing.getFollowers(this.state.userID, this.state.label === 'Following' ?  true : false ).then( response => {
            this.__deactiveLoader();
            console.log(response);
            let result = this.state.label === 'Following' ? response.followings : response.followers;
            if(result){
                this.setState( { data : result } )
            }
            
        } ).catch( error => {   
            console.log(error);
            this.__deactiveLoader();
        } ) 
    }

    render() {
        const PageLoader = (props) => this.__loader();
      	return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{ this.state.label }</Text>
                    </View>
                </Header>
                {/* header */}
               
                <View style={ [ styles.mainContentMenu ] }> 
                    <View style={ [ styles.cartListView,{ marginTop : 10, } ] }>
                       {
                            !this.state.loader ?
                                <View>
                                    {
                                        this.state.data.length > 0 ?
                                            <FlatList 
                                                extraData={this.state}
                                                data={this.state.data}
                                                renderItem={({item}) =>
                                                    <CTouchable onPress={ () => this.__goToUserProfile(item.id) } style={ [ styles.reviewCart ] }>
                                                        <View style={ [ styles.reviewCart.cart ] }>
                                                            <View style={ { flex : 0.1 } }>
                                                                <Image style={ [ styles.profileImage ] } source={ { uri :  item.profile_image_url  } } /> 
                                                            </View>
                                                            <View style={ [ styles.reviewCart.info, { flex : 0.9 } ] }>
                                                                <View style={ { flexDirection : 'row' } }>
                                                                    <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ item.name }</Text>
                                                                </View>
                                                            </View>
                                                        </View>
                                                    </CTouchable>
                                                }   
                                                keyExtractor={(item, index) => index} 
                                            />
                                        :
                                            <View style={ { justifyContent : 'center', alignItems : 'center' } }>
                                                <Text style={ [ styles.defaultBoldText,{ color : '#000', fontSize : 22 } ] } >{'No ' + this.state.label}</Text>
                                            </View>
                                    }
                                </View> 
                            :
                                <View>
                                    <ActivityIndicator size="large" color="#0000ff" />
                                </View>

                        }
                    </View>
                </View>
            </Wrapper>
        );
    }
}
