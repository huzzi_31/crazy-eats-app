<View style={ {marginTop : 15} }>
                            <View style={ { flexDirection : 'row' } }>
                                {/* day */}
                                <View style={ [ styles.dobFields, { flex : 3 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Day' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.martial_status} onValueChange = {(value) => this.setState( { day :  value } )}>
                                            <Picker.Item label="Single"   value="Single" />
                                            <Picker.Item label="Married"  value="Married" />
                                            <Picker.Item label="Divorced" value="Divorced" />
                                            <Picker.Item label="Widowed"  value="Widowed" />
                                        </Picker>  
                                    </View>
                                </View>
                                {/* month */}
                                <View style={ [ styles.dobFields, { flex : 3 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Month' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.month} onValueChange = {(value) => this.setState( { month :  value } )}>
                                            {
                                                this.state.months.map( ( item, index ) => {
                                                    return(
                                                        <Picker.Item label={item} value={parseInt(index) + 1} />   
                                                    )
                                                } )
                                            }
                                        </Picker>  
                                    </View>
                                </View>
                                {/* year */}
                                <View style={ [ styles.dobFields, { flex : 4 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Year' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.martial_status} onValueChange = {(value) => this.setState( { year :  value } )}>
                                            <Picker.Item label="Single"   value="Single" />
                                            <Picker.Item label="Married"  value="Married" />
                                            <Picker.Item label="Divorced" value="Divorced" />
                                            <Picker.Item label="Widowed"  value="Widowed" />
                                        </Picker>  
                                    </View>
                                </View>
                                {/* year */}
                            </View>      
                        </View>