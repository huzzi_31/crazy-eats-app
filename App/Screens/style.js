import {Dimensions} from 'react-native';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const fullWidth = '100%' ;
module.exports = {
	splashContainer: {
        flex: 1,
    },
    defaultBoldText : {
        fontFamily: 'OpenSans-Bold',
    },
    defaultText : {
        fontFamily: 'OpenSans-Regular',
    },
    defaultTextColor : {
        color : '#fd3c00',
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    splashLogoView : {
        alignItems: 'center',
		justifyContent: 'center',
    },
    splashScreenText : {
        fontFamily : 'sans-serif',
        color : '#fff', 
        fontSize : 20, 
        fontWeight : 'bold',
    },
    button : {
        backgroundColor: '#fa5503',
		borderRadius: 25,
		padding: 8,
		alignItems: 'center',
		justifyContent: 'center',
        minWidth: '80%',
        marginVertical : 10
    },
    loginMainContent : {
       paddingVertical : 20,
       marginTop : 10,
       paddingHorizontal : 30
    },
    loginHeadingView : {
        marginVertical : 50,
        text : {
            color : '#fd3c00',
            fontSize : 38,
            fontFamily: 'OpenSans-Bold',
        }
    },
    loginFormView : {
        label : {
          color : '#5e5e5e',
          fontFamily: 'OpenSans-Bold',
          fontSize : 15  ,
          paddingLeft : 2
        },
        textField : {
            borderBottomWidth : 1,
            borderColor : '#9b9b9b'
        }
    },
    right : {
        alignItems: 'flex-end', 
        justifyContent: 'center',
    },
    left : {
        alignItems: 'flex-start', 
        justifyContent: 'center',
    },
    center : {
        justifyContent : 'center', 
        alignItems : 'center'
    },
    loginBtns : {
        marginTop : 40,
        btn : {
            borderWidth : 2,
            height : 50,
            backgroundColor : '#fff'
        }
    },
   
    registerCheckBoxView : {
        flexDirection : 'row',
        CheckBox : {
            paddingRight: 5,
        },
    },
    
    homeSearchView : {
        borderColor : '#ff9e80',
        borderWidth : 2,
        flexDirection : 'row',
        padding : 0,
        marginTop : 20,
        height : 50
    },

    searchFiltersView : {
        marginTop : 20,  
        filters : {
            borderColor : '#e05355',
            borderWidth : 2,
            paddingHorizontal : 10,
            paddingVertical : 5,
            marginRight : 10,
            marginVertical : 5,
            justifyContent : 'center',
            alignItems : 'center'
        },
        detailScreenFilter : {
            borderColor : '#e05355',
            borderRadius : 20,
            borderWidth : 1,
            paddingHorizontal : 10,
            paddingVertical : 5,
            marginRight : 10,
            marginVertical : 5,
            justifyContent : 'center',
            alignItems : 'center'
        },
        box : {
            color : '#555555', 
            fontSize : 14   
        }
    },

    lisitngImages:{  
		width : '100%',
    },

    HomeMainContent : {
        paddingVertical : 20,
        marginTop : 5,
        paddingHorizontal : 20
     },
   
    homeScreenRating : {
        paddingHorizontal : 3,
    },

    homeCartOtherInfo : {
        flexDirection : 'row',
    },
    
    CartShadow : {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,
    },
    
    cartView : {
        marginTop : 10
    },

    HeaderSearchBox : {
        borderRadius : 10,
        backgroundColor : '#f0f0f2',
        flexDirection : 'row',
        flex : 1,
        height : 40,
       
    },
    
    DetailHeader : {
        paddingVertical : 5,
        paddingHorizontal : 10,
        borderBottomWidth : 1,
        borderBottomColor : '#d9d9d9'
    },

    header: {
		left: {
			flex: 0.2, 
			alignItems: 'flex-start', 
			height: 50, 
			justifyContent: 'center'
		},
		center: {
			flex: 0.6, 
			height: 50, 
            justifyContent: 'center',
            alignItems: 'center', 
		},
		right: {
			flex: 0.2, 
			alignItems: 'flex-end', 
			height: 50, 
			justifyContent: 'center',
		},
		title: {
			fontSize: 17, 
			textAlign: 'center',
			color:'#fff'
		}
    },
    
    headerText : {
        fontFamily: 'OpenSans-Bold',
        fontSize : 22,
        color : '#4a4a4a'
    },

    otherInfo : {
        paddingVertical : 15,
        icon : {
            paddingRight : 15 ,
            color : "#e05355"
        },
        text : {
            color : '#4a4a4a', 
            fontSize : 12
        }
    },

    detailFooterbtns : {
        justifyContent : 'center', 
        alignItems : 'center',
        flex : 1,
        text : {
            color : '#fff',
            fontSize : 18,
            padding : 15
        }
    },
    addReviews : {
        flexDirection : 'row',
        marginBottom : 15,
        Label : {
            fontFamily: 'OpenSans-Bold',
            fontSize : 20,
            color : '#4a4a4a',
            textAlign:'right'
        },
    },

    textArea : {
        borderWidth : 1,
        borderColor : '#cbcbcb',
        height : 'auto', 
        textAlignVertical : 'top',
        paddingLeft : 10,
        fontSize : 12
    },

    menuTitleBar : {
        flexDirection : 'row',
        marginTop : 5,
        text : {
            fontFamily : 'OpenSans-Bold', 
            color : '#d9d9d9',
            fontSize : 18
        },
        btn : {
            justifyContent : 'center', 
            alignItems : 'center', 
            paddingVertical : 10,
            borderColor : '#000',
        }
    },

    activeMenu : {
        borderBottomColor : '#ec695a',
        borderBottomWidth : 3,
        text : {
            color : '#292929'
        }
    },

    menuCartView : {
        marginTop : 10,
        cart : {
            flexDirection : 'row',
            borderBottomWidth : 1,
            borderBottomColor : '#d9d9d9',
            paddingVertical : 10,
        },
        text : {
            fontFamily : 'OpenSans-Bold', 
            color : '#292929',
            fontSize : 15
        },
        detailsView : {
            paddingVertical : 3,
        }
    },

    mainContentReviews : {
        marginTop : 15,
    },

    reviewCart : {
        borderBottomWidth : 1,
        borderBottomColor : '#d9d9d9',
        cart : {
            paddingHorizontal : 20,
            marginVertical : 10,
            flexDirection : 'row',
        },
        info : {
            marginLeft : 20,
            justifyContent : 'center',
            text : {
                paddingHorizontal : 3
            }
        },
        description : {
            color : '#5d5d5d',
            fontSize : 12
        },
        gallery : {
            paddingVertical : 5,
            images : {
                resizeMode : 'cover', 
                borderRadius : 2,
            }
        },
        icon : {
            flexDirection : 'row',
            paddingVertical : 10,
            _icons : {
                flexDirection : 'row',
                __icons : {
                    paddingRight : 10,
                    marginTop : 3
                }
            }
        }
    },

    profileImage : {
        width : 50,
        borderRadius : 50,
        height : 50
    },

    homeScreenFilters : {
        backgroundColor : '#fd3c00',
        text : { color : '#fff' }
    },

    fieldParentView : {
        flexDirection : 'row',
        field : {
            width : '90%'
        }
    },

    loaderOverlay: {
        flex : 1,
        position : 'absolute',
        left : 0,
        top : 0,
        opacity : 0.5,
        backgroundColor : 'black',
        width : viewportWidth,
        height : '100%'
    } ,
      
    textAreaSetting : {
        height : 'auto', 
        textAlignVertical : 'top',
    },

    imageView : {
        marginTop : 5
    },

    FloatBtnFixed:{
        position : 'absolute',
        // width : 70,
        // height : 70,
        alignItems : 'center',
        justifyContent : 'center',
        right : 10,
        bottom : 30,
        backgroundColor : '#ed1d1d',
        padding  : 10,
        borderRadius : 30
     },
    profileModal : {
        justifyContent : 'center',
        alignItems : 'center',
        height : 230,
        backgroundColor : "#3B5998"
    },
    modalInner : {
        height : '100%', 
        justifyContent : 'center',
        alignItems : 'center',
        backgroundColor : 'rgba(1,1,1,0.5)'
    },
    defaultBtnBlock : {
        backgroundColor : '#fa5503',
        borderRadius : 25,
        height : 45,
        alignItems : 'center',
        justifyContent : 'center',
        minWidth : '100%',
        marginBottom : 15
    },

    restaurantStats : {
        paddingHorizontal : 5,
        paddingVertical : 15,
        flex : 5,
        justifyContent : 'center',
        alignItems : 'center',
        heading : {
            fontFamily: 'OpenSans-Bold',
            fontSize : 26,
            color : '#fff'
        }, 
        text : {
            fontFamily: 'OpenSans-Regular',
            fontSize : 13,
            color : '#fff'
        }
    },

    dobFields : {
        marginRight : 5
    },

    validationIcon : {
		position : 'absolute',
        right : 15,
        top : 15
    },
    
    registerValidateServerErrors : {
        fontFamily: 'OpenSans-Bold',
        color : "#f20000",
        fontSize : 13
    },

    profileInfoParentView : {
        flexDirection : 'row',
        marginTop : 20
    },

    profileInfoHeading : {
        fontFamily: 'OpenSans-Bold', 
        color : '#000000', 
        fontSize : 16
    },

    profileInfoText : {
        fontSize : 12,
        fontFamily: 'OpenSans-Bold', 
        color : '#000000'
    },

    profileInfoView : {
        flex : 5
    },

    imagePickerView : {
		flexDirection : 'row',
		paddingVertical : 10,
		btn : {
			justifyContent : 'center',
			alignItems : 'center',
			borderColor : '#929292',
			borderWidth : 0.5,
			height : 70,
			paddingVertical : 30,
			backgroundColor : '#f2f2f2',
		},
		view : {
			flex : 0.2,
			marginHorizontal : 3
		},
		iconView:{
			borderRadius : 50,
			borderWidth : 3,
			width : 40,
			height : 40,
			backgroundColor : '#000',
			justifyContent : 'center',
			alignItems : 'center',
		}
    },
    
    iconShadow : {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,

        elevation: 11,
    },

    followBtn : {
        borderRadius : 50, 
        paddingVertical : 8, 
        justifyContent : 'center', 
        alignItems : 'center', 
        borderColor : '#3d98f2', 
        width : 150, 
        borderWidth : 1,
        paddingHorizontal : 2,
        marginHorizontal : 5
    },

    shareBtn : {
        borderRadius : 50, 
        paddingVertical : 8, 
        justifyContent : 'center', 
        alignItems : 'center', 
        borderColor : '#3d98f2', 
        width : 150, 
        borderWidth : 1,
        paddingHorizontal : 2,
        marginHorizontal : 2,
        backgroundColor : '#3d98f2'
    },

    userStatsFollowBtn : {
        text : {
            alignSelf : 'center'
        }
    },

    restaurantProfileMenusBtn : {
        justifyContent : 'center', 
        alignItems : 'center',
        marginHorizontal : 10,
        label : {
            color : '#fd3c00',
            fontSize : 12,
            marginTop : 5
        } 
    },

    HoursDayTable : {
        marginVertical : 15
    },

    HoursToggleBtnTable : {
        marginVertical : 11,
        btn : {
            alignSelf : 'center'
        }
    },

    boldHourText : {
        color : '#000',
        fontFamily: 'OpenSans-Bold',
    },

    hourPicker : {
        width : viewportWidth
    },

    restaurantCart : {
        marginVertical : 10,
        image : {
            width : '100%', 
            height : 180 
        },
        detailBox : {
            flexDirection : 'row',
            backgroundColor : '#ffffff',
            height : 100,
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 2,
            },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4,
            logoView : {
                justifyContent : 'center', 
                alignItems : 'center'
            },
            logo : {
                width : 60, 
                height : 60,
                borderRadius : 50,
                marginHorizontal : 8
            },
            description : {
                justifyContent : 'center',
                marginLeft : 10,
                iconView : {
                    flexDirection : 'row',
                    marginTop : 5,
                    text : {
                        fontFamily: 'OpenSans-Regular',
                        fontSize : 15,
                        color : '#707070',
                        marginLeft : 3
                    }
                }
            }
        }
    },

    drawerNavTextView:{
		alignItems : 'center',
		flexDirection : 'row',
		padding : 15,

	},
	drawerIcon : {
		paddingRight : 10,
	},
    
};