import React from 'react';
import {View, Alert, AsyncStorage,Text,TextInput} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper} from '../Components';
import {Validator} from '../Utils/Validator';
import {Guest} from '../Models/Guest';
import Icon from 'react-native-vector-icons/Feather';
import {FBLoginManager} from 'react-native-facebook-login';
import OneSignal from 'react-native-onesignal'; 
import {_} from 'lodash';

import {
	LoginManager,
    LoginButton, 
    AccessToken
} from 'react-native-fbsdk';

export class LoginScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            login : this.props.navigation.state.params.screen,
            email : '',
            password : '',
            loginValidated : false,
            usernameError: true,
            passwordError: true,
            isShowPassword : true,
            loaded : true
        }
        
        this.configureNotification();
    }

    configureNotification(){
        OneSignal.init("d19ded94-8c9f-4793-b26f-dace0ef351bf");
        OneSignal.configure();
        OneSignal.addEventListener('ids', this.onIds);
    }

    onIds(device) {
        global.deviceId = device.userId;
        console.log('device',device.userId);
    }

    __screenHeading(){
        return this.state.login === 'restaurant-login' ? 'Restaurant Login' : 'Login';
    }

    __facebookLogin(){
        let that = this;
		FBLoginManager.loginWithPermissions(['email','public_profile'], function(error, data){
            if (!error) {
                
              //  Alert.alert('Message', 'outer');
              console.log('Login data: ', data.credentials.token);
              if(data.credentials.token != undefined){
                that.__activeLoader();
                Guest.facebookLogin(data.credentials.token.toString()).then(response => {
                    that.__deactiveLoader();
                    console.log(response);
                    //Alert.alert('Message', 'inner');
                    if(response.status){
                        if(response.registeration_required){
                            
                    //Alert.alert('Message', 'response.registeration_required');
                            let resgistrationParams = {
                                name : response.data.name,
                                email : response.data.email 
                            }
                            if(response.message){
                                Alert.alert('Alert!',response.message);
                            }
                            else{
                                that.__go('register', { screen : that.state.login, data : resgistrationParams  } )
                            }
                        } else{
                            that.setState({
                                email : response.user.email
                            });
                           // Alert.alert('Message', 'response.data.email');
                        }
                    } else{
                        Alert.alert('Error', response.message);
                    }
                }).catch(error => {
                    console.log(error);
                    Alert.alert('Error', 'Please Try Again!');
                    that.__deactiveLoader();
                })
              }
            } else {
              console.log('Error: ', error);
              Alert.alert('Error', 'Unable To Connect With Facebook At The Moment');
            }
        })
          
    }

    __validateLogin() {
    	
    	let errorCount = 0;
    	let errors = {};

    	if(!Validator.isEmail(_.trim(this.state.email))) {
    		errorCount++;
            errors.usernameError = true;
            
    	} else {
            errors.usernameError = false;
            
    	}

    	if(Validator.isEmpty(this.state.password)) {
    		errorCount++;
    		errors.passwordError = true;
    	} else {
    		errors.passwordError = false;
    	}

    	errors.loginValidated = true;

    	this.setState(errors);

    	return errorCount == 0 ? true : false; 

    }

    __login(){
        let that = this;
        if(this.__validateLogin()){
            that.__activeLoader();
            Guest.login(this.state.email,this.state.password).then(response => {
                console.log(response);
                that.__deactiveLoader();
                if(response.status){
                    global.userData = response.user;
                    global.userData.token = response.token;
                    
                    AsyncStorage.setItem('user', JSON.stringify(global.userData));
                    AsyncStorage.setItem('isUserLoggedIn', '1');
                    AsyncStorage.setItem('token',  response.token);
                    if(response.user.role === 'restaurant'){
                        global.drawerComponent.setState({
                            hideNavigation : false,
                            isUserLoggedIn: true,
                            userEmail :  response.user.email,
                            userName  :  response.user.name,
                            profileImageUrl : response.user.profile_image_url,
                            restaurant : this.__getCurrentUserRestaurants(),
                        });
                        that.__go('RestaurantList');
                    } else{
                        that.__goAndReset('MainTabs');
                    }
                } else{
                    Alert.alert('Error', response.message);
                }
            }).catch(error => {
                console.log(error);
                that.__deactiveLoader();
                Alert.alert('Error', 'Please Try Again!');
            })
        }
    }

    render() {
        const PageLoader = (props) => this.__loader();
		return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                <View style={ styles.loginMainContent }>
                    <View style={ styles.loginHeadingView }>
                        <Text style={ styles.loginHeadingView.text }>
                            {
                                this.__screenHeading()
                            }
                        </Text>
                    </View>
                    <View style={ styles.loginFormView }>
                        <View>
                            <Text style={ styles.loginFormView.label }>{ 'Email Address' }</Text>
                            <View style={ [ styles.loginFormView.textField, { flexDirection : 'row' }] }>
                                <TextInput value={this.state.email} ref={ input => { this.inputs['email'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('password') } onChangeText={ (text) => this.setState({ email : text }) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,{ fontSize : 20,width : '90%' } ] } placeholder={ 'yourname@mail.com' }></TextInput>
                                {this.__showValidationIcon(this.state.usernameError, this.state.loginValidated)}
                            </View>
                            
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Password' }</Text>
                            <View style={ [ styles.loginFormView.textField, {flexDirection : 'row'} ] }>
                                <View style={ { flex : 8, flexDirection : 'row' } }>
                                    <TextInput onSubmitEditing={ () => this.__login() } ref={ input => { this.inputs['password'] = input; }} onChangeText={ (text) => this.setState({ password : text }) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,{ fontSize : 20, width : '100%' } ] } secureTextEntry={this.state.isShowPassword} placeholder={ 'password' }></TextInput>
                                    {this.__showValidationIcon(this.state.passwordError, this.state.loginValidated)}
                                </View>
                                <CTouchable onPress={ () => this.setState( { isShowPassword : !this.state.isShowPassword } ) } style={ {flex:2, marginTop : 10} }>
                                    <Icon name="eye" color={'#9b9b9b'} size={25}></Icon>
                                </CTouchable>
                            </View>
                        </View>
                        <View style={ [ styles.right, { marginVertical : 10 } ] }>
                            <CTouchable onPress={ () => this.__go('forgetPassword') }>
                                <Text style={ [ styles.defaultTextColor,styles.defaultBoldText,{ fontSize : 18 } ] }>
                                    { 'forget password?' }
                                </Text>
                            </CTouchable>
                        </View>
                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__login()}>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'LOGIN'} </Text>
                                </CTouchable>
                            </View>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#3b5998'} ] } onPress={ () => this.__facebookLogin()}>
                                    <Text style={ [ styles.defaultBoldText,{ color : '#3b5998', fontSize : 18 } ] }> {'Continue with facebook'} </Text>
                                </CTouchable>
                            </View>
                            <View style={ styles.center }>
                                <CTouchable onPress={ () => this.setState( {  login : this.state.login === 'restaurant-login' ? 'normal-login' : 'restaurant-login'  } ) }>
                                    <Text style={ [ styles.defaultBoldText, { color : '#5e5e5e', borderBottomColor : '#5e5e5e', borderBottomWidth : 2 } ] }>
                                        {'switch to '}{ this.state.login === 'restaurant-login' ? 'login' : 'restaurant login' }
                                    </Text>
                                </CTouchable>
                            </View>
                        </View>
                        <View style={ [ styles.center, { backgroundColor : '#fff',marginTop : 20} ] }>
                            <View style={ { marginBottom : 10 } }>
                                <Text style={ [ styles.defaultBoldText, { color : '#5e5e5e' } ] }>{'Dont have account ? '}
                                    <Text onPress={ () => this.__go('register', { screen : this.state.login } ) } style={ [ styles.defaultBoldText,styles.defaultTextColor ] }>{'Register'}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
