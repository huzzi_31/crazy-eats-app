import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions,FlatList} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cart,Header,MenuCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {listing} from './../Models/listing';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export class MenuScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.go = this.__go.bind(this);
        this.state = {
            reviewMessage : '',
            activeMenu : 1,
            restaurantId : this.props.navigation.state.params.restaurant_id,
            data : [],
            loaded :  false,
            menus : [],
            isMenuEdit : this.props.navigation.state.params.restaurant_id === parseInt(global.userData.restaurant_id) ? true : false
        }
        global.menus.on('getMenus', () => {
            this.__getMenus();
        }); 
        this.__getMenus();
       
    }

    __activeMenu(_step){
        this.setState( { activeMenu : _step, menus : this.state.data.filter((e) => e.id === _step)[0].menus } )
    }

    __getMenus(){
        this.__activeLoader();
        listing.listMenus(this.state.restaurantId).then( response => {
            this.__deactiveLoader();
            if(response.menus){
                this.setState( { data : response.menus, activeMenu : response.menus[0].id, menus : response.menus[0].menus,   } ) 
            }
        } ).catch( error => {
            this.__deactiveLoader();
        } )
    }   

    render() {
        const PageFooter = (props) => 
            <View>
                <CTouchable onPress={ () => this.__back() } style={ [ styles.detailFooterbtns, { backgroundColor : '#fff', width : '100%', height : 150, paddingVertical : 25} ] }>
                    <Text style={ [ { color : '#8e8e8e', fontSize : 18 } ] }>{'Cancel'}</Text>
                </CTouchable>
                <CTouchable activeOpacity={ 0.5 } style={ [ styles.FloatBtnFixed,{ flexDirection : 'row' } ] } onPress={() => this.__go('AddMenu')}>
                    <Icon name={'plus'} size={20} color={'#fff'} />
                    <Text style={ [ { color : '#fff', fontSize : 15 } ] }>{'Add Menu'}</Text>
                </CTouchable>
            </View>;
        const PageLoader = (props) => this.__loader();
       return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : this.state.isMenuEdit ? <PageFooter /> : null}>
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{'Menu'}</Text>
                    </View>
               </Header>
                {/* header */}
                <View style={ [ styles.mainContentMenu ] }> 
                    <View style={ [ styles.menuTitleBar ] }>
                        {
                            this.state.data.map((category, index) => {
                                return(
                                    <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === category.id ? styles.activeMenu : '', { flex : 3 } ] }>
                                        <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(category.id) }>
                                            <Text style={ [ styles.menuTitleBar.text ] }>{ category.name }</Text>
                                        </CTouchable>
                                    </View>
                                )
                            })
                        }
                    </View>
                    <View style={ [ styles.menuCartView ] }>
                        {
                            this.state.menus.length > 0 ?
                                <FlatList 
                                    extraData={this.state}
                                    data={this.state.menus}
                                    renderItem={({item}) =>
                                        <MenuCart edit={this.state.isMenuEdit} navigator={this.go} item={item} />
                                    }   
                                    keyExtractor={(item, index) => index} 
                                /> :
                            <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                                <Text style={ [ styles.defaultBoldText, { color : '#292929' }] }>{ 'No Menus Found!' }</Text>
                            </View>
                        }
                    </View>
                </View>
            </Wrapper>
        );
    }
}
