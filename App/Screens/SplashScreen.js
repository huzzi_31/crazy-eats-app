import React from 'react';
import {Image, View, Alert, AsyncStorage,Text,ImageBackground} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable} from '../Components';

export class SplashScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            isUserLoggedIn : false
        }
        AsyncStorage.getItem('user').then(response => {
            console.log(response)
            try {
                let responseJson = JSON.parse(response);
                if(responseJson.id !== undefined && responseJson.id !== 0) {
					global.userData = responseJson;
                    this.setState( { isUserLoggedIn : true } )
                    global.isUserLoggedIn = true;
                    if(responseJson.role === 'restaurant'){
                        global.drawerComponent.setState({
                            hideNavigation : false,
                            isUserLoggedIn: global.isUserLoggedIn,
                            userEmail : responseJson.email,
                            userName : responseJson.name,
                            profileImageUrl : responseJson.profile_image_url,
                        });
                    }
                    this.__goAndReset('MainTabs');
                }
            } catch(e) {}
           
        });

        setTimeout( () => { 
            if(!global.isLocationEnable){
                Alert.alert('Warning','We are unable to locate you for better result you need to enable GPS settings for this app.');
                if( global.isUserLoggedIn ){
					this.__goAndReset('MainTabs');
				}
            }
        }, 25000);
    }

    render() {
        return (
            <View style={ styles.splashContainer }>
                <ImageBackground source={require('../../assets/img/splash_background.jpg')} style={styles.backgroundImage}>
                    <View style={ styles.splashLogoView }>
                        <Image style= { {resizeMode :'contain', height:'40%' } } source={require('../../assets/img/logo.png')}></Image> 
                    </View>
                    {
                        this.state.isUserLoggedIn === false ?
                            <View style={ {justifyContent : 'center', alignItems : 'center', flex : 0.50, } }>
                                <Text style={ [ styles.splashScreenText, { fontSize : 40 }  ] }> Are you ?</Text>
                            </View>
                            :
                        null
                    }
                    {
                        this.state.isUserLoggedIn === false ?
                            <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1, } }>
                                <CTouchable style={ [ styles.button, {backgroundColor : '#f26122'} ] } onPress={ () => this.__goAndReset('login',{screen : 'normal-login'})}>
                                    <Text style={ [ styles.splashScreenText ] }> {'Foodie ?'}</Text>
                                </CTouchable>
                                <CTouchable style={ [ styles.button, { backgroundColor : '#51b845' } ] } onPress={ () => this.__goAndReset('login',{screen : 'restaurant-login'})}>
                                    <Text style={ [ styles.splashScreenText ] }> {'Restaurant ?'}</Text>
                                </CTouchable>
                                <View style={ { marginTop : 5 } }>
                                    <Text style={ [ styles.splashScreenText,{ color : '#51b845' } ] }> {'Entree + dessert or starters, only $10'}</Text>
                                </View>
                            </View> 
                            :
                        null
                    }
                </ImageBackground>
            </View>
        );
    }
}
