import React from 'react';
import {Image, View, Alert, Text,TextInput,Modal,FlatList,Picker} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header,ReviewCart,Cuisines} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {Validator} from '../Utils/Validator';
import {Guest} from '../Models/Guest';
import ImagePicker from 'react-native-image-picker';
import {listing} from '../Models/listing';
import ImageResizer from 'react-native-image-resizer';
var states = require('../../state.json');
export class ProfileScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.ratings = this.__ratings.bind(this);
        this.contentDescription = this.__contentDescription.bind(this);
        this.addvote = this.__addVote.bind(this);
        this.state = {
            reviewMessage : '',
            activeMenu : 1,
            isUserProfile : global.userData.role === 'restaurant' ? false : true,
            isOpen: false,
            isDisabled : false,
            swipeToClose : true,
            userData : [],
            loaded : false,
            modalVisible : false,
            currentEdit : '',
            reviews : [],
            isUserField : false,
            restaurant : {
                name: '',
                logo_url: '',
                banner_urls: []
            },
            params : {
                field : '',
                value : '',
            },
            cityParams : {
                fields: {
                    state : '',
                    city : ''
                }
            },
            updateValueError : true,
            currentEditValue : '',
            years : [],
            months : [],
            days : [],
            month : 1,
            year : 1970,
            day : 1,
            statesList : states,
            city : '',
            state : states[0].name,
            martial_status : '',
            currentImageGallery : [],
            isgalleryModal : false,
            cuisines : [],
            isCuisinesEdit : false,
            filterToggle : false,
            cuisine_ids : [],
			reviewGalleryModal : false,
			reviewGalleryModalImage : '',
        };
        global.restaurtant.on('updateRestaurant', () => {
            this.__getRestaurant();
        }); 
        this.__getRestaurant();
        this.__setDates();
        this.__getCuisines();
      
    }

    __getCuisines(){
        listing.getCuisines().then( response => {
            console.log(response);
            if(response.cuisines){
                this.setState( { cuisines : response.cuisines } )
            }
        } ).catch( error => {
            Alert.alert('Error','Please Try Again');
        } )  
    }

    __activeFilters(id, _step, toggle){
        const __step = [...this.state.cuisine_ids];

        if(toggle){
            __step.push(id);
        } else{
            var index = __step.indexOf(id)
            __step.splice(index, 1);
        }

        let params = this.state.params;
        params.field = 'cuisine_ids';
        params.value = __step;
        
        this.setState( { cuisine_ids : __step, params : params },() => {
            this.__edit(this.state.params);
        } )
    }

    __addVote(params){
        this.__activeLoader();
        listing.vote(this.state.restaurantId,params,'review').then( response => {
            if(response.status === false){
                Alert.alert('Error', response.message)
            } else{
                if(response.reviews.length > 0){
                    this.setState( { reviews : response.reviews, } );
                }
            }
            this.__deactiveLoader();
        } ).catch( error => {
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        } )
    }

   __renderProfileDetail(){
       if(!this.state.isUserProfile){
           return(
                <View style={ { flexDirection : 'column', paddingHorizontal : 15 } }>
                    <View style={ [ styles.profileInfoParentView, ] }>
                        <View style={ { flex : 0.9, flexDirection : 'row' } }>
                            <Text style={ [ styles.profileInfoHeading ] }>{ 'Restaurant Address' }</Text>
                        </View>
                        <View style={ { flex : 0.1, justifyContent : 'flex-end', alignItems : 'flex-end' } }>
                            <CTouchable onPress={ () => this.__onEditModal('address', this.state.restaurant.address ) } >
                                <MaterialIcon name={'edit'}  size={15}  />
                            </CTouchable>
                        </View>
                    </View>
                    <View>
                        <Text style={ [ styles.profileInfoText ] }>{ this.state.restaurant.address ? this.state.restaurant.address : 'N/A' }</Text>
                    </View>
                    <View style={ [ styles.profileInfoParentView ] }>
                        <View style={ { flex : 0.9 } }>
                            <Text style={ [ styles.profileInfoHeading ] }>{ 'Short Description' }</Text>
                        </View>
                        <View style={ { flex : 0.1, justifyContent : 'flex-end', alignItems : 'flex-end' } }>
                            <CTouchable onPress={ () => this.__onEditModal('description', this.state.restaurant.description ) } >
                                <MaterialIcon name={'edit'}  size={20}  />
                            </CTouchable>
                        </View>
                    </View>
                    <View>
                        <Text style={ [ styles.profileInfoText ] }>{ this.state.restaurant.description }</Text>
                    </View>
                    <View style={ [ styles.profileInfoParentView ] }>
                        <View style={ { flex : 1 } }>
                            <Text style={ [ styles.profileInfoHeading ] }>{ 'Created By' }</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={ [ styles.defaultBoldText, { fontSize : 12 } ] }>{ this.state.userData.name + ' - Owner' }</Text>
                    </View>
                </View>
           )
       } else{
           return(
               null
           )
       }
   }

   __getRestaurant(){
       this.__activeLoader();
        Guest.me().then( response => {
            this.__deactiveLoader();
            console.log('huz',response)
            if(response.me){
                this.__setState(response);
            } else{
                Alert.alert('Error', response.message);
            }
        } ).catch( error => {
                this.__deactiveLoader();
        } )
        
    }

    __validate() {
    	
    	let errorCount = 0;
    	let errors = {};

    	if(Validator.isEmpty(this.state.params.value) ) {
    		errorCount++;
    		errors.updateValueError = true;
    	} else {
    		errors.updateValueError = false;
        }

    	errors.updateFieldErrors = true;

    	this.setState(errors);

    	return errorCount == 0 ? true : false; 

    }

    __onEditModal(field, value, isUserField = false){
        let params = this.state.params;
        params.field = field;
        params.value = value;
        this.setState( { params : params, modalVisible : true, isUserField : isUserField, isCuisinesEdit : false } )
    }

    __closeModal(){
        let params = this.state.params;
        params.field = '';
        params.value = '';
        this.setState( { params : params, modalVisible : false, isCuisinesEdit : false } )
    }

    __setState(response){
        this.setState( { userData : response.me, reviews : response.me.reviews, restaurant : response.me.restaurant, year : response.me.dob_y ? parseInt(response.me.dob_y) : 1970, month : response.me.dob_m ? parseInt(response.me.dob_m) : 1, day : response.me.dob_d ? parseInt(response.me.dob_d) : 1, city : response.me.city != null ? response.me.city : '', state : response.me.state != null ? response.me.state : '', martial_status : response.me.martial_status, cuisine_ids : this.state.isUserProfile ? response.me.cuisine_ids :  response.me.restaurant.cuisine_ids   } )
    }

    __update(params, validate = true){
        let param = this.state.params;
        if(param.field === 'location'){
            let cityParams = this.state.cityParams;
            cityParams.fields.state = this.state.state;
            cityParams.fields.city = this.state.city;
            this.setState( { cityParams : cityParams }, () => this.__edit(this.state.cityParams) )

        }  else if(params.field === 'dob'){
            params.value = this.__date();
            this.setState( { params : param }, () => this.__edit(this.state.params) )

        } else if(params.field === 'martial_status'){
            params.value = this.state.martial_status;
            this.setState( { params : param }, () => this.__edit(this.state.params) )

        } else {
            if(this.__validate()){
                this.__edit(params)
            }
        }
    }

    __date(){
        return parseInt(this.state.year) + '-' + parseInt(this.state.month) + '-' + parseInt(this.state.day);
    }

    __setDates(){
        let currentYear = new Date().getFullYear();
        for (var i = 1970; i <= currentYear; i++) {
			let array = this.state.years;
			array.push(i);
			this.setState({
				array
			});
        }
        for (var i = 1; i <= 12; i++) {
            let months = this.state.months;
			months.push(i);
			this.setState({
				months
			});
        }
        for (var i = 1; i <= 31; i++) {
            let days = this.state.days;
			days.push(i);
			this.setState({
				days
			});
        }
	}
    
    __onMonthChange(value){
        this.setState( { 
            month : value,
            days : []
        }, () => this.__validateDate(value) )
     }
 
     __validateDate(value){
         var selectedYear = this.state.year;
         var selectedMonth = value;
         var day = 31;
         if(selectedMonth === 2){
            day = selectedYear % 4 === 0 ? 29 : 28;
         } else if(selectedMonth === 2 || selectedMonth === 4 || selectedMonth === 6 || selectedMonth === 9 || selectedMonth === 11){
            day = 30;
         };
         
         for (var i = 1; i <= day; i++) {
            let days = this.state.days;
            days.push(i);
            this.setState({
                days
            });
         }
    }

    __renderEditField(){
        if(this.state.params.field === 'dob'){
            return(
                <View style={ { flexDirection : 'row' } }>
                    {/* year */}
                    <View style={ [ styles.dobFields, { flex : 4 } ] }>
                        <Text style={ styles.loginFormView.label }>{ 'Year' }</Text>
                        <View style={ [ styles.loginFormView.textField ] }>
                            <Picker selectedValue = {this.state.year} onValueChange = {(value) => this.setState( { year :  value } )}>
                                {   
                                    this.state.years.map( ( item, index ) => {
                                        return (
                                            <Picker.Item label={item + ''} value={item} />   
                                        )
                                    } )
                                }
                            </Picker>  
                        </View>
                    </View>
                    {/* year */}
                    {/* month */}
                    <View style={ [ styles.dobFields, { flex : 3 } ] }>
                        <Text style={ styles.loginFormView.label }>{ 'Month' }</Text>
                        <View style={ [ styles.loginFormView.textField ] }>
                            <Picker selectedValue = {this.state.month} onValueChange = { (value) => this.__onMonthChange(value) }>
                                {
                                    this.state.months.map( ( item, index ) => {
                                        return (
                                            <Picker.Item label={item + ''} value={item} />   
                                        )
                                    } )
                                }
                            </Picker>  
                        </View>
                    </View>
                    {/* day */}
                    <View style={ [ styles.dobFields, { flex : 3} ] }>
                        <Text style={ styles.loginFormView.label }>{ 'Day' }</Text>
                        <View style={ [ styles.loginFormView.textField ] }>
                            <Picker selectedValue={this.state.day} onValueChange = {(value) => this.setState( { day :  value } )}>
                                {
                                    this.state.days.map( ( item, index ) => {
                                        return (
                                            <Picker.Item label={'' +item +''} value={item} />   
                                        )
                                    } )
                                } 
                            </Picker>  
                        </View>
                    </View>
                    {/* day */}
                
                </View> 
            )
        } else if(this.state.params.field === 'location'){
            return(
                <View style={ { flexDirection : 'row' } }>
                    <View style={ { marginTop : 15, flex : 0.5 } }>
                        <Text style={ styles.loginFormView.label }>{ 'State' }</Text>
                        <View style={ [ styles.loginFormView.textField ] }>
                            <Picker selectedValue = {this.state.state} onValueChange = {(value) => this.setState( { state :  value } )}>
                                {
                                    this.state.statesList.map((item, index) => {
                                        return (
                                            <Picker.Item label={item.name} value={item.name} />
                                        ) 
                                    })
                                }
                            </Picker>  
                        </View>
                    </View>
                    <View style={ { marginTop : 15, flex : 0.5 } }>
                        <Text style={ styles.loginFormView.label }>{ 'City' }</Text>
                        <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                            <TextInput value={ this.state.city } ref={ input => { this.inputs['city'] = input; }}  onChangeText={ (text) => this.setState( { city : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Enter City' }></TextInput>
                            {this.__showValidationIcon(this.state.cityError, this.state.formValidated)}
                        </View>
                    </View>
                </View>
            )
        } else if(this.state.params.field === 'martial_status'){
            return(
                <View style={ {marginTop : 15} }>
                    <Text style={ styles.loginFormView.label }>{ 'Martial Status' }</Text>
                    <View style={ [ styles.loginFormView.textField ] }>
                        <Picker selectedValue={this.state.martial_status} onValueChange = {(value) => this.setState( { martial_status :  value } )}>
                            <Picker.Item label="Single"   value="Single" />
                            <Picker.Item label="Married"  value="Married" />
                            <Picker.Item label="Divorced" value="Divorced" />
                            <Picker.Item label="Widowed"  value="Widowed" />
                        </Picker>  
                    </View>
                </View>
            )
        } else{
            return(
                <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                    <TextInput keyboardType={ this.state.params.field === 'zip_code' ? 'numeric' : 'default' } value={this.state.params.value} onSubmitEditing={ () => this.__update(this.state.params) } onChangeText={ (text) => {
                    let params = this.state.params;
                    params.value = text;
                    this.setState( { params : params } )
                    } } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Edit' } ></TextInput>
                    {this.__showValidationIcon(this.state.updateValueError, this.state.updateFieldErrors)}
                </View>
            )
        }
        
    }

    __edit(params){
        this.__activeLoader();
        this.setState( { modalVisible : false } )
        let role = this.state.isUserProfile ? 'user' : this.state.isUserField ? 'user' : 'restaurant';
        Guest.updateField(role,params).then( response => {
            this.__deactiveLoader();
            if(response.me){
                this.__setState(response);
            } else{
                Alert.alert('Error', response.message);
            }
        } ).catch( error => {
            this.__deactiveLoader();
        } )
    }

    __openImagePicker(options,save_type,field_name) {
        ImagePicker.showImagePicker(options, (image) => {
            if (image.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (image.error) {
              console.log('ImagePicker Error: ', image.error);
            }
            else if (image.customButton) {
              console.log('User tapped custom button: ', image.customButton);
            }
            else {
                this.__activeLoader();
                let originalRotation = image.originalRotation;
                let uri = image.uri;
                let width = image.width;
                let height = image.height;
                let formatType = image.type;
                
                ImageResizer.createResizedImage( uri, width, height , 'JPEG', 80, originalRotation === 90 ? 90 : (originalRotation === 270 ? -90 : 0) ).then( uri => {
                    
                    listing.getAttachmentUrls(uri.uri, formatType, image.fileName, save_type).then(response  => {
                        this.__deactiveLoader();
                        if(response.status === 1){
                            let params = {
                                field : field_name,
                                value : response.image_path
                            };
                            this.__edit(params);
                        } else{
                            Alert.alert('Error', response.data.error ? response.data.error : 'Internal Server Error' )
                        }
                    }).catch(error =>{
                        this.__deactiveLoader();
                        Alert.alert("Error", "Please Check Your Connection!");
                    })
                } ).catch( err => {
                    this.__deactiveLoader();
                    console.log( err );
                    Alert.alert( 'Unable to resize the photo', 'Please try again!' )
                } )
            }
        });
    }

    __userStats(){
        return(
            
            <View style={ { flexDirection : 'row', justifyContent : 'center', marginVertical : 6 } }>
                <CTouchable onPress={ () => this.__go( 'followers', { id : this.state.userData.id, label : 'Followers' } ) }  style={ [ styles.followBtn , {  backgroundColor : '#129eed', flexDirection : 'row' } ] }>
                    <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{'Followers'}</Text>
                    <Text style={ [ styles.defaultBoldText, { color : '#fff', marginLeft : 5 } ] }>{ this.state.userData.followers_count ? '(' + this.state.userData.followers_count + ')' : '(0)' }</Text>
                </CTouchable>
                <CTouchable onPress={ () => this.__go( 'followers', { id : this.state.userData.id, label : 'Following' } ) }  style={ [ styles.followBtn , {  backgroundColor : '#fd3c00', flexDirection : 'row', borderColor : '#fd3c00' } ] }>
                    <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{'Followings'}</Text>
                    <Text style={ [ styles.defaultBoldText, { color : '#fff', marginLeft : 5 } ] }>{ this.state.userData.following_count ? '(' + this.state.userData.following_count + ')' : '(0)' }</Text>
                </CTouchable>
            </View>
        )
    }

    render() {
        const PageFooter = (props) => 
        <View>
            <CTouchable onPress={ () => this.__go('DetailScreen', { data : this.state.userData.restaurant, isEdit : true }) } activeOpacity={ 0.5 } style={ [ styles.FloatBtnFixed,{ flexDirection : 'row' } ] }>
                <Text style={ [ { color : '#fff', fontSize : 15 } ] }>{'View Restaurant'}</Text>
            </CTouchable>
        </View>;
        const PageLoader = (props) => this.__loader();
        let banner_urls = this.state.isUserProfile ? this.state.userData.banner_image_url : this.state.restaurant.banner_urls != undefined && this.state.restaurant.banner_urls[0] != undefined ? this.state.restaurant.banner_urls[0] : '';
       return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : !this.state.isUserProfile ? <PageFooter /> : null}>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }></View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{ 'Profile' }</Text>
                    </View>
                    <View style={ styles.header.right }>
                        <CTouchable  onPress={() => this.__logout()}>
                            <Icon name={'log-out'} size={30} color={'#000'} />
                        </CTouchable>
                    </View>
                </Header>
                {/* header */}
                <View style={ { width :'100%' } }>
                    <Image source={ { uri : banner_urls  } } style={ { height : 220, width : '100%' } } />
                    <View style={ { position : 'absolute', top : 0, left : 0, right : 10, bottom : 90, alignItems : 'flex-end', justifyContent : 'flex-end' } }>
                        <CTouchable onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, this.state.isUserProfile ? false : 'RB', this.state.isUserProfile ? 'banner_image_path' : 'banner_path') } >
                            <MaterialIcon name={'edit'} color={'#fff'} size={30}  />
                        </CTouchable>
                    </View>
                    <View style={ { marginTop : 80, justifyContent : 'center' } }>
                        <View style={ { position : 'absolute', left : 0, right : 0, bottom : 0, justifyContent : 'flex-end', alignItems : 'center', paddingVertical :10, paddingHorizontal : 10, } }>
                            <View>
                                <Image 
                                    source={ { uri : this.state.isUserProfile ? this.state.userData.profile_image_url : this.state.restaurant.logo_url } } 
                                    style={ { borderRadius : 50, width : 100, height : 100 } }
                                />
                                <View style={ {  position : 'absolute',  right : 0, bottom : 0,  } }>
                                    <CTouchable style={ [ styles.iconShadow, { borderRadius : 50, backgroundColor : '#fff', padding : 5 } ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, this.state.isUserProfile ? 'UP' : 'RL', this.state.isUserProfile ? 'profile_image_path' :'logo_path') } >
                                        <MaterialIcon name={'edit'}  size={18}  />
                                    </CTouchable>
                                </View>
                            </View>
                            <View style={ { flexDirection : 'row' } }>
                                <Text style={ [ styles.defaultBoldText, { fontSize : 22, color:'#000', marginTop : 5 } ] }>{this.state.isUserProfile ? this.state.userData.name : this.state.restaurant.name}</Text>
                                <CTouchable onPress={ () => this.__onEditModal( 'name', this.state.isUserProfile ? this.state.userData.name : this.state.restaurant.name) } >
                                    <MaterialIcon name={'edit'}  size={20}  />
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
                {
                    this.state.isUserProfile ?
                        this.__userStats() :
                    null
                }
                <View style={ { justifyContent : 'center', alignItems : 'center', marginVertical : 15 , flexDirection : 'row'} }>
                    {
                        !this.state.isUserProfile ?
                            <CTouchable style={ styles.restaurantProfileMenusBtn } onPress={() => this.__go('AddMenu')}>
                                <Image source={ require('../../assets/img/menuicon.png') } />
                                <Text style={ [ styles.defaultBoldText,styles.restaurantProfileMenusBtn.label, ] }>{ 'Add Menu' }</Text>
                            </CTouchable> :
                        null
                    }
                    {
                        !this.state.isUserProfile ?
                            <CTouchable style={ styles.restaurantProfileMenusBtn } onPress={() => this.__go('restaurtantHour',{ data : this.state.restaurant })}>
                                <Icon name={ "clock" } size={32} />
                                <Text style={ [ styles.defaultBoldText,styles.restaurantProfileMenusBtn.label, ] }>{ 'Edit Hours' }</Text>
                            </CTouchable> :
                        null
                    }
                    <CTouchable style={ styles.restaurantProfileMenusBtn } onPress={() => this.__go('updatePassword',{ user : this.state.userData })}>
                        <Icon name={ "lock" } size={32} />
                        <Text style={ [ styles.defaultBoldText,styles.restaurantProfileMenusBtn.label, ] }>{ 'Password' }</Text>
                    </CTouchable>
                   
                </View>
                {
                    !this.state.isUserProfile ?
                        <View style={ { paddingVertical : 10,  } }>
                            <View style={ { flexDirection : 'row', } }>
                                <View style={ [ styles.restaurantStats, { backgroundColor : '#fd3c00' } ] }>
                                    <Text style={ [ styles.restaurantStats.text ] }>{'No .Of Views'}</Text>
                                    <Text style={ [ styles.restaurantStats.heading ] }>{this.state.restaurant.views}</Text>
                                </View>
                                <View style={ [ styles.restaurantStats, { backgroundColor : '#00AFF0' } ] }>
                                    <Text style={ [ styles.restaurantStats.text ] }>{'Favorite'}</Text>
                                    <Text style={ [ styles.restaurantStats.heading ] }>{this.state.restaurant.no_of_favorite}</Text>
                                </View>
                            </View>
                        </View>
                    :
                    null
                }
                <View style={ [ styles.mainContentReviews, ] }> 
                    <View style={ { flexDirection : 'column', paddingHorizontal : 15 } }>
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ 'Email' }</Text>
                            </View>
                            <View style={ [ styles.profileInfoView] }>
                                <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.email ? this.state.userData.email : 'N/A' }</Text>
                            </View> 
                        </View>
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ 'Date Of Birth' }</Text>
                            </View>
                            <View style={ [ styles.profileInfoView, { flexDirection : 'row' }  ] }>
                                <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.dob ? this.state.userData.dob : 'N/A' }</Text>
                            </View>
                            <CTouchable onPress={ () => this.__onEditModal('dob', this.state.userData.dob, true ) } >
                                <MaterialIcon name={'edit'}  size={12}  />
                            </CTouchable>
                        </View>
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ 'phone' }</Text>
                            </View>
                            <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.phone ? this.state.userData.phone : 'N/A' }</Text>
                            </View> 
                            <CTouchable onPress={ () => this.__onEditModal('phone', this.state.userData.phone, true ) } >
                                <MaterialIcon name={'edit'}  size={12}  />
                            </CTouchable>
                        </View>
                        {
                            this.state.isUserProfile ?
                                <View style={ [ styles.profileInfoParentView ] }>
                                    <View style={ [ styles.profileInfoView ] }>
                                        <Text style={ [ styles.profileInfoHeading ] }>{ 'Marital status' }</Text>
                                    </View>
                                    <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                        <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.martial_status ? this.state.userData.martial_status : 'N/A' }</Text>
                                    </View>
                                    <CTouchable onPress={ () => this.__onEditModal('martial_status', this.state.userData.phone, true ) } >
                                        <MaterialIcon name={'edit'}  size={12}  />
                                    </CTouchable>
                                </View>
                            :
                                null
                        }
                        {
                            this.state.isUserProfile ?
                                <View style={ [ styles.profileInfoParentView ] }>
                                    <View style={ [ styles.profileInfoView ] }>
                                        <Text style={ [ styles.profileInfoHeading ] }>{ 'Address' }</Text>
                                    </View>
                                    <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                        <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.address ? this.state.userData.address : 'N/A' }</Text>
                                    </View>
                                    <CTouchable onPress={ () => this.__onEditModal('address', this.state.userData.address, true ) } >
                                        <MaterialIcon name={'edit'}  size={12}  />
                                    </CTouchable>
                                </View> 
                            :
                                null
                        }
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ 'Location' }</Text>
                            </View>
                            <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.location ? this.state.userData.location : 'N/A' }</Text>
                            </View>
                            <CTouchable onPress={ () => this.__onEditModal('location', this.state.userData.location, true ) } >
                                <MaterialIcon name={'edit'}  size={12}  />
                            </CTouchable>
                        </View> 
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ 'Zip Code' }</Text>
                            </View>
                            <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.zip_code ? this.state.userData.zip_code : 'N/A' }</Text>
                            </View>
                            <CTouchable onPress={ () => this.__onEditModal('zip_code', this.state.userData.zip_code, true ) } >
                                <MaterialIcon name={'edit'}  size={12}  />
                            </CTouchable>
                        </View>
                        <View style={ [ styles.profileInfoParentView ] }>
                            <View style={ [ styles.profileInfoView ] }>
                                <Text style={ [ styles.profileInfoHeading ] }>{ this.state.isUserProfile ? 'Favorite Foods' : 'Cuisines'  }</Text>
                            </View>
                            {
                                this.state.isUserProfile ?
                                    <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                        <Text style={ [ styles.profileInfoText ] }>{ this.state.userData.cuisine_text ? this.state.userData.cuisine_text : 'N/A' }</Text>
                                    </View>
                                :
                                    <View style={ [ styles.profileInfoView, { flexDirection : 'row' } ] }>
                                        <Text style={ [ styles.profileInfoText ] }>{ this.state.restaurant.cuisine_text ? this.state.restaurant.cuisine_text : 'N/A' }</Text>
                                    </View>
                            }
                            <CTouchable onPress={ () => this.setState( { modalVisible : true, isCuisinesEdit : true } ) } >
                                <MaterialIcon name={'edit'}  size={12}  />
                            </CTouchable>
                        </View>
                    </View>
                    {
                        this.__renderProfileDetail()
                    }
                    {
                        this.state.reviews.length > 0 ?
                            <View>
                                <View style={ { paddingVertical : 10, justifyContent : 'center', alignItems : 'center' } }>
                                    <Text style={ [ styles.defaultBoldText, { color : '#000000', fontSize : 18 } ] }>{ 'User Reviews' }</Text>
                                </View>
                                <FlatList 
                                    extraData={this.state}
                                    data={this.state.reviews}
                                    renderItem={({item}) =>
                                        <ReviewCart parent={this} currentUser={true} item={item} restaurant={item.restaurant} name={this.state.isUserProfile ? this.state.userData.name : item.user.name } go={this.__go.bind(this)} restaurantName={ this.state.isUserProfile ? item.restaurant.name : this.state.restaurant.name } ratings={this.ratings} content={this.contentDescription} vote={this.addvote} redirectToUser={false} image={this.state.userData.profile_image_url} />
                                    }   
                                    keyExtractor={(item, index) => index} 
                                /> 
                            </View>:
                        <View style={ { justifyContent : 'center', alignItems : 'center', marginTop : 15 } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#292929' }] }>{ 'No Reviews Yet!' }</Text>
                        </View>
                    }
                </View>
                <Modal transparent={true}  visible={this.state.modalVisible} animationType={'slide'} onRequestClose={ () => this.__closeModal() }>
                    <View style={styles.modalInner}>
                        <View style={ { width: 400, padding: 20, backgroundColor: '#ffffff', borderRadius: 15 } }>
                            <View style={ styles.validationIcon } >
                                <CTouchable onPress={() =>  this.__closeModal()}>
                                    <Icon name="x" color="#cccccc" size={22} />
                                </CTouchable>
                            </View>
                            {
                                this.state.isCuisinesEdit ?
                                    <View style={ { flexDirection : 'row', flexWrap : 'wrap' } }>
                                        {/* cuisines filters */}
                                        <Cuisines toggle={this.state.filterToggle} active={this.state.cuisine_ids} activeFilters={this.__activeFilters.bind(this)} cuisines={this.state.cuisines} />
                                        {/* cuisines filters */}
                                    </View>
                                :
                                    <View>
                                        <Text style={ styles.loginFormView.label }>{ 'Edit ' + this.state.params.field }</Text>
                                        {
                                            this.__renderEditField()
                                        }
                                    </View>
                            }
                            {
                                !this.state.isCuisinesEdit ?
                                
                                    <CTouchable style={ [ styles.defaultBtnBlock, { backgroundColor : '#fa5503', marginTop : 5 } ] } onPress={() => this.__update(this.state.params)}>
                                        <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] } >{'Submit'}</Text>
                                    </CTouchable>
                                :
                                null
                            }
                        </View>
                    </View>
                </Modal>
            </Wrapper>
            
           
        );
    }
}
