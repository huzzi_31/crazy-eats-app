import React from 'react';
import {View,Text,TextInput,Alert} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Header,Wrapper} from '../Components';
import {Validator} from '../Utils/Validator';
import {Guest} from '../Models/Guest';
import Icon from 'react-native-vector-icons/Feather';
import {
	LoginManager,
    LoginButton, 
    AccessToken
} from 'react-native-fbsdk';

export class UpdatePasswordScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            user : this.props.navigation.state.params.user,
            oldPassword : '',
            password : '',
            confirmPassword : '',
            updateValidated : false,
            old_passwordError: false,
            passwordError: false,
            confirmPasswordError : false,
            loaded : true,
            errors : {},
        }

    }

    __validateFields() {
    	
    	let errorCount = 0;
    	let errors = {};

    	if(Validator.isEmpty(this.state.oldPassword)) {
    		errorCount++;
    		errors.old_passwordError = true;
    	} else {
    		errors.old_passwordError = false;
        }
        
        if(Validator.isEmpty(this.state.password) || this.state.password.length < 7 ) {
    		errorCount++;
    		errors.passwordError = true;
    	} else {
    		errors.passwordError = false;
        }

        if(this.state.confirmPassword != this.state.password || Validator.isEmpty(this.state.confirmPassword)  ) {
            errorCount++;
    		errors.confirmPasswordError = true;
    	} else {
            errors.confirmPasswordError = false;
        }

    	errors.updateValidated = true;

    	this.setState(errors);

    	return errorCount == 0 ? true : false; 

    }

    __init(){
        this.setState( {
            oldPassword : '',
            password : '',
            confirmPassword : '',
            updateValidated : false,
            old_passwordError: false,
            passwordError: false,
            confirmPasswordError : false,
        } )
    }

    __updatePassword(){
       if(this.__validateFields()){
            let param = {
                password_confirmation : this.state.confirmPassword,
                password : this.state.password,
                old_password :  this.state.oldPassword,
            }
            this.__activeLoader();
            Guest.updatePassword(param).then( response => {
                this.__deactiveLoader();
                if(response.status){
                    Alert.alert('Success', response.message);
                    global.userData.token = response.token;
                    this.__logout()
                } else{
                    Alert.alert('Error', response.message);
                    if(response.errors){
                        this.setState( { errors : response.errors }, () => {
                            this.__errorHandling();
                        } );
                    }
                }
            } ).catch( error => {
                this.__deactiveLoader();
                Alert.alert( "Error", "Please Try Again!" )
            } )
       }
    }

    __errorHandling(){
        let validation = {};
        const errorsHandle = Object.keys(this.state.errors).map(key => 
            {   
                if(key === 'password'){
                    validation['confirmPasswordError'] = true
                }
                validation[key + 'Error'] = true;
                console.log(key + 'Error');
            }
        )
        validation['updateValidated'] =  true;
        this.setState(validation)
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                            <Icon name={'chevron-left'} size={25} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ [ styles.header.center, { flexDirection : 'row' } ] }>
                        <Text style={ [ styles.headerText, { color : '#000' } ] }>{ 'Update Password' }</Text>
                    </View>
                </Header>
                {/* header */}
                <View style={ styles.loginMainContent }>
                    <View style={ styles.loginFormView }>
                        <View>
                            <Text style={ styles.loginFormView.label }>{ 'Old Password' }</Text>
                            <View style={ [ styles.loginFormView.textField, { flexDirection : 'row' }] }>
                                <TextInput value={ this.state.oldPassword } secureTextEntry={true} ref={ input => { this.inputs['oldPassword'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('password') } onChangeText={ (text) => this.setState({ oldPassword : text }) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,{ fontSize : 20,width : '90%' } ] }></TextInput>
                                {this.__showValidationIcon(this.state.old_passwordError, this.state.updateValidated)}
                            </View>
                            
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'New Password' }</Text>
                            <View style={ [ styles.loginFormView.textField, {flexDirection : 'row'} ] }>
                                <View style={ { flex : 8, flexDirection : 'row' } }>
                                    <TextInput value={ this.state.password } returnKeyType={"next"} ref={ input => { this.inputs['password'] = input; }} onChangeText={ (text) => this.setState({ password : text }) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,{ fontSize : 20, width : '100%' } ] } ></TextInput>
                                    {this.__showValidationIcon(this.state.passwordError, this.state.updateValidated)}
                                </View>
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Confirm Password' }</Text>
                            <View style={ [ styles.loginFormView.textField, {flexDirection : 'row'} ] }>
                                <View style={ { flex : 8, flexDirection : 'row' } }>
                                    <TextInput value={ this.state.confirmPassword } onSubmitEditing={ () => this.__updatePassword() } ref={ input => { this.inputs['confirmPassword'] = input; }} onChangeText={ (text) => this.setState({ confirmPassword : text }) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,{ fontSize : 20, width : '100%' } ] }></TextInput>
                                    {this.__showValidationIcon(this.state.confirmPasswordError, this.state.updateValidated)}
                                </View>
                            </View>
                        </View>
                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__updatePassword()}>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'UPDATE'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
