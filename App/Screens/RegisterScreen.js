import React from 'react';
import {View, Alert,Text,TextInput,Picker,Image} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cuisines,RegisterHourCart} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {Validator} from '../Utils/Validator';
import {Guest} from '../Models/Guest';
import CheckBox from 'react-native-check-box';
import DatePicker from 'react-native-datepicker';
import RadioForm from 'react-native-simple-radio-button';
import {listing} from './../Models/listing';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import ReactNativeLocationServicesSettings from 'react-native-location-services-settings';

var states = require('../../state.json');


export class RegisterScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.__activeFilters = this.__activeFilters.bind(this);
        
        this.state = {
            login : this.props.navigation.state.params.screen,
            name :  '',
            email : '',
            password : '',
            confirmPassword : '',
            termsNCondition : false,
            isSubscribe : false,
            isPasswordMatch : false,
            nameError : false,
            emailError : false,
            passwordError : false,
            addressError : false,
            zip_codeError : false,
            phoneError : false,
            restaurant_phoneError : false,
            restaurant_nameError : false,
            restaurant_addressError : false,
            restaurant_descriptionError : false,
            confirmPasswordError : false,
            registerValidated : false,
            role : this.props.navigation.state.params.screen === 'restaurant-login' ? 'restaurant' : 'user',
            passwordMatchText : '',
            dob : '',
            sex : 'm',
            martial_status : 'Single',
            radio_props : [
                {label: 'Male', value: 'm' },
                {label: 'Female', value: 'f' }
            ],
            restaurant_name : null,
            restaurant_description : null,
            restaurant_address : null,
            cuisine_ids : [],
            cuisines : [],
            filterToggle : false,
            logo_path : '',
            banner_path : '',
            profile_image_path : '',
            loaded : true,
            restaurant_banner : false,
            restaurant_logo : false,
            profile_picture : false,
            phone : null,
            restaurant_phone: null,
            month : 1,
            year : 1970,
            day : 1,
            days : [],
            months : [],
            years : [],
            errors : {},
            statesList : states,
            city : null,
            state : states[0].name,
            zip_code : null,
            address : null,
            resizeImage : null,
            hours : this.__getHoursObject(),
            times : this.__getquarterTimes()
        };
        this.__setDates();
    }

    __setDates(){
        let currentYear = new Date().getFullYear();
        for (var i = 1970; i <= currentYear; i++) {
			let array = this.state.years;
			array.push(i);
			this.setState({
				array
			});
        }
        for (var i = 1; i <= 12; i++) {
            let months = this.state.months;
			months.push(i);
			this.setState({
				months
			});
        }
        for (var i = 1; i <= 31; i++) {
            let days = this.state.days;
			days.push(i);
			this.setState({
				days
			});
        }
		
	}

    componentWillMount(){
        this.__getCuisines();
        let FbData = this.props.navigation.state.params.data;
        console.log(FbData);
        this.setState({
            name : FbData != undefined ? FbData.name : '',
            email : FbData != undefined ? FbData.email != undefined ? FbData.email : '' : '',
        })
    }

    __getCuisines(){
        this.__activeLoader();
        listing.getCuisines().then( response => {
            console.log(response);
            this.__deactiveLoader();
            if(response.cuisines){
                this.setState( { cuisines : response.cuisines } )
            }
        } ).catch( error => {
            this.__deactiveLoader();
            Alert.alert('Error','Please Try Again');
        } )  
    }

    __validateRegister() {
    	let errorCount = 0;
    	let errors = {};

    	if(!Validator.isEmail(this.state.email)) {
    		errorCount++;
            errors.emailError = true;
            
    	} else {
            errors.emailError = false;
        }
        
        if(Validator.isEmpty(this.state.name)) {
    		errorCount++;
            errors.nameError = true;
            
    	} else {
            errors.nameError = false;
        }

        if(Validator.isEmpty(this.state.phone)) {
    		errorCount++;
            errors.phoneError = true;
            
    	} else {
            errors.phoneError = false;
        }

        if(Validator.isEmpty(this.state.zip_code)) {
    		errorCount++;
            errors.zip_codeError = true;
            
    	} else {
            errors.zip_codeError = false;
        }

        if(this.state.login === 'restaurant-login'){
            if(Validator.isEmpty(this.state.restaurant_name)) {
                errorCount++;
                errors.restaurant_nameError = true;
                
            } else {
                errors.restaurant_nameError = false;
            }
            if(Validator.isEmpty(this.state.restaurant_description)) {
                errorCount++;
                errors.restaurant_descriptionError = true;
                
            } else {
                errors.restaurant_descriptionError = false;
            }
            if(Validator.isEmpty(this.state.restaurant_address)) {
                errorCount++;
                errors.restaurant_addressError = true;
            } else {
                errors.restaurant_addressError = false;
            }

            if(Validator.isEmpty(this.state.restaurant_phone)) {
                errorCount++;
                errors.restaurant_phoneError = true;
                
            } else {
                errors.restaurant_phoneError = false;
            }
        }

        if(this.state.login != 'restaurant-login'){
            if(Validator.isEmpty(this.state.address)) {
                errorCount++;
                errors.addressError = true;
                
            } else {
                errors.addressError = false;
            } 
        }

    	if(Validator.isEmpty(this.state.password) || this.state.password.length < 7 ) {
    		errorCount++;
    		errors.passwordError = true;
    	} else {
    		errors.passwordError = false;
        }

        if(this.state.confirmPassword != this.state.password || Validator.isEmpty(this.state.confirmPassword)  ) {
            errorCount++;
    		errors.confirmPasswordError = true;
    	} else {
            errors.confirmPasswordError = false;
        }

    	errors.registerValidated = true;
        console.log(errors);
    	this.setState(errors);
        return errorCount == 0 ? true : false; 
        

    }

    __setParams(){
        if(this.state.login === 'restaurant-login'){
            return {
                name : this.state.name,
                email : this.state.email,
                password : this.state.password,
                phone : this.state.phone,
                password_confirmation : this.state.confirmPassword,
                role : this.state.role,
                dob : this.__date(),
                sex : this.state.sex,
                restaurant_name : this.state.restaurant_name,
                restaurant_description :  this.state.restaurant_description,
                restaurant_address :  this.state.restaurant_address,
                restaurant_phone : this.state.restaurant_phone,
                cuisine_ids : this.state.cuisine_ids,
                logo_path : this.state.logo_path,
                banner_path : this.state.banner_path,
                profile_image_path : this.state.profile_image_path,
                latitude : global.currentLat,
                longitude : global.currentLng,
                city : this.state.city,
                state : this.state.state,
                zip_code : this.state.zip_code,
                hours : this.state.hours
            };
        } else{
            return {
                name : this.state.name,
                email : this.state.email,
                phone : this.state.phone,
                password : this.state.password,
                password_confirmation : this.state.confirmPassword,
                role : this.state.role,
                dob : this.__date(),
                sex : this.state.sex,
                profile_image_path : this.state.profile_image_path,
                martial_status : this.state.martial_status,
                city : this.state.city,
                state : this.state.state,
                address :  this.state.address,
                zip_code : this.state.zip_code,
                cuisine_ids : this.state.cuisine_ids,
            };
        }
    }

    __register(){
        let that = this;
        if(this.__validateRegister()){
            this.__activeLoader();
            let params = this.__setParams();

            Guest.register(params).then(response => {
                console.log(response);
                this.__deactiveLoader();
                if(response.status){
                    Alert.alert('Success', response.message);
                    this.__goAndReset('login', {screen : this.state.login});
                } else{
                    Alert.alert('Error', response.message);
                    if(response.errors != undefined && response.errors){
                        this.setState( { errors : response.errors }, () => {
                            this.__errorHandling();
                        } );
                    }
                }
            }).catch(error => {
                this.__deactiveLoader();
                console.log(error);
                Alert.alert('Error', 'Please Try Again!');
            })
        }
    }

    __errorHandling(){
        let validation = {};
        const errorsHandle = Object.keys(this.state.errors).map(key => 
            {   
                if(key === 'password'){
                    validation['confirmPasswordError'] = true
                }
                validation[key + 'Error'] = true
            }
        )
        validation['registerValidated'] =  true;
        this.setState(validation)
    }

    __renderRadioButton(){
        return(
            <RadioForm
                formHorizontal={true}
                initial={0}
                buttonSize={10}
                labelStyle={{fontSize: 12, color: '#9b9b9b'}}
                buttonStyle={{marginLeft: 10}}
                radio_props={this.state.radio_props}
                onPress={(value) => {this.setState( { sex : value } )}}
            >
            </RadioForm>
        )
    }

    __activeFilters(id, _step, toggle){
        const __step = [...this.state.cuisine_ids];

        if(toggle){
            __step.push(id);
        } else{
            var index = __step.indexOf(id)
            __step.splice(index, 1);
        }

        this.setState( { cuisine_ids : __step } )
    }

    __openImagePicker(options,save_type) {
        ImagePicker.showImagePicker(options, (image) => {
            if (image.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (image.error) {
              console.log('ImagePicker Error: ', image.error);
            }
            else if (image.customButton) {
              console.log('User tapped custom button: ', image.customButton);
            }
            else {
                this.__activeLoader();
                let originalRotation = image.originalRotation;
                let uri = image.uri;
                let width = image.width;
                let height = image.height;
                let formatType = image.type;
                
                ImageResizer.createResizedImage( uri, width, height , 'JPEG', 80, originalRotation === 90 ? 90 : (originalRotation === 270 ? -90 : 0) ).then( uri => {
                    
                    listing.getAttachmentUrls(uri.uri, formatType, image.fileName, save_type).then(response  => {
                        this.__deactiveLoader();
                        if(response.status === 1){
                            console.log(save_type, "type");
                            if(save_type === 'RB'){
                                this.setState( { banner_path : response.image_path, restaurant_banner : response.image_url } )
                            }
                            if(save_type === 'UP'){
                                this.setState( { profile_image_path : response.image_path, profile_picture : response.image_url  } )
                            }
                            if(save_type === 'RL'){
                                this.setState( { logo_path : response.image_path, restaurant_logo : response.image_url  } )
                            }
                        
                        } else{
                            Alert.alert('Error', response.data.error ? response.data.error : 'Internal Server Error' )
                        }
                    }).catch(error =>{
                        this.__deactiveLoader();
                        Alert.alert("Error", "Please Check Your Connection!");
                    })
                } ).catch( err => {
                    this.__deactiveLoader();
                    console.log( err );
                    Alert.alert( 'Unable to resize the photo', 'Please try again!' )
                } )
            }
        });
    }
    
    __onMonthChange(value){
       this.setState( { 
           month : value,
           days : []
       }, () => this.__validateDate(value) )
    }

    __validateDate(value){
        var selectedYear = this.state.year;
        var selectedMonth = value;
        var day = 31;
        if(selectedMonth === 2){
           day = selectedYear % 4 === 0 ? 29 : 28;
        } else if(selectedMonth === 2 || selectedMonth === 4 || selectedMonth === 6 || selectedMonth === 9 || selectedMonth === 11){
            day = 30;
        };
        
        for (var i = 1; i <= day; i++) {
            let days = this.state.days;
			days.push(i);
			this.setState({
				days
			});
        }
    }

    __date(){
        return parseInt(this.state.year) + '-' + parseInt(this.state.month) + '-' + parseInt(this.state.day);
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                <View style={ styles.loginMainContent }>
                    <View style={ [ styles.left ] }>
                        <CTouchable onPress={ () => this.__back() }>
                            <Text style={ [ styles.defaultBoldText,{ color : '#4a4a4a', borderBottomColor : '#000', borderBottomWidth : 1, fontSize : 18 } ] }>{ 'Back' }</Text>
                        </CTouchable>
                    </View>
                    <View style={ [ styles.loginHeadingView, { marginBottom : 50, marginTop : 20 } ] }>
                        <Text style={ styles.loginHeadingView.text }>
                            { this.state.login === 'normal-login' ? 'Register' : 'Restaurant Register' }
                        </Text>
                    </View>
                    <View style={ styles.loginFormView }>
                        <View>
                            <Text style={ styles.loginFormView.label }>{ 'Full Name' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={this.state.name} ref={ input => { this.inputs['name'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('email') } onChangeText={ (text) => this.setState( { name : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Your full name' }></TextInput>
                                {this.__showValidationIcon(this.state.nameError, this.state.registerValidated)}
                            </View>
                            {   
                                this.state.errors.name ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.name[0] }</Text>
                                :
                                null
                            }
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Email Address' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput keyboardType={ 'email-address' } value={this.state.email} ref={ input => { this.inputs['email'] = input; }} onSubmitEditing={ () => this.focusNextField('phone') } returnKeyType={"next"} onChangeText={ (text) => this.setState( { email : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'yourname@mail.com' }></TextInput>
                                {this.__showValidationIcon(this.state.emailError, this.state.registerValidated)}
                            </View>
                            {   
                                this.state.errors.email ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.email[0] }</Text>
                                :
                                null
                            }
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Phone Number' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={this.state.phone} keyboardType={'numeric'} ref={ input => { this.inputs['phone'] = input; }} onSubmitEditing={ () => this.focusNextField('password') } returnKeyType={"next"} onChangeText={ (text) => this.setState( { phone : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'xxxxxxxxxx' }></TextInput>
                                {this.__showValidationIcon(this.state.phoneError, this.state.registerValidated)}
                            </View>
                            {   
                                this.state.errors.phone ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.phone[0] }</Text>
                                :
                                null
                            }
                        </View>

                        <View style={ {marginTop : 15} }>
                            <View style={ { flexDirection : 'row' } }>
                                {/* year */}
                                <View style={ [ styles.dobFields, { flex : 4 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Year' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.year} onValueChange = {(value) => this.setState( { year :  value } )}>
                                            {   
                                                this.state.years.map( ( item, index ) => {
                                                    return (
                                                        <Picker.Item label={item + ''} value={item} />   
                                                    )
                                                } )
                                            }
                                        </Picker>  
                                    </View>
                                </View>
                                {/* year */}
                                {/* month */}
                                <View style={ [ styles.dobFields, { flex : 3 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Month' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.month} onValueChange = { (value) => this.__onMonthChange(value) }>
                                            {
                                                this.state.months.map( ( item, index ) => {
                                                    return (
                                                        <Picker.Item label={item + ''} value={item} />   
                                                    )
                                                } )
                                            }
                                        </Picker>  
                                    </View>
                                </View>
                                {/* day */}
                                <View style={ [ styles.dobFields, { flex : 3 } ] }>
                                    <Text style={ styles.loginFormView.label }>{ 'Day' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.day} onValueChange = {(value) => this.setState( { day :  value } )}>
                                            {
                                                this.state.days.map( ( item, index ) => {
                                                    return (
                                                        <Picker.Item label={item + ''} value={item} />   
                                                    )
                                                } )
                                            } 
                                        </Picker>  
                                    </View>
                                </View>
                                {/* day */}
                               
                            </View>  
                            {   
                                this.state.errors.dob ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.dob[0] }</Text>
                                :
                                null
                            }    
                        </View>
                        
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Sex' }</Text>
                            <View style={ { marginTop : 5 } }>{ this.__renderRadioButton() }</View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Password' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={this.state.password} ref={ input => { this.inputs['password'] = input; }} onSubmitEditing={ () => this.focusNextField('confirm_password') } returnKeyType={"next"} onChangeText={ (text) => this.setState( { password : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } secureTextEntry={true} placeholder={ 'password' }></TextInput>
                                {this.__showValidationIcon(this.state.passwordError, this.state.registerValidated)}
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Confirm Password' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={this.state.confirmPassword} ref={ input => { this.inputs['confirm_password'] = input; }} onChangeText={ (text) => this.setState( { confirmPassword : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } secureTextEntry={true} placeholder={ 'confirm password' }></TextInput>
                                {this.__showValidationIcon(this.state.confirmPasswordError, this.state.registerValidated)}
                            </View>
                        </View>
                        {
                            this.state.login != 'restaurant-login' ?
                                <View style={ {marginTop : 15} }>
                                    <Text style={ styles.loginFormView.label }>{ 'Martial Status' }</Text>
                                    <View style={ [ styles.loginFormView.textField ] }>
                                        <Picker selectedValue = {this.state.martial_status} onValueChange = {(value) => this.setState( { martial_status :  value } )}>
                                            <Picker.Item label="Single"   value="Single" />
                                            <Picker.Item label="Married"  value="Married" />
                                            <Picker.Item label="Divorced" value="Divorced" />
                                            <Picker.Item label="Widowed"  value="Widowed" />
                                        </Picker>  
                                    </View>
                                </View> :
                            null
                        }

                        {
                            this.state.login != 'restaurant-login' ?
                                <View style={ {marginTop : 15} }>
                                    <Text style={ styles.loginFormView.label }>{ 'Address' }</Text>
                                    <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                        <TextInput value={this.state.address} multiline={true} numberOfLines={4} ref={ input => { this.inputs['address'] = input; }}  onChangeText={ (text) => this.setState( { address : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Address' }></TextInput>
                                        {this.__showValidationIcon(this.state.addressError, this.state.registerValidated)}
                                    </View>
                                    {   
                                        this.state.errors.address ?
                                            <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.address[0] }</Text>
                                        :
                                        null
                                    }
                                </View> :
                            null
                        }
                        
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'State' }</Text>
                            <View style={ [ styles.loginFormView.textField ] }>
                                <Picker selectedValue = {this.state.state} onValueChange = {(value) => this.setState( { state :  value } )}>
                                    {
										this.state.statesList.map((item, index) => {
											return (
												<Picker.Item label={item.name} value={item.name} />
											) 
										})
									}
                                </Picker>  
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'City' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.city } ref={ input => { this.inputs['city'] = input; }} onSubmitEditing={ () => this.focusNextField('zip_code')  } onChangeText={ (text) => this.setState( { city : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Enter City' }></TextInput>
                                {this.__showValidationIcon(this.state.cityError, this.state.formValidated)}
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Zip Code' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput keyboardType={'numeric'} maxLength={12} value={ this.state.zip_code } ref={ input => { this.inputs['zip_code'] = input; }} onSubmitEditing={ () => this.state.login === 'normal-login' ? this.__register() : this.focusNextField('restaurant_name')  } onChangeText={ (text) => this.setState( { zip_code : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Enter Zip Code' }></TextInput>
                                {this.__showValidationIcon(this.state.zip_codeError, this.state.formValidated)}
                            </View>
                            {   
                                this.state.errors.zip_code ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.zip_code[0] }</Text>
                                :
                                null
                            }
                        </View>
                        {
                            this.state.login === 'restaurant-login' ?
                                <View>
                                    <View style={ { marginTop : 15, justifyContent : 'center', alignItems : 'center' } }>
                                        <Text style={ [ styles.loginFormView.label, { fontSize : 20 } ] }>{'Restaurant Details'}</Text>
                                    </View>
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{ 'Restaurant Name' }</Text>
                                        <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                            <TextInput value={this.state.restaurant_name} ref={ input => { this.inputs['restaurant_name'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('restaurant_description') } onChangeText={ (text) => this.setState( { restaurant_name : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Full restaurant name' }></TextInput>
                                            {this.__showValidationIcon(this.state.restaurant_nameError, this.state.registerValidated)}
                                        </View>
                                        {   
                                            this.state.errors.restaurant_name ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_name[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{ 'Restaurant Description' }</Text>
                                        <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                            <TextInput value={this.state.restaurant_description} multiline = {true} numberOfLines={4} ref={ input => { this.inputs['restaurant_description'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('restaurant_address') } onChangeText={ (text) => this.setState( { restaurant_description : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Restaurant Description' }></TextInput>
                                            {this.__showValidationIcon(this.state.restaurant_descriptionError, this.state.registerValidated)}
                                        </View>
                                        {   
                                            this.state.errors.restaurant_description ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_description[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{ 'Address' }</Text>
                                        <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                            <TextInput value={this.state.restaurant_address} multiline = {true} numberOfLines={4} ref={ input => { this.inputs['restaurant_address'] = input; }} onSubmitEditing={ () =>  this.focusNextField('restaurant_phone') } onChangeText={ (text) => this.setState( { restaurant_address : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Address' }></TextInput>
                                            {this.__showValidationIcon(this.state.restaurant_addressError, this.state.registerValidated)}
                                        </View>
                                        {   
                                            this.state.errors.restaurant_address ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_address[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{ 'Restaurant Phone' }</Text>
                                        <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                            <TextInput value={this.state.restaurant_phone} keyboardType={'numeric'} ref={ input => { this.inputs['restaurant_phone'] = input; }} onSubmitEditing={ () => this.__register()  } returnKeyType={"next"} onChangeText={ (text) => this.setState( { restaurant_phone : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'xxxxxxxxxx' }></TextInput>
                                            {this.__showValidationIcon(this.state.restaurant_phoneError, this.state.registerValidated)}
                                        </View>
                                        {   
                                            this.state.errors.restaurant_phone ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.restaurant_phone[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{  'Restaurant Banner'  }</Text>
                                        <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', rotation : 0, storageOptions: { skipBackup: true,  path: 'images' } },'RB') }>
                                            <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                                        </CTouchable>
                                        {   
                                            this.state.errors.banner_path ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.banner_path[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    {
                                        this.state.restaurant_banner ?
                                            <View style={ [ styles.imageView ] }>
                                                <Image source={ { uri : this.state.restaurant_banner } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                                <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                                    <CTouchable onPress={ () => this.setState( { restaurant_banner : false, banner_path : ''  } ) } style={ { width : '5%' } }>
                                                        <Icon name={'x-circle'} size={20} />
                                                    </CTouchable>
                                                </View>
                                            </View>
                                        :
                                        null
                                    }
                                    <View style={ {marginTop : 15} }>
                                        <Text style={ styles.loginFormView.label }>{  'Restaurant Logo' }</Text>
                                        <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, 'RL') }>
                                            <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                                        </CTouchable>
                                        {   
                                            this.state.errors.logo_path ?
                                                <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.logo_path[0] }</Text>
                                            :
                                            null
                                        }
                                    </View>
                                    {
                                        this.state.restaurant_logo ?
                                            <View style={ [ styles.imageView ] }>
                                                <Image source={ { uri :  this.state.restaurant_logo } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                                <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                                    <CTouchable onPress={ () => this.setState( { restaurant_logo : false, logo_path : ''  } ) } style={ { width : '5%' } }>
                                                        <Icon name={'x-circle'} size={20} />
                                                    </CTouchable>
                                                </View>
                                            </View>
                                        :
                                        null
                                    }
                                </View> :
                            null
                        }
                        
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Profile Image' }</Text>
                            <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, 'UP') }>
                                <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                            </CTouchable>
                            {   
                                this.state.errors.profile_image_path ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.profile_image_path[0] }</Text>
                                :
                                null
                            }
                        </View>
                        {
                            this.state.profile_picture ?
                                <View style={ [ styles.imageView ] }>
                                    <Image source={ { uri :  this.state.profile_picture } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                    <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                        <CTouchable onPress={ () => this.setState( { profile_picture : false, profile_image_path : ''  } ) } style={ { width : '5%' } }>
                                            <Icon name={'x-circle'} size={20} />
                                        </CTouchable>
                                    </View>
                                </View>
                            :
                            null
                        }
                        <View style={ styles.searchFiltersView }>
                            {
                                this.state.login === 'normal-login' ?
                                    <Text style={ styles.loginFormView.label }>{ 'Interested In' }</Text>
                                : 
                                null
                            }
                            <View style={ { flexDirection : 'row', marginTop : 15, flex : 1, flexWrap : 'wrap' } }>
                                {/* cuisines filters */}
                                <Cuisines toggle={this.state.filterToggle} active={this.state.cuisine_ids} activeFilters={this.__activeFilters} cuisines={this.state.cuisines} />
                                {/* cuisines filters */}
                                {   
                                    this.state.errors.cuisine_ids ?
                                        <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.cuisine_ids[0] }</Text>
                                    :
                                    null
                                }
                            </View>
                        </View>

                        {
                            this.state.login === 'restaurant-login' ?
                                <RegisterHourCart hours={this.state.hours} times={this.state.times} isClosed={this.__thisIsClosed.bind(this)} setValue={this.__setValue.bind(this)} />
                            :
                            null
                        }

                        <View style={ {marginTop : 20} }>
                            <View style={ styles.registerCheckBoxView }>
                                <CheckBox
                                onClick={()=>{
                                    this.setState({
                                        termsNCondition:!this.state.termsNCondition
                                    })
                                }}
                                isChecked={this.state.termsNCondition}
                                style={ styles.registerCheckBoxView.CheckBox }
                                />
                                <Text style={ [ styles.defaultBoldText, { color : '#9b9b9b' } ] }>
                                    { ' I agree to Terms & Conditions' }
                                </Text>
                            </View>
                        </View>
                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__register()  }>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'REGISTER'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
