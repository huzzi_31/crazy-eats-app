import React from 'react';
import {Image, View, Text,FlatList,AsyncStorage} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {listing} from '../Models/listing';

export class RestaurantSplashScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            restaurants : []  
        }
        this.__getCurrentUserRestaurants();
        global.restaurtant.on('updateRestaurant', () => {
            this.__getCurrentUserRestaurants();
        });
    }
    
    __getCurrentUserRestaurants(){
        listing.getCurrentUserRestaurants().then( response => {
            console.log(response);
            if(response.status){
                this.setState( {
                    restaurants : response.restaurants
                } )
            }
        } ).catch( error => {
            console.log(error);
        } )
    }

    __switchRestaurant(id){
        console.log(id);
        global.userData.restaurant_id = id;
        AsyncStorage.setItem('user', JSON.stringify(global.userData));
        this.__goAndReset('MainTabs');
    }

    __renderCart(item){
        return(
            <CTouchable onPress={ () => this.__switchRestaurant(item.id) } style={ [ styles.restaurantCart ] }>
                <View>
                    <Image style={ styles.restaurantCart.image } source={ { uri : item.banner_urls[0] } } /> 
                </View>
                <View style={ styles.restaurantCart.detailBox } >
                    <View style={ styles.restaurantCart.detailBox.logoView }>
                        <Image style={ styles.restaurantCart.detailBox.logo } source={ { uri : item.logo_url  } } />
                    </View>
                    <View style={ styles.restaurantCart.detailBox.description }>
                        <View>
                            <Text style={ [ styles.defaultBoldText,{ color : '#000', fontSize : 18 } ] }>{item.name}</Text>
                        </View>
                        <View style={ [ { flexDirection : 'row' } ] }>
                            <View style={ [ styles.restaurantCart.detailBox.description.iconView ] } >
                                <MaterialIcon name="favorite" size={22} color={'#ea2e2f'} /> 
                                <Text style={ styles.restaurantCart.detailBox.description.iconView.text }>{item.no_of_favorite}</Text>
                            </View>
                            <View style={ [ styles.restaurantCart.detailBox.description.iconView, { marginLeft : 8 } ] } >
                                <Icon name="check-square" color={'#169639'} size={22} /> 
                                <Text style={ styles.restaurantCart.detailBox.description.iconView.text }>{item.checked_in_count}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </CTouchable>
        )
    }
    
    render() {
        const PageLoader = (props) => this.__loader();
        return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header style={ { backgroundColor : '#ff5500' } }>
                    <View style={ [ styles.header.left, { flex: 0.5, } ]}>
                        <Text style={ [ styles.defaultBoldText,{ color : '#fff', fontSize : 16 } ] }>{ 'Choose Restaurants' }</Text>
                    </View>
                    <View style={ [ styles.header.center, { flex : 0.1 } ] }></View>
                    <View style={ [ styles.header.right,{ flex : 0.4 } ] }>
                        <CTouchable style={ { flexDirection : 'row' } } onPress={() => this.__go('RestaurantCreate')}>
                            <Icon style={ { marginRight : 5 } } name={'plus-circle'} size={15} color={'#fff'} />
                            <Text style={ [ styles.defaultBoldText, { color : '#fff', fontSize : 12 } ] }>{ 'Add Restaurant' }</Text>
                        </CTouchable>
                    </View>
                </Header>
                {/* header */}
                <View style={ [ styles.HomeMainContent ] }>
                    <View>
                        <FlatList 
                            extraData={this.state}
                            data={this.state.restaurants}
                            renderItem={({item}) =>
                                this.__renderCart(item)
                            }   
                            keyExtractor={(item, index) => index} 
                        />
                    </View>
                </View>
            </Wrapper>
            
           
        );
    }
}
