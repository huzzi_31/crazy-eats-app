import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cart,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {listing} from '../Models/listing';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

export class WriteReviewScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.__init();
    }

    __init(){
        this.state = {
            reviewMessage : '',
            restaurantId : this.props.navigation.state.params.id,
            enviromentRating : 0,
            foodRating : 0,
            serviceRating : 0,
            varietyRating : 0,
            loaded : true,
            image_path : '',
            image_url : false,
            pickerList : [0,1,2,3],
            attacthmentUri : [],
            attacthmentPaths : []
        }
    }

    
    __review(){
        
        let params = {
            restaurant_id : this.state.restaurantId,
            review : this.state.reviewMessage,
            rating : this.__totalRatings(),
            image_path : this.__setAttachments(this.state.attacthmentPaths),
            enviroment : this.state.enviromentRating,
            food : this.state.food,
            service : this.state.serviceRating,
            variety : this.state.varietyRating,
        };

        this.__activeLoader();

        listing.addReview(params).then( response => {
            this.__deactiveLoader();
            console.log(response);
            global.restaurtant.emit('updateRestaurant');
            if(response.status){
                Alert.alert('Success', response.message);
                this.__back();
            } else{
                Alert.alert('Error', response.message);
            }
        } ).catch( error => {
            this.__deactiveLoader();
            Alert('Error','Please Try Again')
        } )
    }

    __totalRatings(){
        let ratings = parseInt(this.state.enviromentRating) + parseInt(this.state.serviceRating) + parseInt(this.state.foodRating) + parseInt(this.state.varietyRating);
        let total = ratings / 4;
        return total;
    }


    __setRatings(rating,type){
        if(type === 'enviroment'){
            this.setState( { enviromentRating : rating } )
        }
        if(type === 'food'){
            this.setState( { foodRating : rating } )
        }
        if(type === 'service'){
            this.setState( { serviceRating : rating } )
        }
        if(type === 'variety'){
            this.setState( { varietyRating : rating } )
        }
    }
    

    __activeRatings(type,rating){
        if(type === 'enviroment'){
            if(this.state.enviromentRating >= rating){
                return '#fdc10a';
            } else{
                return '#9b9b9b';
            }
        }
        if(type === 'food'){
            if(this.state.foodRating >= rating){
                return '#fdc10a';
            } else{
                return '#9b9b9b';
            }
        }
        if(type === 'service'){
            if(this.state.serviceRating >= rating){
                return '#fdc10a';
            } else{
                return '#9b9b9b';
            }
        }
        if(type === 'variety'){
            if(this.state.varietyRating >= rating){
                return '#fdc10a';
            } else{
                return '#9b9b9b';
            }
        }
    }

    __ratingsList(type){
		let array = [1,2,3,4,5];
        return(
            array.map((ratings, index) => {
				return( 
                    <CTouchable onPress={() => this.__setRatings(ratings,type)}>
                        <MaterialIcon name="star" color={ this.__activeRatings(type,ratings) } size={ 25 }  style={ styles.homeScreenRating } ></MaterialIcon>
                    </CTouchable>
                )
            })
        )
    }

    __removedImage(i){
        console.log(i);
        var array = [...this.state.attacthmentUri]; 
        array.splice(i, 1);
        this.setState( { attacthmentUri : array } );
    }

    __renderImagePicker(){
		return(
			this.state.pickerList.map((item, index) => {
				return (
                    <View style={ styles.imagePickerView.view }>
                        <CTouchable style={ styles.imagePickerView.btn } onPress={() => this.__openImagePicker({allowsEditing: true, cameraType: 'back',rotation : 0},index)}>
                            {
                               
                                this.state.attacthmentUri[index] || this.state.attacthmentUri[index] != undefined ?
                                    <Image 
                                        source={ { uri : this.state.attacthmentUri[index] } }
                                        style={ { width : 90, height : 70, alignSelf : 'center' } } 
                                    />
                                    
                                :
                                    <View style={ styles.imagePickerView.iconView }>
                                        <Icon name="plus" color="#fff" size={ 30 } />
                                    </View>
                            }
                            {
                                this.state.attacthmentUri[index] || this.state.attacthmentUri[index] != undefined ?
                                    <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                        <CTouchable onPress={ () => this.__removedImage(index) } style={ { width : '50%' } }>
                                            <Icon name={'x-circle'} size={20} />
                                        </CTouchable>
                                    </View> :
                                null
                            }
                        </CTouchable>
                    </View>
                ) 
			})
		)
	}

    __openImagePicker(options,index) {
        ImagePicker.showImagePicker(options, (image) => {
            console.log('image = ', image);
           
            if (image.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (image.error) {
              console.log('ImagePicker Error: ', image.error);
            }
            else if (image.customButton) {
              console.log('User tapped custom button: ', image.customButton);
            }
            else {
                this.__activeLoader();
                let originalRotation = image.originalRotation;
                let uri = image.uri;
                let width = image.width;
                let height = image.height;
                let formatType = image.type;
                
                ImageResizer.createResizedImage( uri, width, height , 'JPEG', 80, originalRotation === 90 ? 90 : (originalRotation === 270 ? -90 : 0) ).then( uri => {
                    
                    listing.getAttachmentUrls(uri.uri, formatType, image.fileName, false).then(response  => {
                        console.log(response);
                        this.__deactiveLoader();
                        if(response.status === 1){
                            let uri = this.state.attacthmentUri;
                            let paths = this.state.attacthmentPaths;
                            paths[index] = response.image_path;
                            uri[index] = response.image_url;
                            this.setState( { attacthmentUri : uri, attacthmentPaths : paths } );
                        } else{
                            Alert.alert('Error', response.data.error ? response.data.error : 'Internal Server Error' )
                        }
                    }).catch(error =>{
                        this.__deactiveLoader();
                        console.log(error);
                        Alert.alert("Error", "Please Check Your Connection!");
                    })
                } ).catch( err => {
                    this.__deactiveLoader();
                    console.log( err );
                    Alert.alert( 'Unable to resize the photo', 'Please try again!' )
                } )
            }
        });
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
          <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                            <Icon name={'chevron-left'} size={35} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{' Write a Review '}</Text>
                    </View>
               </Header>
                {/* header */}
                <View style={ [ styles.HomeMainContent, {  paddingVertical : 10, paddingHorizontal : 10 } ] }>
                    <View style={ styles.addReviews }>
                        <View style={ { flex : 0.6, flexDirection : 'row' } }>
                            {
                                this.__ratingsList('enviroment')
                            }
                        </View>
                        <View style={ { flex : 0.4 } }>
                            <Text style={ styles.addReviews.Label }>{'Enviroment'}</Text>
                        </View>
                    </View>
                    <View style={ styles.addReviews }>
                        <View style={ { flex : 0.6, flexDirection : 'row' } }>
                            {
                                this.__ratingsList('food')
                            }
                        </View>
                        <View style={ { flex : 0.4 } }>
                            <Text style={ styles.addReviews.Label }>{'Food'}</Text>
                        </View>
                    </View>
                    <View style={ styles.addReviews }>
                        <View style={ { flex : 0.6, flexDirection : 'row' } }>
                            {
                                this.__ratingsList('service')
                            }
                        </View>
                        <View style={ { flex : 0.4 } }>
                            <Text style={ styles.addReviews.Label }>{'Service'}</Text>
                        </View>
                    </View>
                    <View style={ styles.addReviews }>
                        <View style={ { flex : 0.6, flexDirection : 'row' } }>
                            {
                                this.__ratingsList('variety')
                            }
                        </View>
                        <View style={ { flex : 0.4 } }>
                            <Text style={ styles.addReviews.Label }>{'Variety'}</Text>
                        </View>
                    </View>
                    <View style={ { marginVertical : 15 } }>
                        <TextInput
                            underlineColorAndroid={'transparent'}
                            multiline={true}
                            numberOfLines={4}
                            style={ [ styles.textArea ] }
                            placeholder={ 'Write some thing..' }
                            onChangeText={ (text) => this.setState( { reviewMessage : text } ) }
                            value={ this.state.reviewMessage }
                        />
                    </View>
                    <View style={ { marginBottom : 10, } }>
                        <Text style={ [ styles.defaultBoldText, { fontSize : 18, color : '#000' } ] }>{'Upload Images'}</Text>
                        <Text style={ [ styles.defaultText, { fontSize : 15 } ] }>{'You Can Upload Max 4 Images'}</Text>
                    </View>
                    <View style={ { marginBottom : 15, flexDirection : 'row' } }>
                        {
                            this.__renderImagePicker()
                        }
                    </View>
                    {
                        this.state.image_url ?
                            <View style={ [ styles.imageView ] }>
                                <Image source={ { uri :  this.state.image_url } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                    <CTouchable onPress={ () => this.setState( { image_url : false } ) } style={ { width : '5%' } }>
                                        <Icon name={'x-circle'} size={20} />
                                    </CTouchable>
                                </View>
                            </View>
                        :
                        null
                    }
                    <View style={ { justifyContent : 'center', alignItems : 'center', } }>
                        <CTouchable onPress={ () => this.__review() } style={ { borderRadius : 50, paddingVertical : 10, justifyContent : 'center', alignItems : 'center', backgroundColor : '#fd3c00', width : 150  } }>
                            <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{ 'Submit' }</Text>
                        </CTouchable>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
