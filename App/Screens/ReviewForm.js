import React from 'react';
import { View,Text,TextInput,Picker,Alert} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {Validator} from '../Utils/Validator';
import {CTouchable,Wrapper,Header} from '../Components';
import {Guest} from '../Models/Guest';

export class ReviewForm extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            name : global.userData.name,
            email : global.userData.email,
            phone : '',
            message : '',
            usernameError : true,
            emailError : true,
            phoneError : true,
            messageError : true,
            formValidated : false,
            loaded : true
        }
    }

    __init(){
        this.setState( {
            name : global.userData.name,
            email : global.userData.email,
            phone : '',
            message : '',
            usernameError : true,
            emailError : true,
            phoneError : true,
            messageError : true,
            formValidated : false,
            loaded : true
        } )
    }

    __validate() {
    	
    	let errorCount = 0;
    	let errors = {};

    	if(!Validator.isEmail(this.state.email)) {
    		errorCount++;
            errors.emailError = true;
            
    	} else {
            errors.emailError = false;
            
        }
        
        if(Validator.isEmpty(this.state.name)) {
    		errorCount++;
            errors.usernameError = true;
            
    	} else {
            errors.usernameError = false;
        }

        if(Validator.isEmpty(this.state.phone)) {
    		errorCount++;
            errors.phoneError = true;
            
    	} else {
            errors.phoneError = false;
        }

        if(Validator.isEmpty(this.state.message)) {
    		errorCount++;
            errors.messageError = true;
            
    	} else {
            errors.messageError = false;
        }
    	
    	errors.formValidated = true;

    	this.setState(errors);

    	return errorCount == 0 ? true : false; 

    }

    __contactForm(){
        if(this.__validate()){
            this.__activeLoader();
            let params = {
                name : this.state.name,
                email : this.state.email,
                phone : this.state.phone,
                message : this.state.message,
            }

            Guest.contactForm(params).then( response => {
                this.__deactiveLoader();
                console.log(response);
                if(response.status){
                    Alert.alert('Success', response.message)
                    this.__init();
                } else{
                    Alert.alert('Failed', response.message); 
                }
            } ).catch( error => {
                this.__deactiveLoader();
                console.log(error);
            } ) 
        }
    }

    render() {
        const PageLoader = (props) => this.__loader();
        return (
          <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{'Contact CrayZEats'}</Text>
                    </View>
                </Header>
                {/* header */}
                <View style={ styles.loginMainContent }>
                    <View style={ styles.loginFormView }>
                        <View>
                            <Text style={ styles.loginFormView.label }>{'Full Name'}</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.name } ref={ input => { this.inputs['name'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('email') } onChangeText={ (text) => this.setState( { name : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'Your full name' }></TextInput>
                                {this.__showValidationIcon(this.state.usernameError, this.state.formValidated)}
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Email Address' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.email } ref={ input => { this.inputs['email'] = input; }} onSubmitEditing={ () => this.focusNextField('phone') } returnKeyType={"next"} onChangeText={ (text) => this.setState( { email : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'yourname@mail.com' }></TextInput>
                                {this.__showValidationIcon(this.state.emailError, this.state.formValidated)}
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Phone Number' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.phone } keyboardType={'numeric'} ref={ input => { this.inputs['phone'] = input; }} onSubmitEditing={ () => this.focusNextField('message') } returnKeyType={"next"} onChangeText={ (text) => this.setState( { phone : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 20 } ] } placeholder={ 'xxxxxxxxxx' }></TextInput>
                                {this.__showValidationIcon(this.state.phoneError, this.state.formValidated)}
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Message' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.message } numberOfLines={4} multiline={true} ref={ input => { this.inputs['message'] = input; }} onSubmitEditing={ () => this.__contactForm() }  onChangeText={ (text) => this.setState( { message : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,styles.textAreaSetting, { fontSize : 20 } ] } placeholder={ 'Enter Message' }></TextInput>
                                {this.__showValidationIcon(this.state.messageError, this.state.formValidated)}
                            </View>
                        </View>

                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__contactForm()}>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'SUBMIT'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
