import React from 'react';
import {Image, View, Alert, AsyncStorage, Animated,Text,TextInput,ImageBackground,Dimensions} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,GuestWrapper,Wrapper,Cart,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export class ByPlacesList extends BaseScreen {

    constructor(props) {
        super(props);
    }

    render() {
       return (
            <Wrapper >
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                            <Icon name={'chevron-left'} size={35} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ styles.header.center }>
                        <Text style={ styles.headerText }>{' Places Nearby '}</Text>
                    </View>
               </Header>
                {/* header */}
                <View style={ [ styles.HomeMainContent, {  paddingVertical : 10, paddingHorizontal : 10 } ] }>
                    <View style={ [ styles.cartView, { marginTop : 0 } ] }>
                        <Cart />
                        <Cart />
                        <Cart />
                        <Cart />
                    </View>
                </View>
            </Wrapper>
        );
    }
}
