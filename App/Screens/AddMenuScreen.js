import React from 'react';
import {Image, View, Alert, Text, TextInput} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {listing} from './../Models/listing';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

export class AddMenuScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.__activeFilters = this.__activeFilters.bind(this);
        this.state = {
            reviewMessage : '',
            isEdit : this.props.navigation.state.params.data ? this.props.navigation.state.params.data : false,
            activeMenu : this.props.navigation.state.params.data ? parseInt(this.props.navigation.state.params.data.restaurant_category_id) : false ,
            name : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.name : '' ,
            price : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.price :'',
            description : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.description :'',
            image_path : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.image_path != "" ? this.props.navigation.state.params.data.image_path : '' : '',
            image_url : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.image_path != "" ? this.props.navigation.state.params.data.image_url : false : false,
            categories : [
                {
                    id : 1,
                    name : 'Starters'
                },
                {
                    id : 2,
                    name : 'Entrees'
                },
                {
                    id : 3,
                    name : 'Desserts'
                },
            ],
            priceError : false,
            nameError : false,
            fieldValidated : false,
            errors : {},
        }  
    }


    __init(){
        this.setState( {
            reviewMessage : '',
            isEdit : this.props.navigation.state.params.data ? this.props.navigation.state.params.data : false,
            activeMenu : this.props.navigation.state.params.data ? parseInt(this.props.navigation.state.params.data.restaurant_category_id) : false ,
            name : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.name : '' ,
            price : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.price :'',
            description : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.description :'',
            image_path : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.image_path != "" ? this.props.navigation.state.params.data.image_path : '' : '',
            image_url : this.props.navigation.state.params.data ? this.props.navigation.state.params.data.image_path != "" ? this.props.navigation.state.params.data.image_url : false : false,
            categories : [
                {
                    id : 1,
                    name : 'Starters'
                },
                {
                    id : 2,
                    name : 'Entrees'
                },
                {
                    id : 3,
                    name : 'Desserts'
                },
            ],
            priceError : false,
            nameError : false,
            fieldValidated : false,
            errors : {},
        })
    }

    __activeFilters(id){
        this.setState({ activeMenu : id })
    }

    __addMenu(){

        let params = {
            name :  this.state.name,
            description : this.state.description,
            price : this.state.price,
            image_path : this.state.image_path,
            restaurant_category_id : this.state.activeMenu,
            restaurant_id :  global.userData.restaurant_id,
        }

        listing.addMenu(params,this.state.isEdit ? this.state.isEdit.id : false ).then( response => {
            console.log(response);
            if(response.status){
                global.menus.emit('getMenus');
                Alert.alert('Success', response.message);
                if(!this.state.isEdit){
                    this.__init();
                }
            }
            else{
                Alert.alert('Error', response.message);
                if(response.errors != undefined && response.errors){
                    this.setState( { errors : response.errors }, () => {
                        this.__errorHandling();
                    } );
                }
            }
        } ).catch( error => {
            console.log(error);
            Alert.alert('Error', 'Please Check Your Connection')
        } )
    }
    
    __errorHandling(){
        let validation = {};
        const errorsHandle = Object.keys(this.state.errors).map(key => 
            {   
                validation[key + 'Error'] = true
            }
        )
        validation['fieldValidated'] =  true;
        this.setState(validation);
    }

    __openImagePicker(options) {
        ImagePicker.showImagePicker(options, (image) => {
            console.log('image = ', image);
           
            if (image.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (image.error) {
              console.log('ImagePicker Error: ', image.error);
            }
            else if (image.customButton) {
              console.log('User tapped custom button: ', image.customButton);
            }
            else {
                this.__activeLoader();
                let originalRotation = image.originalRotation;
                let uri = image.uri;
                let width = image.width;
                let height = image.height;
                let formatType = image.type;
                
                
                ImageResizer.createResizedImage( uri, width, height , 'JPEG', 80, originalRotation === 90 ? 90 : (originalRotation === 270 ? -90 : 0) ).then( uri => {
                    
                    listing.getAttachmentUrls(uri.uri, formatType, image.fileName, 'RM').then(response  => {
                        console.log(response);
                        this.__deactiveLoader();
                        if(response.status === 1){
                            this.setState( { image_path : response.image_path, image_url : response.image_url } );
                        } else{
                            Alert.alert('Error', response.data.error ? response.data.error : 'Internal Server Error' )
                        }
                    }).catch(error =>{
                        this.__deactiveLoader();
                        Alert.alert("Error", "Please Check Your Connection!");
                    })
                } ).catch( err => {
                    this.__deactiveLoader();
                    console.log( err );
                    Alert.alert( 'Unable to resize the photo', 'Please try again!' )
                } )
               
            }
        });
    }

    render() {
       return (
            <Wrapper >
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{'Add Menu Items'}</Text>
                    </View>
                </Header>
                {/* header */}
                <View style={ styles.loginMainContent }>
                    <View style={ styles.loginFormView }>
                        <View>
                            <Text style={ styles.loginFormView.label }>{'Menu Item'}</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.name } ref={ input => { this.inputs['name'] = input; }} returnKeyType={"next"} onSubmitEditing={ () => this.focusNextField('price') } onChangeText={ (text) => this.setState( { name : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,{ fontSize : 20 } ] } placeholder={ 'Enter Menu Item Name Here' }></TextInput>
                                {this.__showValidationIcon(this.state.nameError, this.state.fieldValidated)}
                            </View>
                            {   
                                this.state.errors.name ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.name[0] }</Text>
                                :
                                null
                            }
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Price' }</Text>
                            <View style={ [ styles.fieldParentView,styles.loginFormView.textField ] }>
                                <TextInput value={ this.state.price } ref={ input => { this.inputs['price'] = input; }} onSubmitEditing={ () => this.focusNextField('description') } keyboardType={'numeric'} returnKeyType={"next"}  onChangeText={ (text) => this.setState( { price : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.defaultBoldText,styles.fieldParentView.field,{ fontSize : 20 } ] } placeholder={ 'e.g 15' }></TextInput>
                                {this.__showValidationIcon(this.state.priceError, this.state.fieldValidated)}
                            </View>
                            {   
                                this.state.errors.price ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.price[0] }</Text>
                                :
                                null
                            }
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Image' }</Text>
                            <CTouchable style={ [ styles.searchFiltersView.filters, ] } onPress={ () => this.__openImagePicker({ title: 'Select Image', storageOptions: { skipBackup: true,  path: 'images' },rotation : 0 }, this.state.login === 'restaurant-login' ? 'RL' : 'UP') }>
                                <Text style={ [ styles.defaultBoldText,styles.fieldParentView.field, { fontSize : 15,textAlign : 'center' } ] }>{ 'Upload Image' }</Text>
                            </CTouchable>
                            {   
                                this.state.errors.image_path ?
                                    <Text style={ styles.registerValidateServerErrors }>{ this.state.errors.image_path[0] }</Text>
                                :
                                null
                            }
                        </View>
                        {
                            this.state.image_url ?
                                <View style={ [ styles.imageView ] }>
                                    <Image source={ { uri :  this.state.image_url } } style={ [ { resizeMode : 'cover', height : 90, width : '50%' } ]} />
                                    <View style={ { position : 'absolute', top : 0, left : 0, right : 0, bottom : 0, } }>
                                        <CTouchable onPress={ () => this.setState( { image_url : false } ) } style={ { width : '5%' } }>
                                            <Icon name={'x-circle'} size={20} />
                                        </CTouchable>
                                    </View>
                                </View>
                            :
                            null
                        }
                        <View style={ styles.searchFiltersView }>
                            <Text style={ styles.loginFormView.label }>{ 'Pick Category' }</Text>
                            <View style={ { flexDirection : 'row', flex : 1, flexWrap : 'wrap' } }>
                                {
                                    this.state.categories.map((category, index) => {
                                        return(
                                            <View style={ [ styles.searchFiltersView.filters, this.state.activeMenu === category.id ? styles.homeScreenFilters : '' ] }>
                                                <CTouchable onPress={ () => this.__activeFilters(category.id)  }>
                                                    <Text style={ [ styles.defaultBoldText, styles.searchFiltersView.box, this.state.activeMenu === category.id ? styles.homeScreenFilters.text : '' ] }>{category.name}</Text>
                                                </CTouchable>
                                            </View>
                                        )
                                    })
                                }
                            </View>
                        </View>
                        <View style={ {marginTop : 15} }>
                            <Text style={ styles.loginFormView.label }>{ 'Description' }</Text>
                            <TextInput value={ this.state.description } multiline = {true} ref={ input => { this.inputs['description'] = input; }} onSubmitEditing={ () => this.__addMenu() } numberOfLines={4}  onChangeText={ (text) => this.setState( { description : text } ) } underlineColorAndroid={ 'transparent' } style={ [ styles.loginFormView.textField,styles.defaultBoldText,{ fontSize : 20, height : 'auto', textAlignVertical : 'top', } ] } placeholder={ 'Description' }></TextInput>
                        </View>

                        <View style={ styles.loginBtns }>
                            <View>
                                <CTouchable style={ [ styles.button,styles.loginBtns.btn, {borderColor : '#fd3c00',} ] } onPress={ () => this.__addMenu()}>
                                    <Text style={ [ styles.defaultBoldText,styles.defaultTextColor, { fontSize : 18 } ] }> {'SUBMIT'} </Text>
                                </CTouchable>
                            </View>
                        </View>
                    </View>
                </View>
            </Wrapper>
        );
    }
}
