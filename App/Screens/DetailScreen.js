import React from 'react';
import {Image, View, Alert,Text,Dimensions,Linking,Share,Modal} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Header,HourChart} from '../Components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/Feather';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Communications from 'react-native-communications';
import ReactNativeLocationServicesSettings from 'react-native-location-services-settings';
import {listing} from '../Models/listing';
import {Guest} from '../Models/Guest';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export class DetailScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {  
            item : this.props.navigation.state.params.data,
            isEdit : this.props.navigation.state.params.isEdit ? true : false,
            images :[],
            activeSlide: 0,
            loaded : true,
            isCheckedIn : false ,
            user : [],
            isModalVisible : false,
            sharedUser : [],
            params : {
                cuisines : [],
            },
            uri : "https://maps.googleapis.com/maps/api/staticmap?center=Al Bada'a, Dubai&zoom=13&size=350x200&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyBcP29rWZ2RbDnsDHCh7eTSCwrwJPNIS4o"
        }
        this.__getRestaurant();
        this.__setDetails();
        this.__getViews();
        this.__getCurrentUser();
        global.restaurtant.on('updateRestaurant', () => {
            this.__getRestaurant();
            this.__getCurrentUser();
        }); 
    }

    __setDetails(){
        let item = this.state.item;
        this.setState( { images : item.banner_urls } )
    }
    
    
    __getRestaurant(){
       listing.singleRestaurant(this.state.item.id).then( response => {
           console.log('server' , response);
            this.setState( {
                item : response
            } )
       } ).catch( error => {
           console.log(error)
       } )
    }

    __getViews(){
        listing.getRestaurantsViews(this.state.item.id).then( response => {
        } ).catch( error => {
            console.log(error);
        } )
    }
    
    __imageCarousel ({item, index}) {
        return (
            <View key={index}>
                <Image 
                source={ { uri : item } }
                style={ { width : '100%', height : viewportHeight / 4 }}
                />
            </View>
        );
    }

    __appShare(){
        Share.share({
            message: this.state.item.description,
            url: 'http://crazyeats.intelvue.com',
            title: this.state.item.name
        }, {
            // Android only:
            dialogTitle: 'Share This With Your Friends',
            // iOS only:
            excludedActivityTypes: [
                'com.apple.UIKit.activity.PostToTwitter'
            ]
        })
    }

    get pagination () {
        const { activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={this.state.item.banner_urls}
              activeDotIndex={activeSlide}
              dotStyle={{
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginHorizontal: 2,
                  backgroundColor : '#e05355',
              }}
              inactiveDotStyle={{
                width: 10,
                height: 10,
                borderRadius: 5,
                marginHorizontal: 2,
                backgroundColor : '#fff',
              }}
            //   inactiveDotOpacity={0.4}
            //   inactiveDotScale={0.6}
            />
        );
    }

    __setFollowers(id, _step, toggle){
        const __users = [...this.state.sharedUser];
    
        if(toggle){
            __users.push(id);
        } else{
            var index = __users.indexOf(id)
            __users.splice(index, 1);
        }

        this.setState( { sharedUser : __users } )
    }

    __addFavourite(){
        let params = {
            restaurant_id : this.state.item.id,
            type : this.state.item.favorite ? 'remove' : 'add'
        };
        this.__activeLoader();
        
        listing.addFavourite(params).then( response => {
            this.__deactiveLoader();
            global.initHistory.emit('getHistory');
            if(response.added){
                let item = this.state.item;
                item.favorite = true;
                this.setState( { item  : item } )
            } else{
                let item = this.state.item;
                item.favorite = false;
                this.setState( { item  : item } ) 
            }
        } ).catch( error => {
            Alert.alert('Error','Please Try Again')
            this.__deactiveLoader();
            console.log(error);
        } )
    }

    __checkIn(){
        this.__activeLoader();
        let params ={
            latitude: global.currentLat,
            longitude: global.currentLng,
        }
        listing.checkIn(params,this.state.item.id).then( response => {
            global.initHistory.emit('getHistory');
            this.__deactiveLoader();
            if(response.status){
                let item = this.state.item;
                item.checked_in_count = response.checked_in_count;
                this.setState( { item : item } )
                Alert.alert('Success', response.message);
            } else{
                Alert.alert('Alert!', response.message);
            }
        } ).catch( error => {
            this.__deactiveLoader();
            Alert(error);
            console.log(error);
        } ) 
    }

    __getCurrentLocation(){
        global.currentLat = '';
        global.currentLng = '';
        this.__activeLoader();
        navigator.geolocation.watchPosition( 
			(position) => {
				var lat = parseFloat(position.coords.latitude);
				var long = parseFloat(position.coords.longitude);
				global.currentLat = lat;
				global.currentLng = long;
                global.isLocationEnable = true;
                this.__deactiveLoader();
                this.__checkIn();
			},
			(error) => {
                console.log(error.message);
                Alert.alert('Error',error.message)
                this.__deactiveLoader();

			},
		)
    }

    __enableLocation(){
        ReactNativeLocationServicesSettings.checkStatus('high_accuracy').then(res => {
            if (!res.enabled) {
            	ReactNativeLocationServicesSettings.askForEnabling(res => {
                	if (res) {
						this.__getCurrentLocation();
                	} else {
                  		alert('location services were denied by the user')
                	}
              	})
            } else{
				this.__getCurrentLocation();
			}
        })
    }

    __getCurrentUser(){
        this.__activeLoader();
        Guest.me().then( response => {
            this.__deactiveLoader();
            if(response.me){
                this.setState( { user : response.me.followers },() => console.log(this.state.user) )
            } else{
                Alert.alert('Error', response.message);
            }
        } ).catch( error => {
                this.__deactiveLoader();
        } )
         
    }

    __shareWithFollowers(){
        this.setState( { isModalVisible : false } )
        this.__activeLoader();
        listing.share(this.state.sharedUser,this.state.item.id).then( response => {
            console.log(response);
            this.__deactiveLoader();
            if(response.status){
                Alert.alert('Success', 'This Restaurant Has Been Shared With Your Followers' );
            } else{
                Alert.alert('Failed', 'Cannot Shared Restaurant!' )
            }
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            Alert.alert('Error' , 'Please Try Again!')
        } )
    }

    __activeFilters(id){
        let __step = this.state.params;
        __step.cuisines = [];
        __step.cuisines.push(id);
        this.setState( { __step },() => {
            if(this.state.params.cuisines.length > 0){
                this.__go('SearchResults',{ params : this.state.params })
            }
        } )
    }

    render() {
        const PageFooter = (props) => 
                <View style={ { flexDirection : 'row' } }>
                    <View style={ [ styles.detailFooterbtns, { backgroundColor : '#2dcdcd' } ] } >
                        <CTouchable onPress={ () => this.__go('MenuScreen',{ restaurant_id : this.state.item.id }) }>
                            <Text style={ styles.detailFooterbtns.text }>{'Menu'}</Text>
                        </CTouchable>
                    </View>
                    { global.userData.role != 'restaurant' ?
                        <View style={ [ styles.detailFooterbtns, { backgroundColor : '#6ec148' } ] } >
                            <CTouchable onPress={ () => this.__enableLocation() }>
                                <Text style={ styles.detailFooterbtns.text }>{'Check In'}</Text>
                            </CTouchable>
                        </View>
                        : null
                    }
                    { global.userData.role != 'restaurant' ?
                        <View style={ [ styles.detailFooterbtns, { backgroundColor : '#fd3c00' } ] } >
                            <CTouchable onPress={ () => this.__go('WriteReview', { id : this.state.item.id, }) }>
                                <Text style={ styles.detailFooterbtns.text }>{'Write Review'}</Text>
                            </CTouchable>
                        </View>
                        : null
                    }
                    
                </View> 
        const PageLoader = (props) => this.__loader();
       return (
            <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : <PageFooter />}>
                {/* header */}
                <Header>
                    <View style={ styles.header.left }>
                        <CTouchable onPress={() => this.__back()} style={ { padding: 5 } }>
                            <Icon name={'chevron-left'} size={25} color={ '#000' } />
                        </CTouchable>
                    </View>
                    <View style={ [ styles.header.center, { flexDirection : 'row' } ] }>
                        <Text style={ [ styles.headerText, { color : '#000' } ] }>{ this.state.item.name }</Text>
                    </View>
                    <View style={ styles.header.right }>
                        <CTouchable onPress={ () => this.__go('MenuScreen',{ restaurant_id : this.state.item.id }) } style={ { padding: 5 } }>
                            <Icon name={'info'} size={25} color={ '#000' } />
                        </CTouchable>
                    </View>
               </Header>
                {/* header */}
                <View style={ [ styles.DetailScreenSlider, ] }>
                   
                    <Carousel
                        data={this.state.item.banner_urls}
                        autoplay={true}
                        autoplayInterval={2000}
                        renderItem={this.__imageCarousel}
                        sliderWidth={viewportWidth} 
                        itemWidth={viewportWidth}
                        onSnapToItem={(index) => this.setState({ activeSlide: index }) }
                        inactiveSlideScale={1}
                    />
                    <View style={ { position : 'absolute', left : 0, right : 0, bottom : 0, } }>
                        { this.pagination }
				    </View>
                </View>
                <View style={ { flexDirection : 'row', borderBottomWidth : 1, borderBottomColor : '#d9d9d9', paddingTop : 20, paddingBottom : 10, paddingHorizontal : 10  } }>
                    <View style={ {  flexDirection : 'row', flex : 1 } }>
                        <View>
                            <Image style={ [ styles.profileImage ] } source={ { uri :  this.state.item.logo_url  } } />
                        </View>
                        <View style={ { paddingLeft : 5, flex : 1 } }>
                            <View>
                                <Text style={ [ styles.defaultBoldText, { color : '#000' } ] }>{ this.state.item.name }</Text>
                            </View>
                            <View style={ { flexDirection : 'row', marginTop : 2, flex : 1 } }>
                                <View style={ [ { flexDirection : 'row', flex : 0.5 }] } >
                                    {
                                        this.__ratings(this.state.item.rating) 
                                    }
                                    <CTouchable onPress={ () => this.__go('Reviews', { id : this.state.item.id, name : this.state.item.name, restaurant : this.state.item,  }) }>
                                        <Text style={ { color : '#5e5e5e', fontSize : 12 } }>{ this.state.item.review_count + ' Reviews' }</Text>
                                    </CTouchable>
                                </View>
                                <View style={ [ { flexDirection : 'row',  flex : 0.5, justifyContent : 'flex-end' }] } >
                                    <View style={ { paddingHorizontal : 3 } }>
                                        <Image source={ require ('../../assets/img/location_icon.png') } style={ { width : 15, height : 15 } } />
                                    </View>
                                    <CTouchable onPress={ () => this.__go('checkIn', { id : this.state.item.id }) }>
                                        <Text style={ { color : '#5e5e5e', fontSize : 12 } }>{ this.state.item.checked_in_count + ' Checked in here' }</Text>
                                    </CTouchable>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={ { borderBottomWidth : 1, borderBottomColor : '#d9d9d9', paddingTop : 20, paddingBottom : 10, paddingHorizontal : 10  } }>
                    <View>
                        <Text style={ [ styles.defaultBoldText,  { color : '#4a4a4a', fontSize : 18 } ] }>{ 'Famous For' }</Text> 
                    </View>
                    <View style={ { flexDirection : 'row', marginTop : 5, flex : 1, flexWrap : 'wrap' } }>
                        {
                            this.state.item.cuisines != undefined && this.state.item.cuisines.length > 0 ?
                                this.state.item.cuisines.map((cuisines, index) => {
                                    return(
                                        <CTouchable onPress={ () => this.__activeFilters(cuisines.id) } style={ [ styles.searchFiltersView.detailScreenFilter] }>
                                            <Text style={ [ styles.searchFiltersView.box ] }>{cuisines.name}</Text>
                                        </CTouchable>
                                    )
                                })
                            :
                            null
                        }
                    </View>
                    
                </View>
                <View style={ [ styles.HomeMainContent, { paddingVertical : 10 } ] }>
                    <View style={ [ styles.foodDetailDescription, { flexDirection : 'row' } ] }>
                        <View style={ [ { flex : 1 } ] }>
                            <Text style={ [ styles.defaultBoldText, { color : '#4a4a4a', fontSize : 13 } ] }>{ this.state.item.description }</Text>
                        </View>
                    </View>
                    {
                        global.userData.role != 'restaurant' ?
                            <View style={ [ styles.otherInfo ] }>
                                <View style={ { flexDirection : 'row' } }>
                                    <View style={ { flex : 0.5, } }>
                                        <CTouchable onPress={ () => this.setState( { isModalVisible : true } ) } style={ { flexDirection : 'row', } }>
                                            <Icon name="share-2" size={20} style={ [ styles.otherInfo.icon ] } />
                                            <Text style={ [ styles.defaultBoldText, styles.otherInfo.text ] }>{'Invite Friends'}</Text>
                                        </CTouchable>
                                    </View>
                                    <View style={ { flex : 0.5, justifyContent : 'flex-end'  } }>
                                        <CTouchable onPress={ () => this.__addFavourite() } style={ { flexDirection : 'row'} }>
                                            {
                                                this.state.item.favorite ?
                                                    <MaterialIcon name="favorite" size={20} style={ [ styles.otherInfo.icon ] } /> 
                                                :
                                                    <Icon name="heart" size={20} style={ [ styles.otherInfo.icon ] }  />
                                            }
                                            <Text style={ [ styles.defaultBoldText, styles.otherInfo.text ] }>{'Add to Favorites'}</Text>
                                        </CTouchable>
                                    </View>
                                </View>
                                <View style={ { flexDirection : 'row', paddingTop : 20 } }>
                                    <View style={ { flex : 0.5, } }>
                                        <CTouchable onPress={ () => this.__go('Reviews', { id : this.state.item.id, name : this.state.item.name, restaurant : this.state.item,  }) } style={ { flexDirection : 'row', } }>
                                            <Icon name="message-square" size={20} style={ [ styles.otherInfo.icon ] } />
                                            <Text style={ [ styles.defaultBoldText, styles.otherInfo.text ] }>{'Reviews'}</Text>
                                        </CTouchable>
                                    </View>
                                    {
                                        this.state.item.phone ?
                                            <View style={ { flex : 0.5, justifyContent : 'center'  } }>
                                                <CTouchable onPress={ () => Communications.phonecall(this.state.item.phone, true) } style={ { flexDirection : 'row'} }>
                                                    <Icon name="phone" size={20} style={ [ styles.otherInfo.icon ] }  />
                                                    <Text style={ [ styles.defaultBoldText, styles.otherInfo.text ] }>{'Call Restaurant'}</Text>
                                                </CTouchable>
                                            </View> :
                                        null
                                    }
                                </View>
                                {
                                    this.state.item.restaurant_hours ?
                                        <View style={ {  justifyContent : 'center',paddingTop : 10, flex : 1 , alignItems : 'center'  } }>
                                            <CTouchable style={ { flexDirection : 'row'} }>
                                                <Icon name="clock" size={20} style={ [ styles.otherInfo.icon ] }  />
                                                <Text style={ [ styles.defaultBoldText, styles.otherInfo.text ] }>{this.state.item.restaurant_hours}</Text>
                                            </CTouchable>
                                        </View> :
                                    null
                                }
                            </View>
                        :
                            null
                    }
                    {
                        this.state.item.hours!= undefined ?
                        <View>
                            <View style={ { justifyContent : 'center', alignItems : 'center' } }>
                                <Text style={ [ styles.defaultBoldText, { fontSize : 18, color : '#000' } ] }>{ 'Hours' }</Text>
                            </View>
                            <HourChart hours={this.state.item.hours} />
                        </View>
                        : null
                    }
                    <View style={ [ styles.mapView ] }>
                        {
                            this.state.item.map_image_url != '' ?
                                <CTouchable onPress={ this.state.item.map != '' ? () => Linking.openURL(this.state.item.map) : '' }>
                                    <Image 
                                        source={ { uri : this.state.item.map_image_url } }
                                        style={ { width : '100%', height : 150, resizeMode : 'cover' }}
                                    />
                                </CTouchable>
                            :
                            null
                        }
                        
                    </View>
                </View>
                <Modal transparent={true} visible={this.state.isModalVisible} animationType={'slide'} onRequestClose={ () => this.setState( { isModalVisible : false } ) }>
                    <View style={styles.modalInner}>
                        <View style={ { width: 400, padding: 20, backgroundColor: '#ffffff', borderRadius: 15 } }>
                            <View style={ styles.validationIcon } >
                                <CTouchable onPress={() =>  this.setState( { isModalVisible : false } )}>
                                    <Icon name="x" color="#cccccc" size={22} />
                                </CTouchable>
                            </View>
                            <View style={ { marginVertical : 8 } }>
                                <Text style={ { color : '#000', fontSize : 15, fontFamily : 'OpenSans-Bold' } }>{'Share This Restaurant With Your Friends Using:'}</Text>
                            </View>
                            <View style={ { flexDirection : 'row', marginTop : 15, flexWrap : 'wrap' } }>
                                {
                                    this.state.user != undefined && this.state.user.length > 0 ?
                                        this.state.user.map((user, index) => {
                                            return(
                                                <CTouchable onPress={ () => this.__setFollowers(user.id , index, this.state.sharedUser.indexOf(user.id) !== -1 ? false : true )  }>
                                                    <Image 
                                                        source={ { uri : user.profile_image_url } } 
                                                        style={ [ { borderRadius : 50, width : 80, height : 80, }, this.state.sharedUser.indexOf(user.id) !== -1 ? { opacity : 0.3 } : '' ] }
                                                    />
                                                    { 
                                                        this.state.sharedUser.indexOf(user.id) !== -1 ?
                                                            <View style={ { position : 'absolute', left : 20,  top : 20, } }>
                                                                <Icon name={'check'} size={40} color={'#000'} />
                                                            </View>
                                                        :
                                                        null
                                                    }
                                                </CTouchable>
                                            )
                                        })
                                    :
                                    null
                                }
                            </View>
                            <View style={ { flexDirection : 'row', justifyContent : 'center', alignItems : 'center', marginTop : 15 } }>
                                <CTouchable style={ [ styles.shareBtn ] } onPress={ () => this.__appShare() }>
                                    <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{'Other Apps'}</Text>
                                </CTouchable>
                                <CTouchable  style={ [ styles.shareBtn,{ backgroundColor : '#f26122', borderColor : '#f26122'  } ] } onPress={ () => this.__shareWithFollowers() } >
                                    <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{'With Followers'}</Text>
                                </CTouchable>
                            </View>
                            
                        </View>
                    </View>
                </Modal>
            </Wrapper>
        );
    }
}
