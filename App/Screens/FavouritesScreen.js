import React from 'react';
import { View,Text,FlatList,ActivityIndicator,Image} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper,Cart,Header} from '../Components';
import {listing} from '../Models/listing';


export class FavouritesScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            activeMenu : 1,
            favourites : [],
            history : [],
            loaded : true,
            restaurantCheckIns : [],
            isUserProfile : global.userData.role === 'restaurant' ? false : true,
        }
        this.ratings = this.__ratings.bind(this);
        this.go = this.__go.bind(this);
        this.__getFavNHistory();
        global.initHistory.on('getHistory', () => {
            this.__getFavNHistory();
        }); 
    }

    __activeMenu(_step){
        this.setState( { activeMenu : _step } )
    }

    __getFavNHistory(){
        this.__activeLoader();
        listing.getFavNHistory().then( response => {
            this.__deactiveLoader();
            if(this.state.isUserProfile){
                this.setState( {
                    favourites : response.favorites,
                    history : response.check_ins,
                } )
            } else{
                this.setState( {
                    restaurantCheckIns : response.check_ins,
                } )
            }
            
        } ).catch( error => {   
            this.__deactiveLoader();
        } )
    }
    
    __getCheckedInHistory(){
        this.__activeLoader();
        listing.getCheckedInHistory().then( response => {
            this.__deactiveLoader();
        } ).catch( error => {   
            this.__deactiveLoader();
        } ) 
    }

    __renderCart(){
        if(this.state.isUserProfile){
            return(
                <View style={ [ styles.mainContentMenu ] }> 
                    <View style={ [ styles.menuTitleBar ] }>
                        <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 1 ? styles.activeMenu : '', { flex : 5 } ] }>
                            <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(1) }>
                                <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 1 ? styles.activeMenu.text : '' ] }>{ 'Favorites' }</Text>
                            </CTouchable>
                        </View>
                        <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 2 ? styles.activeMenu : '' , { flex : 5 } ] }>
                            <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(2) }>
                                <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 2 ? styles.activeMenu.text : '' ] }>{ 'History' }</Text>
                            </CTouchable>
                        </View>
                    </View>
                    <View style={ [ styles.cartListView,{ marginTop : 10, paddingHorizontal : 15 } ] }>
                       {
                            !this.state.loader ?
                                <View>
                                    <FlatList 
                                        extraData={this.state}
                                        data={this.state.activeMenu === 1 ? this.state.favourites : this.state.history}
                                        renderItem={({item}) =>
                                            <Cart ratings={this.ratings} item={item} navigator={this.go} />
                                        }   
                                        keyExtractor={(item, index) => index} 
                                    />
                                </View> :
                            <View>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>

                        }
                    </View>
                </View>
            )
        } else{
            return(
                <View style={ [ styles.mainContentMenu ] }> 
                    <View style={ [ styles.cartListView,{ marginTop : 10, } ] }>
                       {
                            !this.state.loader ?
                                <View>
                                    <FlatList 
                                        extraData={this.state}
                                        data={this.state.restaurantCheckIns}
                                        renderItem={({item}) =>
                                            <View style={ [ styles.reviewCart ] }>
                                                <View style={ [ styles.reviewCart.cart ] }>
                                                    <View style={ { flex : 0.1 } }>
                                                        <Image style={ [ styles.profileImage ] } source={ { uri :  item.profile_image_url  } } /> 
                                                    </View>
                                                    <View style={ [ styles.reviewCart.info, { flex : 0.9 }] }>
                                                        <View style={ { flexDirection : 'row' } }>
                                                            <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ item.name }</Text>
                                                        </View>
                                                        <View style={ { marginTop : 2, marginBottom : 5 } }>
                                                            <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ item.pivot_created_at }</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        }   
                                        keyExtractor={(item, index) => index} 
                                    />
                                    
                                </View> :
                            <View>
                                <ActivityIndicator size="large" color="#0000ff" />
                            </View>

                        }
                    </View>
                    {
                        this.state.restaurantCheckIns.length === 0 ?
                            <View style={ { flex : 1, justifyContent : 'center', alignItems : 'center' } }>
                                <Text style={ styles.headerText }>{'No History Found!'}</Text>
                            </View> :
                        null
                    }
                </View>
            )
        }
    }

    render() {
        const PageLoader = (props) => this.__loader();
      	return (
            <Wrapper footer={this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <Header>
                    <View style={ { justifyContent : 'center', alignItems : 'center', flex : 1 } }>
                        <Text style={ styles.headerText }>{this.state.isUserProfile ? 'Favorites' : 'Checked in history'}</Text>
                    </View>
                </Header>
                {/* header */}
                {
                    this.__renderCart()
                }
            </Wrapper>
        );
    }
}
