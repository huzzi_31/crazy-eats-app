import React from 'react';
import {Image, View, Text,FlatList,Alert} from 'react-native';
import {BaseScreen} from '../Screens/BaseScreen';
import styles from './style';
import {CTouchable,Wrapper} from '../Components';
import Icon from 'react-native-vector-icons/Feather';
import {Guest} from '../Models/Guest';
import {listing} from '../Models/listing';

export class UserProfileScreen extends BaseScreen {

    constructor(props) {
        super(props);
        this.state = {
            reviewMessage : '',
            activeMenu : 1,
            loaded : false,
            user_id : this.props.navigation.state.params.id,
            isCheckIns : false,
            CheckIns : [],
            user : [],
            activeMenu : 1,
            favourites : [],
        }
        this.__getUser();
    }

    __getUser(){
        this.__activeLoader();
        Guest.getUser(this.state.user_id).then( response => {
            console.log(response);
            this.__deactiveLoader();
            if(response.me){
                this.__setState(response)
            }
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            Alert.alert('Error','Please Try Again');
        } )
    }
    
    __addVote(params){
        this.__activeLoader();
        listing.vote(this.state.user_id,params,'checkin').then( response => {
            console.log(response);
            this.__deactiveLoader();
            global.restaurtant.emit('updateRestaurant');
            if(response.me){
                this.__setState(response)
            }
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            Alert.alert('Error','Please Try Again');
        } )
    }

    __follow(){
        this.__activeLoader();
        listing.follow(this.state.user_id).then( response => {
            console.log(response);
            this.__deactiveLoader();
            global.restaurtant.emit('updateRestaurant');
            if(response.me){
                this.__setState(response)
            }
        } ).catch( error => {
            this.__deactiveLoader();
            console.log(error);
            Alert.alert('Error','Please Try Again');
        } )
    }

    __setState(response){
        this.setState( { user : response.me, CheckIns : response.me.check_ins, isCheckIns : true, favourites : response.me.favorites  } )
    }

    __renderCheckIns(item){
        if(this.state.activeMenu === 1){
            return(
                <View style={ [ styles.reviewCart ] }>
                    <View style={ [ styles.reviewCart.cart ] }>
                        <View style={ { flex : 0.1 } }>
                            <Image style={ [ styles.profileImage ] } source={ { uri : this.state.user.profile_image_url } } />
                        </View>
                        <View style={ [ styles.reviewCart.info, { flex : 0.9 } ] }>
                        <View style={ { flexDirection : 'row' } }>
                                <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ this.state.user.name }</Text>
                                <Text style={ [ styles.reviewCart.info.text, { color : '#5f5f5f', fontSize : 10, marginTop : 3  } ] }>{ 'Checked in at' }</Text>
                                <CTouchable onPress={ () => this.__go('DetailScreen', { data : item }) }><Text style={ [ styles.reviewCart.info.text, { color : '#e05355', fontSize : 13, borderBottomColor : '#e05355', borderBottomWidth : 1  } ] }>{ item.name }</Text></CTouchable>
                        </View>
                        <View style={ { marginTop : 2, marginBottom : 5 } }>
                            <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ item.pivot_created_at }</Text>
                        </View>
                            <View style={ [ styles.reviewCart.icon ] }>
                                <View style={ [ styles.reviewCart.icon._icons, { paddingRight : 15  } ] }>
                                    <CTouchable onPress={ () => this.__addVote( { id : item.pivot.id, vote : 1 } ) } style={ { flexDirection : 'row'} } >
                                        <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-up'} color={ item.myvote === 'up' ? '#e05355' : '#4a4a4a' } />
                                        <Text>{item.upvote}</Text>
                                    </CTouchable>
                                </View>
                                <View style={ [ styles.reviewCart.icon._icons ] }>
                                    <CTouchable onPress={ () => this.__addVote( { id : item.pivot.id, vote : 0 } ) } style={ { flexDirection : 'row'} } >
                                        <Icon size={15} style={ [ styles.reviewCart.icon._icons.__icons ] } name={'thumbs-down'} color={ item.myvote === 'down' ? '#e05355' : '#4a4a4a' } />
                                        <Text>{item.downvote}</Text>
                                    </CTouchable>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else{
            return(
                <CTouchable onPress={ () => this.__go('DetailScreen', { data : item }) } style={ [ styles.reviewCart ] }>
                    <View style={ [ styles.reviewCart.cart ] }>
                        <View style={ { flex : 0.1 } }>
                            <Image style={ [ styles.profileImage ] } source={ { uri :  item.logo_url  } } /> 
                        </View>
                        <View style={ [ styles.reviewCart.info, { flex : 0.9 }] }>
                            <View style={ { flexDirection : 'row' } }>
                                <Text style={ [ styles.reviewCart.info.text, styles.defaultBoldText, { color : '#000', fontSize : 15  } ] }>{ item.name }</Text>
                            </View>
                            <View style={ { marginTop : 2, marginBottom : 5 } }>
                                <Text style={ { color : '#c3c2c3', fontSize : 10 } }>{ item.pivot_created_at }</Text>
                            </View>
                        </View>
                    </View>
                </CTouchable>
            )
        }
    }

    
    __activeMenu(_step){
        this.setState( { activeMenu : _step} )
    }
    
    render() {
        const PageLoader = (props) => this.__loader();
        return (
          <Wrapper footer={ this.state.loaded == false ? <PageLoader /> : null}>
                {/* header */}
                <View style={ { width:'100%' } }>
                    <Image source={ require('../../assets/img/author_demo.jpg') } style={ { height : 220 ,width : '100%' } } />
                    <View  style={ { position : 'absolute', left : 5, right : 0, bottom : 0, top : 15 } }>
                        <CTouchable onPress={ () => this.__back() }>
                            <Icon name={'chevron-left'} color={'#fff'} size={35} />
                        </CTouchable>
                    </View>
                    <View style={ { marginTop : 80, justifyContent : 'center' } }>
                        <View style={ { position : 'absolute', left : 0, right : 0, bottom : 0, justifyContent : 'flex-end', alignItems : 'center', paddingVertical :10, paddingHorizontal : 10, } }>
                            <Image 
                                source={ { uri : this.state.user.profile_image_url } }
                                style={ { borderRadius : 50, width : 100, height : 100}}
                            />
                            <Text style={ [ styles.defaultBoldText, { fontSize : 22, color:'#000', marginTop : 5 } ] }>{this.state.user.name}</Text>
                        </View>
                    </View>
                </View>
                <View style={ { justifyContent : 'center', alignItems : 'center', marginVertical : 15, flexDirection : 'row' } }>
                    <CTouchable onPress={ () => this.__follow() } style={ [ styles.followBtn , this.state.user.followed ? {  backgroundColor : '#fff' } : {  backgroundColor : '#129eed' } ] }>
                        <Text style={ [ styles.defaultBoldText, this.state.user.followed ? { color : '#129eed' } : { color : '#fff' } ] }>{ this.state.user.followed ? 'Unfollow' : 'Follow' }</Text>
                    </CTouchable>
                    <CTouchable onPress={ () => this.__go('followers', { id : this.state.user_id }) } style={ [ styles.followBtn, { backgroundColor : '#129eed' } ] }>
                        <Text style={ [ styles.defaultBoldText, { color : '#fff' } ] }>{ 'Followers' }</Text>
                    </CTouchable>
                </View>
                {/* header */}
                <View style={ [ styles.menuTitleBar ] }>
                    <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 1 ? styles.activeMenu : '', { flex : 3 } ] }>
                        <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(1) }>
                            <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 1 ? { color : '#000' } : '',  ] }>{ 'Check In History' }</Text>
                        </CTouchable>
                    </View>
                    <View style={ [ styles.menuTitleBar.view, this.state.activeMenu === 2 ? styles.activeMenu : '', { flex : 3 } ] }>
                        <CTouchable style={ [ styles.menuTitleBar.btn ] } onPress={ () => this.__activeMenu(2) }>
                            <Text style={ [ styles.menuTitleBar.text, this.state.activeMenu === 2 ? { color : '#000' } : '', ] }>{ 'Favorites' }</Text>
                        </CTouchable>
                    </View>
                </View>
                <View style={ [ styles.mainContentReviews, ] }> 
                    {
                        this.state.isCheckIns ?
                            <FlatList 
                                extraData={this.state}
                                data={this.state.activeMenu === 1 ? this.state.CheckIns : this.state.favourites}
                                renderItem={({item}) =>
                                    this.__renderCheckIns(item)
                                }   
                                keyExtractor={(item, index) => index} 
                            /> 
                        :
                            null
                    }
                </View>
            </Wrapper>
        );
    }
}
