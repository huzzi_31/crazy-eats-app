import Base from './Base';
import config from '../../config';

export class listing extends Base {

	static getCuisines() {
		return listing.request(config.siteUrl + 'cuisines', 'GET', {});
	}

	static getRestaurants(params) {
		return listing.request(config.siteUrl + 'restaurants', 'POST', params, { 'token': Base.getAuthToken() });
	}

	static getAttachmentUrls(uri, type, name, save_type) {
		return new Promise(function (resolve, reject) {
			let body = new FormData();
            body.append('image', {
			  uri: uri,
			  type: type, 
			  name: name
			});
			if(save_type){
				body.append('type', save_type);
			}
	        
            fetch(config.siteUrl + 'upload/image', {
                'method': 'POST',
                'body': body,
            })
            .then((response) => response.json())
            .then((responseJson) => {
	        	resolve(responseJson);
	        }).catch((error) => {
	        	reject(error);
	        });
		});
	}

	static addMenu(params,id){
		let url = id ? 'restaurants/menu/' + id : 'restaurants/menu';
		return listing.request(config.siteUrl + url, 'POST', params, { 'token': Base.getAuthToken() });
	}

	static listMenus(id){
		let params = global.userData.role == 'user' ? id : global.userData.restaurant_id ;
		return listing.request(config.siteUrl + 'restaurants/menus/' + params, 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static getReviews(id){
		return listing.request(config.siteUrl + 'restaurants/reviews/' + id, 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static addReview(params){
		return listing.request(config.siteUrl + 'restaurants/review', 'POST', params, { 'token': Base.getAuthToken() });
	}

	static addFavourite(params){
		return listing.request(config.siteUrl + 'restaurants/favorite', 'POST', params, { 'token': Base.getAuthToken() });
	}

	static getFavNHistory(){
		let url = global.userData.role === 'restaurant' ? 'me/history?restaurant_id=' + global.userData.restaurant_id : 'me/history';
		return listing.request(config.siteUrl + url, 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static checkIn(params,restaurant_id){
		console.log('fsfsdfs');
		return listing.request(config.siteUrl + 'restaurants/check-in/' + restaurant_id , 'POST', params, { 'token': Base.getAuthToken() } );
	}

	static singleRestaurant(restaurant_id){
		return listing.request(config.siteUrl + 'restaurants/detail/' + restaurant_id + '?latitude=' + global.currentLat + '&longitude='+ global.currentLng +'' , 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static getRestaurantsViews(restaurant_id){
		return listing.request(config.siteUrl + 'restaurants/view/' + restaurant_id , 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static getCheckedInHistory(){
		return listing.request(config.siteUrl + 'me/history', 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static vote(id,params,subject){
		return listing.request(config.siteUrl + 'restaurants/' + subject +'/vote/' + id, 'POST', params , { 'token': Base.getAuthToken() });
	}

	static follow(userId){
		let params = {
			user_id : userId
		}
		return listing.request(config.siteUrl + 'user/follow' , 'POST', params, { 'token': Base.getAuthToken() });
	}

	static getFollowers(id, isFollowingCall = false){
		let label = isFollowingCall ? 'following' : 'followers';
		return listing.request(config.siteUrl + 'user/'+ label +'/' + id , 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static share(users,id){
		let params = {
			restaurant_id : id,
			user_ids : users
		}
		return listing.request(config.siteUrl + 'restaurants/share' , 'POST', params, { 'token': Base.getAuthToken() });
	}

	static getRestaurantCheckIns(id){
		return listing.request(config.siteUrl + 'restaurant/history/' +  id, 'GET', {}, { 'token': Base.getAuthToken() });
	}

	static reportReview(params){
		return listing.request(config.siteUrl + 'restaurant/review/report', 'POST', {params}, { 'token': Base.getAuthToken() });
	}

	static logResponse(params){
		return listing.request(config.siteUrl + 'log', 'POST', {params}, { 'token': Base.getAuthToken() });
	}

	static getCurrentUserRestaurants(){
		return listing.request(config.siteUrl + 'me/restaurants', 'GET', {}, { 'token': Base.getAuthToken() });
	}

}