import Base from './Base';
import config from '../../config';

export class Guest extends Base {

	static login(email, password) {
		
		let params = {
			email: email, 
			password: password,
			device_id : global.deviceId
		};
		return Guest.request(config.siteUrl + 'login', 'POST', params);
	}

	static register(data = {}) {
		return Guest.request(config.siteUrl + 'register', 'POST', data);
	}

	static facebookLogin(token) {
		
		let params = {
			accessToken: token, 
		};
		return Guest.request(config.siteUrl + 'facebookLogin', 'POST', params);
	}
	
	static saveLocation(params) {
		return Guest.request(config.siteUrl + 'save-location', 'POST', params, { 'token': Base.getAuthToken() });
	}

	static me(){
		let url = global.userData.role  === 'restaurant' ? 'me?restaurant_id=' + global.userData.restaurant_id : 'me';
		return Guest.request(config.siteUrl + url , 'GET', {} , { 'token': Base.getAuthToken() });	
	}

	static updateField(role,params){
		let url = role === 'restaurant' ? '/update-field?restaurant_id=' + global.userData.restaurant_id : '/update-field';
		return Guest.request(config.siteUrl + role + url, 'POST', params , { 'token': Base.getAuthToken() });	
	}

	static contactForm(params){
		return Guest.request(config.siteUrl + 'contact-form', 'POST', params , { 'token': Base.getAuthToken() });	
	}

	static getUser(id){
		return Guest.request(config.siteUrl + 'user/' + id, 'GET', {} , {'token': Base.getAuthToken() });	
	}

	static updateHours(params){
		return Guest.request(config.siteUrl + 'restaurant/update-hours?restaurant_id=' + global.userData.restaurant_id, 'POST', params , {'token': Base.getAuthToken() });	
	}

	static updatePassword(params){
		return Guest.request(config.siteUrl + 'user/update-password', 'POST', params , {'token': Base.getAuthToken() });		
	}

	static createRestaurant(params){
		return Guest.request(config.siteUrl + 'me/restaurant', 'POST', params , {'token': Base.getAuthToken() });		
	}

	static forgetPassword(params){
		return Guest.request(config.siteUrl + 'forget-password', 'POST', params );	
	}

}