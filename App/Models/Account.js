import Base from './Base';
import config from '../../config';

export class Account extends Base {

    static addContactNumber(number){
        let params = {
            contact_number : number,
        }
        return Account.request(global.langlocaleUrl + config.apiUrl + 'contact-numbers' , 'POST', {params} , {'Authorization': Base.getAuthToken()});
    }

    static getContactNumber(){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'contact-numbers' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static deleteContactNumber(id){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'delete-numbers?post_id='+id , 'POST', {} , {'Authorization': Base.getAuthToken()});
    }

    static sendForVerification(id,number){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'send-sms?id='+ id +'&contact_number=' + number , 'POST', {} , {'Authorization': Base.getAuthToken()});
    }

    static verifyCode(id,number){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'verify-code?code='+number+'&id='+id , 'POST', {} , {'Authorization': Base.getAuthToken()});
    }

    static simpleAccountListing(){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'listing/normal' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static auctionAccountListing(){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'listing/bid' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static editProfile(params){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'edit-profile' , 'POST', {} , {'Authorization': Base.getAuthToken()});
    }

    static userData(params){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'me' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static showAd(id,status){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'show-ad?post='+id+'&status='+status, 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static getResumes(){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'resume-listing' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static getEnquiries(){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'enquiry-listing' , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }
    
    static getEnquiryDetail(id){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'enquiry-detail?enquiry_id='+id , 'GET', {} , {'Authorization': Base.getAuthToken()});
    }

    static sendEnquiry(params){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'send-enquiry' , 'POST', {params} , {'Authorization': Base.getAuthToken()}); 
    }

    static payment(params){
        return Account.request(global.langlocaleUrl + config.apiUrl + 'get-braintree-client-token' , 'POST', {params} , {'Authorization': Base.getAuthToken()}); 
    }
}