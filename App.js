import React, { Component } from 'react';
import { StackNavigator, DrawerNavigator,DrawerItems,StackActions, NavigationActions  } from 'react-navigation';
import {
    View,
    Text,
    Image,
    AsyncStorage,
    Alert,
    FlatList,
} from 'react-native';
import { SplashScreen } from './App/Screens/SplashScreen';
import { LoginScreen } from './App/Screens/LoginScreen';
import { RegisterScreen } from './App/Screens/RegisterScreen';
import { ForgetPassword } from './App/Screens/ForgetPassword';
import MainTabs from './App/Screens/MainTabs';
import { DetailScreen } from './App/Screens/DetailScreen';
import { WriteReviewScreen } from './App/Screens/WriteReviewScreen';
import { MenuScreen } from './App/Screens/MenuScreen';
import { ReviewerProfile } from './App/Screens/ReviewerProfile';
import { ByPlacesList } from './App/Screens/ByPlacesList';
import { SearchResults } from './App/Screens/SearchResults';
import { AddMenuScreen } from './App/Screens/AddMenuScreen';
import ReactNativeLocationServicesSettings from 'react-native-location-services-settings';
import { SearchForm } from './App/Screens/SearchForm';
import { ReviewScreen } from './App/Screens/ReviewScreen';
import BackgroundTimer from 'react-native-background-timer';
import {Guest} from './App/Models/Guest';
import {listing} from './App/Models/listing';
import { UserProfileScreen } from './App/Screens/UserProfileScreen';
import { Gallery } from './App/Screens/Gallery';
import { Followers } from './App/Screens/Followers';
import { CheckInList } from './App/Screens/CheckInList';
import { RestaurantHourScreen } from './App/Screens/RestaurantHourScreen';
import { UpdatePasswordScreen } from './App/Screens/UpdatePasswordScreen';
import OneSignal from 'react-native-onesignal'; 
import {CTouchable} from './App/Components/index';
import styles from './App/Screens/style';
import { RestaurantSplashScreen } from './App/Screens/RestaurantSplashScreen';
import { RestaurantCreateScreen } from './App/Screens/RestaurantCreateScreen';
import {checkPermission} from 'react-native-android-permissions';


var EventEmitter = require('eventemitter3');

global.userData = {};
global.token = '';
global.currentLat = false;
global.currentLng = false;
global.isUserLoggedIn = false;
global.isLocationEnable = false;
global.cuisinesList = [];
global.deviceId = '';

var ___emitter = new EventEmitter();
global.initHistory = ___emitter.addListener('getHistory', () => {});
global.restaurtant = ___emitter.addListener('updateRestaurant', () => {});
global.menus = ___emitter.addListener('getMenus', () => {});

BackgroundTimer.start();
BackgroundTimer.setInterval(() => {
	if(global.isUserLoggedIn){
        ReactNativeLocationServicesSettings.checkStatus('high_accuracy').then(res => {
            if (res.enabled) {
              //  __getLocation();
            }
        })
    }
}, 60 * 2000);
BackgroundTimer.stop();



const appStackNavigator = StackNavigator({
    Main: {
        screen: SplashScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    login: {
        screen: LoginScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    register: {
        screen: RegisterScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    forgetPassword: {
        screen: ForgetPassword,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    MainTabs: {
        screen: MainTabs,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    DetailScreen: {
        screen: DetailScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    WriteReview: {
        screen: WriteReviewScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    MenuScreen: {
        screen: MenuScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    ReviewerProfile: {
        screen: ReviewerProfile,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    PlacesNearby: {
        screen: ByPlacesList,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    SearchResults: {
        screen: SearchResults,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    AddMenu: {
        screen: AddMenuScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    SearchForm: {
        screen: SearchForm,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    Reviews: {
        screen: ReviewScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    UserProfile: {
        screen: UserProfileScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    gallery: {
        screen: Gallery,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    followers: {
        screen: Followers,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    checkIn: {
        screen: CheckInList,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    restaurtantHour: {
        screen: RestaurantHourScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    updatePassword: {
        screen: UpdatePasswordScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    RestaurantCreate: {
        screen: RestaurantCreateScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },
    RestaurantList: {
        screen: RestaurantSplashScreen,
        headerMode: 'float',
            navigationOptions: {
                header: null
        }
    },

});

class DrawerContent extends React.Component {   

    constructor(props){
        super(props);
        this.state = {
            restaurtants : [],
            hideNavigation : true,
            isUserLoggedIn: false,
            userEmail :  '',
            userName  :  '',
            profileImageUrl : '',
        }
        
        global.drawerComponent = this;
        AsyncStorage.getItem('user').then(response => {
            try {
                let responseJson = JSON.parse(response);
                if(responseJson.id !== undefined && responseJson.id !== 0) {
                    global.userData.token = responseJson.token;
                    this.role = responseJson.role;
                    if(this.role == 'restaurant'){
                        this.__getCurrentUserRestaurants();
                    } 
                }
            } catch(e) {}
           
        });
        this.__askForLocation();
      
    }

    // __askForLocation(){
    //     checkPermission("android.permission.ACCESS_FINE_LOCATION").then((result) => {
    //         console.log("Already Granted!");
    //         console.log(result);
    //       }, (result) => {
    //         console.log("Not Granted!");
    //         console.log(result);
    //       });
    // }

    __askForLocation(){
        ReactNativeLocationServicesSettings.checkStatus('high_accuracy').then(res => {
            if (!res.enabled) {
            	ReactNativeLocationServicesSettings.askForEnabling(res => {
                	if (res) {
						navigator.geolocation.getCurrentPosition(
                            (position) => {
                                var lat = parseFloat(position.coords.latitude);
                                var long = parseFloat(position.coords.longitude);
                                console.log(lat,long,'d');
                                global.currentLat = lat;
                                global.currentLng = long;
                                global.isLocationEnable = true;
                                // if( global.isUserLoggedIn ){
                                // 	this.__goAndReset('MainTabs');
                                // }
                            },
                            (error) => {
                                Alert.alert('Error',error.message)
                            },
                            { enableHighAccuracy : false, timeout : 30000, maximumAge : 10000 }
                        )
                	} else {
                  		alert('location services were denied by the user')
                	}
              	})
            } else{
                this.__getLocation();
                
            }
        })
    }

    __getLocation(){
        navigator.geolocation.getCurrentPosition(
            (position) => {
                var lat = parseFloat(position.coords.latitude);
                var long = parseFloat(position.coords.longitude);
                console.log(lat,long,'d');
                global.currentLat = lat;
                global.currentLng = long;
                global.isLocationEnable = true;
                // if( global.isUserLoggedIn ){
                // 	this.__goAndReset('MainTabs');
                // }
            },
            (error) => {
                Alert.alert('Error',error.message)
            }
        )
    }

    __getCurrentUserRestaurants(){
        listing.getCurrentUserRestaurants().then( response => {
            console.log(response);
            if(response.status){
                this.setState( {
                    restaurants : response.restaurants
                } )
            }
        } ).catch( error => {
            console.log(error);
        } )
    }
    
    __renderNavigation(){
        if(!this.state.hideNavigation){
            return(
                <View style={ { marginTop : 10, flex : 1, } }>
                    <FlatList 
                        extraData={this.state}
                        data={this.state.restaurants}
                        renderItem={({item}) =>
                            {
                                return(
                                    <CTouchable onPress={ () => this.__switchRestaurant(item.id) } style={ styles.drawerNavTextView }>
                                        <Image style={ { width : 40, height : 40, borderRadius : 50, marginHorizontal : 3 } } source={ { uri : item.logo_url  } } />
                                        <Text style={ [ styles.defaultText, { fontSize : 15, marginLeft : 15}]}>{item.name}</Text>
                                    </CTouchable>
                                )
                            }
                        }
                    />
                    <CTouchable onPress={ () => this.__go('RestaurantList') } style={ [styles.drawerNavTextView,{ backgroundColor : '#e05355' } ] }>
                        <Text style={ [ styles.defaultBoldText, { fontSize : 15, marginLeft : 15, color : '#fff'}]}>{ 'Your Restaurants' }</Text>
                    </CTouchable>
                </View>
            )
        }
    }

    __goAndReset(routeName, params = {}) {
		const resetAction = StackActions.reset({
			index: 0,
			actions: [NavigationActions.navigate({ routeName: routeName, params })],
		  });
        this.props.navigation.dispatch(resetAction);
    }
    
    __go(to, params = {}) {
		const pushAction = StackActions.push({
			routeName: to,
			params: params 
		  });
		  this.props.navigation.dispatch(pushAction);
	}
    __switchRestaurant(id){
        global.userData.restaurant_id = id;
        AsyncStorage.setItem('user', JSON.stringify(global.userData));
        this.__goAndReset('MainTabs')
    }

  render() {
    return (
      <View style={ { flex : 1 } }>
        <View style={ { backgroundColor : '#f0f0f2', height : 100 } }>
            <View style={ { flexDirection : 'row', alignItems : 'center', marginLeft : 10, marginTop : 15 } }>
            <View style={ { paddingTop:20,paddingBottom:10 } }>
                <Image source={ { uri : this.state.profileImageUrl  } } style={ { width : 50, height : 50, borderRadius : 50 } } />
            </View> 
            <View style={ { paddingHorizontal:10 } }>
                <Text style={[styles.defaultText,{ fontSize : 18 } ]}>{this.state.userName}</Text>
                <Text style={[styles.defaultText,{ fontSize : 15 } ]}>{this.state.userEmail}</Text>
            </View> 
            </View>
        </View>
        {
          this.__renderNavigation()
        }
      </View> 
    );
  }

}

function __getLocation(){
    navigator.geolocation.getCurrentPosition( 
        (position) => {
            var lat = parseFloat(position.coords.latitude);
            var long = parseFloat(position.coords.longitude);
            console.log(lat,global.currentLat);
            console.log(long,global.currentLng);
            if(lat != global.currentLat && long != global.currentLng ){
                let params = {
                    latitude : lat,
                    longitude : long
                }
                Guest.saveLocation(params).then( response => {
                    console.log(response);
                    global.currentLat =  lat;
                    global.currentLng =  long;
                } ).catch( error => {
                    console.log(error);
                } )
            } else{
                console.log('dsfsd');
            }
        },
        (error) => {
            console.log(error.message)
        },
      
    )
}

const App = DrawerNavigator({
    Main: {
      screen: appStackNavigator,
    },
    
  },{
    contentComponent: DrawerContent ,
})


export default App;
